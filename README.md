# Introduction
Read the documentation in qemu-doc.html.

# How To Build
1. `cd qemu_vpmu && mkdir build`
2. `cd build`
3. `../configure --target-list=arm-softmmu --enable-vpmu`
4. `colormake -j8` (Parallel compiling with 8 threads. You can use make if you want)

# Prerequisite
1. Install dependencies(packages) of QEMU
2. Guest image files (You need to prepare it by yourself).
3. Python 2 (instead of 3)
4. (Deprecated! QEMU 0.15 Only) gnutls < 3.4.0 and nettle <= 2.7.1 (caution: you may make a copy of libgnutls.so, libgnutlsxx.so, libhogweed.so, libnettle.so in case of trivial software dependency)
 
# How To Execute
Before execution, please make sure to run `source ./build/setenv.sh` in advance.

This is just a sample, you may need to modify some parts to fit your needs.

## Board: realview-pbx-a9
```
./build/arm-softmmu/qemu-system-arm -kernel ../images/zImage_realview_pbx_a9 -initrd ../images/rootfs.cpio -M realview-pbx-a9 -nographic -m 1024m
```

## Board: vexpress
```
./build/arm-softmmu/qemu-system-arm -kernel ../images/zImage-vexpress -initrd ../images/rootfs.cpio -append "console=ttyAMA0" -nographic -M vexpress -m 1024m
```

