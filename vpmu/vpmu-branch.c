#include "vpmu.h"
#define MAX_BRANCH_MODEL 16
#define BRANCH_TRACE_BUFFER_SIZE 40000

//Define package type of a single trace
typedef struct Branch_Reference {
    uint32_t type;
    uint32_t pc;
    uint32_t taken;
    uint8_t  core;
} Branch_Reference;

//The data/states of each simulators
typedef struct Branch_Data{
    uint64_t predictor;    //predictor (the states of branch predictors)
    uint64_t correct;      //branch_predict_correct counter
    uint64_t wrong;        //branch_predict_wrong counter
} Branch_Data;

//The unit of managing simulators
typedef struct Branch_Package{
    int config;
    Branch_Data data[VPMU_MAX_CPU_CORES];
    void (*ref)(Branch_Data *, Branch_Reference *);
} Branch_Package;

//Mapping table between json and c funciton
typedef struct Branch_Simulator_Table{
    char *name;
    void (*entry)(Branch_Data *, Branch_Reference *);
} Branch_Simulator_Table;

//Define ring buffer
ringBuffer_typedef(Branch_Reference, BranchTraceBuffer);
//Global trace buffer
static BranchTraceBuffer* branch_trace_buffer_t = NULL;
//Worker thread pointer
static pthread_t branch_predictor_thread;
//Semaphore for signaling worker thread
static sem_t *branch_job_semaphore = NULL;

//Define the general packets of configurations and data of each simulators
static Branch_Package pkg[MAX_BRANCH_MODEL] = {{0}};
//Define the mapping table for locating entry functions
static Branch_Simulator_Table branch_sim_table[MAX_BRANCH_MODEL] = {{0}};
//Total number of registered models(algorithms)
static int model_num = 0;
//Total number of registered simulators(instances)
static int sim_num = 0;
//A function to sum up values in the array
static Branch_Data sum(Branch_Data *);

//include your own model here
#include "sims/branch_one_bit.c"
#include "sims/branch_two_bits.c"

//Handles types of packets
static void inline packet_processor(Branch_Reference ref)
{
    int i, j;
#ifdef CONFIG_VPMU_DEBUG_MSG
    static uint64_t debug_packet_num_cnt = 0;
    debug_packet_num_cnt++;
    if (ref.type == VPMU_PACKET_DUMP_INFO) {
        DBG("  %'"PRIu64" packets received\n", debug_packet_num_cnt);
        debug_packet_num_cnt = 0;
    }
#endif

    //Every simulators should handle VPMU_BARRIER_PACKET to support synchronization
    //The implementation depends on your own packet type and writing style
    switch (ref.type) {
        case VPMU_PACKET_BARRIER:
            break;//Skip every barrier packet
        case VPMU_PACKET_DUMP_INFO:
            CONSOLE_LOG("Branch\n"); 
            for (i = 0; i < sim_num; i++) {
                Branch_Data val = sum(pkg[i].data);
                float a;

                CONSOLE_LOG("  [%d] type : %s\n", 
                        i, branch_sim_table[pkg[i].config].name);
                //Accuracy
                CONSOLE_LOG("    -> predict accuracy    : %'0.2f (", 
                        (float)val.correct / (val.correct + val.wrong));
                for (j = 0; j < GlobalVPMU.cpu_model.num_cores - 1; j++) {
                    a = (float)pkg[i].data[j].correct / 
                        (pkg[i].data[j].correct + pkg[i].data[j].wrong);
                    CONSOLE_LOG("%'0.2f, ", a);
                }
                a = (float)pkg[i].data[j].correct / 
                        (pkg[i].data[j].correct + pkg[i].data[j].wrong);
                CONSOLE_LOG("%'0.2f)\n", a);
                //Correct
                CONSOLE_LOG("    -> correct prediction  : %'"PRIu64" (", val.correct);
                for (j = 0; j < GlobalVPMU.cpu_model.num_cores - 1; j++)
                    CONSOLE_LOG("%'"PRIu64", ", pkg[i].data[j].correct);
                CONSOLE_LOG("%'"PRIu64")\n", pkg[i].data[j].correct);
                //Wrong
                CONSOLE_LOG("    -> wrong prediction    : %'"PRIu64" (", val.wrong);
                for (j = 0; j < GlobalVPMU.cpu_model.num_cores - 1; j++)
                    CONSOLE_LOG("%'"PRIu64", ", pkg[i].data[j].wrong);
                CONSOLE_LOG("%'"PRIu64")\n", pkg[i].data[j].wrong);
            }
            break;
        case VPMU_PACKET_RESET:
            for (i = 0; i < sim_num; i++)
                memset((void *)pkg[i].data, 0, sizeof(Branch_Data) * VPMU_MAX_CPU_CORES);
            break;
        case VPMU_PACKET:
            for (i = 0; i < sim_num; i++) {
                pkg[i].ref(&pkg[i].data[ref.core], &ref);
            }
            break;
        default:
            ERR_MSG("Unexpected packet in branch simulators\n");
    }
}

//The main function of worker threads
void *branch_thread(void * ptr)
{
    static Branch_Reference local_buffer[64];
    const int local_buffer_size = sizeof(local_buffer)  / sizeof(Branch_Reference);
    int num_refs = 0;

    DBG("VPMU worker thread (branch predictor) : start\n");
    //Only be cancelable at cancellation points, ex: sem_wait
    pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, NULL);
    while(1) {
        VPMU_WAIT_JOB(branch_job_semaphore, 
            //Keep draining jobs till it's empty
            while (likely(isBufferNotEmpty(branch_trace_buffer_t))) {
                bulkRead(
                    branch_trace_buffer_t,     //Pointer to ringbuffer
                    local_buffer,              //Pointer to local(private) buffer
                    local_buffer_size,         //#elements of local(private) buffer
                    sizeof(Branch_Reference),  //Size of each elements
                    num_refs);                 //#elements read successfully
                for (int i = 0; i < num_refs; i++)
                    packet_processor(local_buffer[i]);
            }
        );
    }
}

//The interface between QEMU and worker threads. pass the trace through buffer
void branch_ref(uint8_t core, uint32_t pc, uint32_t taken, uint32_t type)
{
    static Branch_Reference local_buffer[32];
    const int local_buffer_size = sizeof(local_buffer) / sizeof(Branch_Reference);
    static int local_index = 0;

#ifdef CONFIG_VPMU_DEBUG_MSG
    static uint64_t debug_packet_num_cnt = 0;
    debug_packet_num_cnt++;
    if (type == VPMU_PACKET_DUMP_INFO) {
        DBG("VPMU sent %'"PRIu64" packets\n", debug_packet_num_cnt);
        debug_packet_num_cnt = 0;
    }
#endif

    local_buffer[local_index].pc = pc;
    local_buffer[local_index].taken = taken;
    local_buffer[local_index].type = type;
    local_buffer[local_index].core = core;

    local_index++;
    //Force to clean out local buffer whenever the packet is a control packet
    if (unlikely(IS_VPMU_CONTROL_PACKET(type)) || 
        unlikely(local_index == local_buffer_size)) {
        VPMU_PUSH_JOB(branch_job_semaphore, 
            waitSpaceSize(branch_trace_buffer_t, local_buffer_size);
            bulkWrite(
                branch_trace_buffer_t,      //Pointer to ringbuffer
                local_buffer,               //Pointer to local(private) buffer
                local_index,                //Number of elements to write
                sizeof(Branch_Reference));  //Size of each elements
        );
        local_index = 0;
    }
}

static Branch_Data sum(Branch_Data *data)
{
    int i;
    Branch_Data sum = {0};

    for (i = 0; i < VPMU_MAX_CPU_CORES; i++) {
        sum.correct += data[i].correct;
        sum.wrong += data[i].wrong;
    }

    return sum;
}

uint64_t VPMU_branch_dump(void *ptr)
{
    if (!vpmu_model_has(VPMU_BRANCH_SIM, GlobalVPMU))
        return 0;
    VPMU_POST_JOB(branch_job_semaphore,
        branch_ref(ALL_PROC, 0, 0, VPMU_PACKET_DUMP_INFO);
    );

    return 0;
}

uint64_t VPMU_branch_sync(void *ptr)
{
    if (!vpmu_model_has(VPMU_BRANCH_SIM, GlobalVPMU))
        return 0;
    //Push the barrier packet into the queue to 
    //ensure everything before the barrier packet is done.
    VPMU_POST_JOB(branch_job_semaphore,
        branch_ref(ALL_PROC, 0, 0, VPMU_PACKET_BARRIER);
    );
    while(isBufferNotEmpty(branch_trace_buffer_t));
    VPMU_POST_JOB(branch_job_semaphore,
        branch_ref(ALL_PROC, 0, 0, VPMU_PACKET_BARRIER);
    );
    //Wait till it's done "twice" to ensure the property of barrier
    //Note this must be done twice due to the bulk read!
    //Otherwise, you might miss less than 1k/2k references!!!!
    while(isBufferNotEmpty(branch_trace_buffer_t));

    Branch_Data val = sum(pkg[0].data);
    GlobalVPMU.branch_predict_correct = val.correct;
    GlobalVPMU.branch_predict_wrong = val.wrong;
    DBG("VPMU: branch sync\n");

    return 0;
}

uint64_t VPMU_branch_free(void *ptr)
{
    if (branch_job_semaphore) {
        sem_destroy(branch_job_semaphore); //Destroy semaphore
        //DO NOT FREE the semaphore, it causes malloc SIGSEGV
        branch_job_semaphore = NULL;
    }

    if (branch_trace_buffer_t) {
        bufferDestroy(branch_trace_buffer_t);
        free(branch_trace_buffer_t);
        branch_trace_buffer_t = NULL;
    }

    if (branch_predictor_thread) {
        //This cancel is a safe cancel
        pthread_cancel(branch_predictor_thread);
        pthread_join(branch_predictor_thread, NULL);
        branch_predictor_thread = 0;
    }

    DBG("VPMU: branch release resources\n");

    return 0;
}

uint64_t VPMU_branch_reset(void *ptr)
{
    VPMU_POST_JOB(branch_job_semaphore,
        branch_ref(ALL_PROC, 0, 0, VPMU_PACKET_RESET);
    );
    //Wait till it's done to ensure the property of barrier
    while(isBufferNotEmpty(branch_trace_buffer_t));
    DBG("VPMU: branch reset\n");

    return 0;
}

static int locate_entry(const char *name)
{
    int i;

    for (i = 0; i < model_num; i++) {
        if (strcmp(name, branch_sim_table[i].name) == 0)
            return i;
    }

    return -1;
}

//Put the registration function of your own model here
static void register_branch_sims(void)
{
    one_bit_register();
    two_bits_register();
}

static void parse_json_configs(char *str)
{
    struct json_object *root;
    int i;

    root = json_tokener_parse(str);
    sim_num = json_object_array_length(root);
    if (sim_num > MAX_BRANCH_MODEL)
        ERR_MSG("# of models(%d) > MAX_BRANCH_MODEL(%d)\n", sim_num, MAX_BRANCH_MODEL);
    for (i = 0; i < sim_num; i++) {
        json_object *obj = json_object_array_get_idx(root, i);
        DBG("\tbranch simulator[%d]=%s\n", i, json_object_to_json_string(obj));
        int idx = locate_entry(get_json_str(obj, "name"));
        if (idx == -1)
            ERR_MSG("Could not find the simulator named \"%s\"\n", 
                    get_json_str(obj, "name"));
        //Copy the configuration and entry function
        pkg[i].config = idx;
        pkg[i].ref = branch_sim_table[idx].entry;
    }
    return ;
}

uint64_t VPMU_branch_init(void *ptr)
{
    DBG("VPMU: branch init\n");
    register_branch_sims();

    //Initialization for multi-simulator configurations
    parse_json_configs((char *)ptr);

    //Initialization for multi-threading
    branch_job_semaphore = (sem_t *)malloc(sizeof(sem_t));
    sem_init(branch_job_semaphore, 0, 0); //initialize mutex to zero
    bufferInit(
            branch_trace_buffer_t, 
            BRANCH_TRACE_BUFFER_SIZE, 
            Branch_Reference, 
            BranchTraceBuffer);
    if (!isBufferAllocated(branch_trace_buffer_t))
        ERR_MSG("branch buffer is not allocated successfully!!\n");
    pthread_create(&branch_predictor_thread, NULL, branch_thread, &GlobalVPMU);

    return 0;
}

