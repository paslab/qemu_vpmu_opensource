#include "vpmu.h"
//lighting shared memory ring buffer header (single master multiple slaves)
#include "vpmu/shm_ringbuffer.h"
#include "vpmu/shm_addr_map.h"
#define MAX_CACHE_MODEL 16

//Define package type of a single trace
typedef struct Cache_Reference {
    uint32_t addr;
    uint16_t size;
    uint16_t type;
    uint8_t  processor;
    uint8_t  core;
} Cache_Reference;

//The unit of managing simulators
typedef struct Cache_Package{
    //A number representing the index of configuration in simulator table
    int config;
    //A pointer pointing to the json string of configuration
    const char *sim_config;
    //semaphore for signaling worker thread
    sem_t cache_job_semaphore;
    //worker process id
    pid_t pid;
    //Other hooked program
    const char *program_path;
    //Cache model information that VPMU required for some functions.
    VPMU_Cache_Model cache_model;
    //cache counter information
    Cache_Data data;
    //Synchronization Counter to identify the serial number of synchronized data
    volatile int sync_counter;
    //Synchronization flag to indicate whether it's done
    volatile int synced_flag;
    //entry functions
    void (*entry)(void *, int, int, int);
} Cache_Package;

//Mapping table between json and c funciton
typedef struct Cache_Simulator_Table{
    char *name;
    void (*entry)(void *, int, int, int);
} Cache_Simulator_Table;

//Define ring buffer
shm_ringBuffer_typedef(Cache_Reference, CacheTraceBuffer, MAX_CACHE_MODEL);
//Global trace buffer
static CacheTraceBuffer* cache_trace_buffer_t;

//Define the general packets of configurations and data of each simulators
static Cache_Package *pkg = NULL;
//Define the mapping table for locating entry functions
static Cache_Simulator_Table cache_sim_table[MAX_CACHE_MODEL] = {{0}};
//Total number of registered models(algorithms)
static int model_num = 0;
//Total number of registered simulators(instances)
static int sim_num = 0;
//Pointer to the shared memory region that contains all the data to be shared
static void *vpmu_cache_shm_page = NULL;
//Define the general packets of configurations and data of each simulators
static volatile uint32_t *shm_token = NULL;

static void check_cache_sim_liveness(void);

#include "sims/dinero.c"
//Put the registration function of your own model here
static void register_cache_sims(void)
{
    dinero_register();
}

//TODO add streaming interface to pipe the result to a file periodically
//interface between QEMU and worker thread. pass the trace through buffer
void
cache_ref(uint8_t proc, uint8_t core, uint32_t addr, uint16_t type, uint16_t data_size)
{
    static Cache_Reference local_buffer[128];
    const int local_buffer_size = sizeof(local_buffer) / sizeof(Cache_Reference);
    static int local_index = 0;

#ifdef CONFIG_VPMU_DEBUG_MSG
    static uint64_t debug_packet_num_cnt = 0;
    debug_packet_num_cnt++;
    if (type == VPMU_PACKET_DUMP_INFO) {
        DBG("VPMU sent %'"PRIu64" packets\n", debug_packet_num_cnt);
        debug_packet_num_cnt = 0;
    }
#endif

    local_buffer[local_index].addr = addr;
    local_buffer[local_index].type = type;
    local_buffer[local_index].size = data_size;
    local_buffer[local_index].processor = proc;
    local_buffer[local_index].core = core;

    local_index++;
    //Force to clean out local buffer whenever the packet is a control packet
    if (unlikely(IS_VPMU_CONTROL_PACKET(type)) ||
        unlikely(local_index == local_buffer_size)) {
        VPMU_SHM_PUSH_JOB(pkg, sim_num,
            shm_waitSpaceSize(cache_trace_buffer_t, sim_num, local_buffer_size);
            shm_bulkWrite(
                cache_trace_buffer_t,     //Pointer to ringbuffer
                local_buffer,             //Pointer to local(private) buffer
                local_index,              //Number of elements to write
                sizeof(Cache_Reference)); //Size of each elements
        );
        local_index = 0;
    }
    //This can dump trace
    //DBG("ENQ: TYPE=%d ADDR=%x SIZE=%d\n", type, addr, data_size);
}

static void *cache_sync_callback_helper(void *ptr)
{
    void **param_array = (void **)ptr;
    void *(*callback)(void *) = param_array[0];

    // Reset the flag whenever you want to sync to enable slave sync data lazyly.
    // Clear this flag means that last round has been done
    // and slaves can start to do this round.
    for (int i = 0; i < sim_num; i ++) pkg[i].synced_flag = 0;
    // Wait all simulators passes synchronization point
    for (int i = 0; i < sim_num; i++) while (!pkg[i].synced_flag) usleep(1);
    //DBG("%d\n", pkg[0].sync_counter);
    callback((void *)param_array[1]);
    free(param_array);

    return NULL;
}

uint64_t VPMU_cache_sync_callback(void (*callback)(void *), void *arg)
{
    void **param_array = (void **)malloc(sizeof(void *) * 2);
    VPMU_SHM_POST_JOB(pkg, sim_num,
        cache_ref(ALL_PROC, ALL_PROC, 0, VPMU_PACKET_SYNC_DATA, 0);
    );

    param_array[0] = (void *)callback;
    param_array[1] = (void *)arg;
    thpool_add_work(GlobalVPMU.thpool,
                   (void *)cache_sync_callback_helper,
                   (void *)param_array);

    return 0;
}

uint64_t VPMU_cache_dump(void *ptr)
{
    CONSOLE_LOG("Cache\n");
    //Set token to the first simulator
    shm_reset_token(shm_token);
    if (vpmu_model_has(VPMU_DCACHE_SIM | VPMU_ICACHE_SIM, GlobalVPMU)) {
        VPMU_SHM_POST_JOB(pkg, sim_num,
            cache_ref(ALL_PROC, ALL_PROC, 0, VPMU_PACKET_DUMP_INFO, 0);
        );
        //Wait till it's done to ensure the property of barrier
        shm_waitBufferEmpty(cache_trace_buffer_t, sim_num);
    }

    return 0;
}

uint64_t VPMU_cache_free(void *ptr)
{
    int i;

    for (i = 0; i < sim_num; i++) {
        sem_destroy(&pkg[i].cache_job_semaphore);
    }
    if (vpmu_cache_shm_page) {
        shmdt(vpmu_cache_shm_page);
        vpmu_cache_shm_page = NULL;
    }

    //This kill signal is NOT a safe kill point
    for (i = 0; i < sim_num; i++) {
        kill(pkg[i].pid, SIGQUIT);
    }

    DBG("VPMU: cache release resources\n");

    return 0;
}

uint64_t VPMU_cache_sync(void *ptr)
{
    //Barrier packet also synchronize data back to Cache_Data structure.
    //Push the barrier packet into the queue to
    //ensure everything before the barrier packet is done.
    VPMU_SHM_POST_JOB(pkg, sim_num,
        cache_ref(ALL_PROC, ALL_PROC, 0, VPMU_PACKET_BARRIER, 0);
    );
    shm_waitBufferEmpty(cache_trace_buffer_t, sim_num);
    VPMU_SHM_POST_JOB(pkg, sim_num,
        cache_ref(ALL_PROC, ALL_PROC, 0, VPMU_PACKET_BARRIER, 0);
    );
    //Wait till it's done "twice" to ensure the property of barrier
    //Note this must be done twice due to the bulk read!
    //Otherwise, you might miss less than 1k/2k references!!!!
    shm_waitBufferEmpty(cache_trace_buffer_t, sim_num);

    return 0;
}

void VPMU_cache_sync_none_blocking(void)
{
    //Push the barrier packet into the queue to synchronize data.
    VPMU_SHM_POST_JOB(pkg, sim_num,
        cache_ref(ALL_PROC, ALL_PROC, 0, VPMU_PACKET_BARRIER, 0);
    );
}

uint64_t VPMU_cache_reset(void *ptr)
{
    int i;

    VPMU_SHM_POST_JOB(pkg, sim_num,
        cache_ref(ALL_PROC, ALL_PROC, 0, VPMU_PACKET_RESET, 0);
    );

    //Clear all the counters of each simulator
    for (i = 0; i < sim_num; i++) {
        memset(&pkg[i].data, 0, sizeof(Cache_Data));
        pkg[i].sync_counter = 0;
        pkg[i].synced_flag = 1; //reset to 1
    }
    DBG("VPMU: cache reset\n");
    return 0;
}

static void fork_child_process(int index, Cache_Package *p)
{
    pid_t pid = fork();

    if (pid) {
        //Parent
        p->pid = pid;
    }
    else {
        //Child
        if (p == NULL || p->entry == NULL)
            ERR_MSG("Entry function is NULL!\n");
        else {
            p->entry((void *)p->sim_config, index,
                    GlobalVPMU.cpu_model.num_cores,
                    GlobalVPMU.gpu_model.num_cores);
        }
        //It should never return!!!
        exit(EXIT_FAILURE);
    }
}

static void fork_execute_process(int index, Cache_Package *p)
{
    pid_t pid = fork();

    if (pid) {
        //Parent
        p->pid = pid;
    }
    else {
        //Child
        execlp(p->program_path, p->program_path, NULL);
        ERR_MSG("Unable to execute program: %s", p->program_path);
        //It should never return!!!
        exit(EXIT_FAILURE);
    }
}


static int locate_entry(const char *name)
{
    int i;

    for (i = 0; i < model_num; i++) {
        if (strcmp(name, cache_sim_table[i].name) == 0)
            return i;
    }

    return -1;
}

static void parse_json_configs(char *str)
{
    struct json_object *root;
    int i;

    root = json_tokener_parse(str);
    sim_num = json_object_array_length(root);
    if (sim_num > MAX_CACHE_MODEL)
        ERR_MSG("# of models(%d) > MAX_CACHE_MODEL(%d)\n",
                sim_num, MAX_CACHE_MODEL);
    for (i = 0; i < sim_num; i++) {
        json_object *obj = json_object_array_get_idx(root, i);
        int idx = locate_entry(get_json_str(obj, "name"));
        if (idx == -1)
            ERR_MSG("Could not find the simulator named \"%s\"\n",
                    get_json_str(obj, "name"));

        //Copy the configuration and entry function
        pkg[i].config = idx;
        pkg[i].sim_config = json_object_to_json_string(obj);
        pkg[i].entry = cache_sim_table[idx].entry;
        //Only set if it exists and is not empty string
        if (check_json_field(obj, "program_path") &&
            strcmp(get_json_str(obj, "program_path"), "") != 0) {
            pkg[i].program_path = get_json_str(obj, "program_path");
        }
        else
            pkg[i].program_path = NULL;
    }

    return ;
}

static void check_cache_sim_liveness(void)
{
    int i, j, cnt = 0;

    //Check liveness of child processes and shared memory cahnnel
    //The amount of testing packets should be big enough
    //because sometimes shm can still work even zombie processes exist.
    for (i = 0; i < 2048; i++) {
        VPMU_SHM_POST_JOB(pkg, sim_num,
            cache_ref(ALL_PROC, ALL_PROC, 0, VPMU_PACKET_BARRIER, 0);
        );
    }
    // Wait for at most 10s to receive all the responses
    for (i = 0; i < 1000; i++) {
        usleep(10000);
        cnt = 0;
        for (j = 0; j < sim_num; j++) {
            if (shm_isBufferEmpty(cache_trace_buffer_t, j))
                cnt++;
        }
        //return when all set
        if (cnt == sim_num)
            return ;
    }
    //some simulators are not responding
    //You know, I'm not always going to be around to help you - Charlie Brown
    ERR_MSG("some cache simulators are not responding. \n" \
            "\tThis might because the some zombie cache simulator exists.\n" \
            "\tOr custom cache simulator were not executed after qemu's execution\n" \
            "\tTry \"killall cache-simulator\" to solve zombie processes.\n");
    exit(EXIT_FAILURE);
}

uint64_t VPMU_cache_init(void *ptr)
{
    int i;

    DBG("VPMU: cache init\n");
    register_cache_sims();

    //Initialize memory
    vpmu_cache_shm_page = vpmu_shm_allocate(
                          "vpmu_cache_ring_buffer",
                          VPMU_SHM_TOTAL_SIZE);
    memset(vpmu_cache_shm_page, 0, VPMU_SHM_TOTAL_SIZE);
    pkg = (Cache_Package *)vpmu_cache_shm_page;
    shm_bufferInit(cache_trace_buffer_t,
                   VPMU_SHM_BUF_DATA_SIZE / sizeof(Cache_Reference),
                   Cache_Reference,
                   CacheTraceBuffer,
                   vpmu_cache_shm_page + VPMU_SHM_ADDR_BUF);
    cache_trace_buffer_t->elems = vpmu_cache_shm_page + VPMU_SHM_ADDR_BUF_DATA;
    shm_token = vpmu_cache_shm_page + VPMU_SHM_ADDR_TOKEN;

    //Initialization for multi-simulator configurations
    parse_json_configs((char *)ptr);

    //Check boundary condition
    if (sizeof(Cache_Package) * sim_num > VPMU_SHM_ADDR_BUF) {
        ERR_MSG("Shared memory address map is not valid\n"
                "The size of \"package data\" times # of simulators\n"
                "is greater than the reserved space\n"
                "%d > %d\n",
                sizeof(Cache_Package) * sim_num,
                VPMU_SHM_ADDR_BUF);
        ERR_MSG("Suggestions: Reduce the number of concurrent cache simulators\n");
        exit(EXIT_FAILURE);
    }
    if (VPMU_SHM_ADDR_BUF + sizeof(CacheTraceBuffer) > VPMU_SHM_ADDR_BUF_DATA) {
        ERR_MSG("Shared memory address map is not valid\n"
                "The start address of \"ring buffer structure\" plus its size\n"
                "is greater than the start address of \"buffer data\"\n"
                "%d > %d\n",
                VPMU_SHM_ADDR_BUF + sizeof(CacheTraceBuffer),
                VPMU_SHM_ADDR_BUF_DATA);
        exit(EXIT_FAILURE);
    }

    //Loop through and fork simulators
    for (i = 0; i < sim_num; i++) {
        //Initialize mutex to zero, and set to process-shared
        sem_init(&pkg[i].cache_job_semaphore, 1, 0);
        if (pkg[i].program_path == NULL)
            fork_child_process(i, &pkg[i]);
        else
            fork_execute_process(i, &pkg[i]);
    }

    //Finally, hook pointers that VPMU required after all memory allocation are done
    GlobalVPMU.cache_data = &pkg[0].data;

    //Wait for other processes want to hook onto VPMU
    check_cache_sim_liveness();

    return 0;
}

static uint64_t sum(
    uint64_t array[MAX_LEVEL_OF_CACHE][VPMU_MAX_CPU_CORES][SIZE_OF_CACHE_INDEX],
    int level,
    int r_w_m_h)
{
    int i = 0, sum = 0;

    for (i = 0; i < VPMU_MAX_CPU_CORES; i++) {
        sum += array[level][i][r_w_m_h];
    }
    return sum;
}

uint64_t vpmu_L1_dcache_access_count(void)
{
    return sum(pkg[0].data.data_cache, L1_CACHE, CACHE_READ)
         + sum(pkg[0].data.data_cache, L1_CACHE, CACHE_WRITE)
         + GlobalVPMU.hot_dcache_read_count
         + GlobalVPMU.hot_dcache_write_count;
}

uint64_t vpmu_L1_dcache_miss_count(void)
{
    return sum(pkg[0].data.data_cache, L1_CACHE, CACHE_READ_MISS)
         + sum(pkg[0].data.data_cache, L1_CACHE, CACHE_WRITE_MISS);
}

uint64_t vpmu_L1_dcache_read_miss_count(void)
{
    return sum(pkg[0].data.data_cache, L1_CACHE, CACHE_READ_MISS);
}

uint64_t vpmu_L1_dcache_write_miss_count(void)
{
    return sum(pkg[0].data.data_cache, L1_CACHE, CACHE_WRITE_MISS);
}

//only for unified L2 cache now !!!!
uint64_t vpmu_L2_dcache_access_count(void)
{
    return sum(pkg[0].data.data_cache, L2_CACHE, CACHE_READ)
         + sum(pkg[0].data.data_cache, L2_CACHE, CACHE_WRITE);
}

//only for unified L2 cache now !!!!
uint64_t vpmu_L2_dcache_miss_count(void)
{
    return sum(pkg[0].data.data_cache, L2_CACHE, CACHE_READ_MISS)
         + sum(pkg[0].data.data_cache, L2_CACHE, CACHE_WRITE_MISS);
}

//only for unified L2 cache now !!!!
uint64_t vpmu_L2_dcache_read_miss_count(void)
{
    return sum(pkg[0].data.data_cache, L2_CACHE, CACHE_READ_MISS);
}

//only for unified L2 cache now !!!!
uint64_t vpmu_L2_dcache_write_miss_count(void)
{
    return sum(pkg[0].data.data_cache, L2_CACHE, CACHE_WRITE_MISS);
}

uint64_t vpmu_L1_icache_access_count(void)
{
    return sum(pkg[0].data.inst_cache, L1_CACHE, CACHE_READ)
         + GlobalVPMU.hot_icache_count;
}

uint64_t vpmu_L1_icache_miss_count(void)
{
    return sum(pkg[0].data.inst_cache, L1_CACHE, CACHE_READ_MISS);
}

uint64_t vpmu_sys_mem_access_cycle_count(void)
{
    return ((vpmu_L1_dcache_miss_count() + vpmu_L1_icache_miss_count())
          * pkg[0].cache_model.latency[L1_CACHE])
          + (vpmu_L2_dcache_miss_count()
          * pkg[0].cache_model.latency[L2_CACHE]);
}

uint64_t vpmu_io_mem_access_cycle_count(void)
{
    int levels = pkg[0].cache_model.levels;
    return GlobalVPMU.iomem_count * pkg[0].cache_model.latency[levels];
}

VPMU_Cache_Model vpmu_get_cache_config(void)
{
    return pkg[0].cache_model;
}

