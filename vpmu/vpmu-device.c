/*
 * Virtual Performance Monitor Unit
 * Copyright (c) 2009 PAS Lab, CSIE & GINM, National Taiwan University, Taiwan.
 */
#include <stdio.h>
#include <stdlib.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

#include "qemu/osdep.h"
#include "qapi/error.h"
#include "qemu-common.h"
#include "cpu.h"
#include "hw/sysbus.h"
#include "hw/arm/arm.h"
#include "hw/devices.h"
#include "net/net.h"
#include "sysemu/sysemu.h"
#include "exec/address-spaces.h"
#include "hw/block/flash.h"
#include "qemu/error-report.h"

//Define type "CPUState"
#include "exec/exec-all.h"

#include "vpmu.h"
#include "vpmu-module.h"
#ifdef CONFIG_VPMU_SET
#include "SET/efd.h"
#endif

/******************************
 * Global variables.
 *
 */
static char vpmu_dev_name[] = "NTU VPMU device";
/* QEMU realted device information, such as BUS, IRQ, VPMU strucutre. */
typedef struct vpmu_state
{
    SysBusDevice     busdev;
    qemu_irq         irq;
    MemoryRegion     iomem;
    VPMU             *VPMU;
    int              count;
    uint8_t          data[VPMU_IOMEM_SIZE];
}vpmu_state_t;

/* Global variables for VPMU counters. */
//extern struct VPMU GlobalVPMU;
vpmu_state_t vpmu_t;

// Saving QEMU's context for TLB and MMU use. (copy data from/to guest)
static ARMCPU vpmu_cpu_context[VPMU_MAX_CPU_CORES];
static CPUArchState vpmu_cpu_env;

//chiaheng
#ifdef CONFIG_VPMU_SET
//#include "vpmu_sampling.c"
#endif

/* Indicator of status of VPMU. */
volatile int VPMU_enabled = 0;

/* turn "on" simulator don't turn simulator off */
static void vpmu_simulator_turn_on(VPMU* vpmu, uint32_t mask)
{
    vpmu->timing_model |= mask;
}

void vpmu_model_setup(VPMU* vpmu, uint32_t model)
{
    vpmu->timing_model = model;

    switch(model)
    {
        case TIMING_MODEL_A:
            vpmu_simulator_turn_on(vpmu, VPMU_INSN_COUNT_SIM);
            break;
        case TIMING_MODEL_B:
            vpmu_simulator_turn_on(vpmu, VPMU_INSN_COUNT_SIM | VPMU_PIPELINE_SIM);
            break;
        case TIMING_MODEL_C:
            vpmu_simulator_turn_on(vpmu, VPMU_INSN_COUNT_SIM | VPMU_DCACHE_SIM
                    | VPMU_ICACHE_SIM | VPMU_PIPELINE_SIM);
            break;
        case TIMING_MODEL_D:
            vpmu_simulator_turn_on(vpmu, VPMU_PIPELINE_SIM | VPMU_DCACHE_SIM
                    | VPMU_ICACHE_SIM);
            break;
        case TIMING_MODEL_E:
            vpmu_simulator_turn_on(vpmu, VPMU_INSN_COUNT_SIM | VPMU_DCACHE_SIM
                    | VPMU_ICACHE_SIM | VPMU_BRANCH_SIM | VPMU_PIPELINE_SIM);
            break;
            //evo0209 : fixed for SET plus insn_count_sim
        case TIMING_MODEL_F:
            vpmu_simulator_turn_on(vpmu, VPMU_INSN_COUNT_SIM | VPMU_PIPELINE_SIM);
            break;
        case TIMING_MODEL_G:
            vpmu_simulator_turn_on(vpmu, VPMU_INSN_COUNT_SIM | VPMU_DCACHE_SIM
                    | VPMU_ICACHE_SIM | VPMU_PIPELINE_SIM);
            break;
        default:
            DBG("function \"%s\" : nothing to setup\n",__FUNCTION__);
            break;
    }
}

/* paslab : monitoring init */
static uint64_t special_read(void *opaque,
        hwaddr addr,
        unsigned size)
{
    uint64_t ret = 0;

    CONSOLE_LOG("read qemu device at addr=0x%lx\n", addr);
    switch(addr) {
        case VPMU_MMAP_GETCLOCKTIME:
            GlobalVPMU.eet_ptr[0] = 0;
            GlobalVPMU.eet_ptr[1] = 0;

            break;
        case VPMU_MMAP_GETTIMER:
            GlobalVPMU.eet_ptr[0] = vpmu_estimated_execution_time_ns();
            ret = GlobalVPMU.eet_ptr[1];
            break;
        default:
            DBG("read unknown address 0x%lx of vpd\n", addr);
    }

    return ret;
}

uintptr_t vpmu_get_phy_addr_global(void *ptr, uintptr_t vaddr)
{
    const int READ_ACCESS_TYPE = 0;
    uintptr_t paddr = 0;
    CPUState *cpu_state = (CPUState *)ptr;
    CPUArchState *cpu_env = (CPUArchState*)cpu_state->env_ptr;
    int mmu_idx = cpu_mmu_index(cpu_env, false);
    int index;
    target_ulong tlb_addr;

    index = (vaddr >> TARGET_PAGE_BITS) & (CPU_TLB_SIZE - 1);
redo:
    tlb_addr = cpu_env->tlb_table[mmu_idx][index].addr_read;
    if ((vaddr & TARGET_PAGE_MASK) == (tlb_addr & (TARGET_PAGE_MASK | TLB_INVALID_MASK))) {
        if (tlb_addr & ~TARGET_PAGE_MASK) {
            ERR_MSG("should not access IO currently\n");
        } else {
            uintptr_t addend;
            addend = cpu_env->tlb_table[mmu_idx][index].addend;
            paddr = (uintptr_t)(vaddr + addend);
        }
    } else {
        tlb_fill(cpu_state, vaddr, READ_ACCESS_TYPE, mmu_idx, GETPC());
        goto redo;
    }

    return paddr;
}

size_t vpmu_copy_from_guest(void *dst, uintptr_t src, const size_t size, void *cs)
{
    size_t left_size = size;

    while (left_size > 0) {
        uintptr_t phy_addr = vpmu_get_phy_addr_global(cs, src);

        if (phy_addr) {
            size_t valid_len = TARGET_PAGE_SIZE - (phy_addr & ~TARGET_PAGE_MASK);
            if (valid_len > left_size) valid_len = left_size;
            memcpy(dst, (void *)phy_addr, valid_len);
            dst = ((uint8_t*)dst) + valid_len;
            src += valid_len;
            left_size -= valid_len;
        }
    }

    if (left_size) {
        ERR_MSG("Page fault in copy from guest\n");
    }

    return (size - left_size);
}

/* paslab : addr was 32-bit addr in 0.9.1 but offset in 0.10.5 ??? */
//extern CharDriverState *serial_hds[MAX_SERIAL_PORTS];
static void special_write(void *opaque,
        hwaddr addr,
        uint64_t value,
        unsigned size)
{
    static int find_entry = 0;
    VPMU *vpmu=((vpmu_state_t *)opaque)->VPMU;
#ifdef CONFIG_VPMU_SET
    uintptr_t paddr = 0;
    char *buffer;
    static int buffer_size = 0;
    FILE *fp;
#endif

    CONSOLE_LOG("write qemu device at addr=0x%lx value=%ld\n", addr, value);

#if 0 //Bug solved.
    //This is a test code to read user data in guest virtual address from host
    if (addr == 100 * 4) {
        ERR_MSG("VA:%lx\n", value);
        uintptr_t guest_vaddr = vpmu_get_phy_addr_global(GlobalVPMU.cs, value);
        ERR_MSG("PA:%lx\n", guest_vaddr);
        ERR_MSG("value:%lx\n", *(unsigned long int *)guest_vaddr);
        return ;
    }
#endif

    switch(addr) {
        case VPMU_MMAP_ENABLE:
            if(value == 0 || (value <= 10 && value >= 2))
            {
                VPMU_reset();
                VPMU_enabled = 1;

                tic(&(vpmu->start_time));
                switch(value)
                {
                    case 2:
                        vpmu_model_setup(vpmu, TIMING_MODEL_A);
                        vpmu_simulator_status(vpmu, 0);
                        break;
                    case 3:
                        vpmu_model_setup(vpmu, TIMING_MODEL_B);
                        vpmu_simulator_status(vpmu, 0);
                        break;
                    case 4:
                        vpmu_model_setup(vpmu, TIMING_MODEL_C);
                        vpmu_simulator_status(vpmu, 0);
                        break;
                    case 5:
                        vpmu_model_setup(vpmu, TIMING_MODEL_D);
                        vpmu_simulator_status(vpmu, 0);
                        break;
                    case 6:
                        vpmu_model_setup(vpmu, TIMING_MODEL_E);
                        vpmu_simulator_status(vpmu, 0);
                        break;
                        //tianman
                    case 7:
#ifdef CONFIG_VPMU_SET
//                        int ii;
//                        for (ii = 0; ii < 512; ii++) {
//                            if (pid_base[ii][0] == 1)
//                                CONSOLE_LOG("PID: %d, Process_name: %s\n",
//                                        ii, pid_name_base[ii]);
//                        }
#endif
                        break;
                    case 8:
                        vpmu_model_setup(vpmu, TIMING_MODEL_E);
                        //JIT model selection
                        vpmu_simulator_turn_on(vpmu, VPMU_MODEL_SELECT);
                        vpmu_simulator_status(vpmu, 0);
                        break;
                    default:
                        CONSOLE_LOG("Use default timing model\n");
                        vpmu_model_setup(vpmu, TIMING_MODEL_A);
                        vpmu_simulator_status(vpmu, 0);
                        break;
                }
            }
            else if(value == 1)
            {
                VPMU_sync();
                toc(&(vpmu->start_time), &(vpmu->end_time));

                if((vpmu->dump_fp) != NULL)
                    fclose(vpmu->dump_fp);
                vpmu->dump_fp = NULL;
                VPMU_dump_result();

                VPMU_enabled = 0;
#ifdef CONFIG_VPMU_VFP
                //	FILE_LOG("Before print_vfp_count \n");
                print_vfp_count();
#endif

            } else if(value == 11) {
                //unsigned long long time = vpmu_wall_clock_period(vpmu);
                VPMU_sync();
                toc(&(vpmu->start_time), &(vpmu->end_time));
                VPMU_dump_result();

#ifdef CONFIG_VPMU_VFP
                //	FILE_LOG("Before print_vfp_count \n");
                print_vfp_count();
#endif
            } else if (value == 16) {
                CONSOLE_LOG("total power: %d\n", vpmu_vpd_total_power());
                CONSOLE_LOG("  mpu power: %d\n", vpmu_vpd_mpu_power());
            }
            break;
        case VPMU_MMAP_BYPASS_ISR_ADDR:
            if (find_entry < NUM_FIND_SYMBOLS)
            {
                /* timer irq entry of target OS */
                ISR_addr[find_entry] = value;
                DBG( "ISR_addr[%d] = %lx\n", find_entry, value);
                find_entry++;
            }
            break;
        case 0x0011:
            CONSOLE_LOG("Total instruction count : %" PRIu64 " \n",
                    vpmu_total_insn_count());
            break;
        case 0x0012:
            CONSOLE_LOG("Total branch instruction count : %" PRIu64 " \n",
                    vpmu_branch_insn_count());
            break;
        case 0x0013:
            CONSOLE_LOG("Data memory access count : %" PRIu64 " \n",
                    vpmu_L1_dcache_access_count());
            break;
        case 0x0014:
            CONSOLE_LOG("Cycle count : %" PRIu64 " \n", vpmu_cycle_count());
            break;
        case 0x0015:
            CONSOLE_LOG("Estimated execution time : %u\n",
                    vpmu_estimated_execution_time());
            break;
        case VPMU_MMAP_POWER_ENABLE:
            CONSOLE_LOG("VPMU status : %s\n", VPMU_enabled ? "enable" : "disable");
            break;
#ifdef CONFIG_VPMU_SET
        case VPMU_MMAP_SET_PROC_NAME:
            if (value != 0) {
                // Copy the whole CPU context including TLB Table and MMU registers for
                // VPMU's use.
                memcpy(&vpmu_cpu_env, ((CPUState *)GlobalVPMU.cs)->env_ptr,
                       sizeof(CPUArchState));
                memcpy(&vpmu_cpu_context[0], GlobalVPMU.cs, sizeof(ARMCPU));
                CPUState *cpu_state_ptr = CPU(&vpmu_cpu_context[0]);
                cpu_state_ptr->env_ptr = &vpmu_cpu_env;

                paddr = vpmu_get_phy_addr_global(&vpmu_cpu_context[0], value);
                DBG("SET trace process name:%s\n", (char *)paddr);
                strcpy(GlobalVPMU.traced_process_name, (char *)paddr);
            }
            break;
        case VPMU_MMAP_SET_PROC_SIZE:
            buffer_size = value;
            break;

        case VPMU_MMAP_SET_PROC_BIN:
            if (buffer_size != 0) {
                buffer = (char *)malloc(buffer_size);
                if (buffer == NULL) {
                    ERR_MSG("Can not allocate memory\n");
                    exit(EXIT_FAILURE);
                }
                vpmu_copy_from_guest(buffer, value, buffer_size, GlobalVPMU.cs);
                fp = fopen("/tmp/vpmu-traced-bin", "wb");
                if (fp != NULL) {
                    fwrite(buffer, buffer_size, 1, fp);
                }
                fclose(fp);
                free(buffer);
                buffer_size = 0;
                buffer = NULL;
                EFD *efd = efd_open_elf((char *)"/tmp/vpmu-traced-bin");
                dump_symbol_table(efd);
                efd_close(efd);
            }
            break;
#endif
        default:
            CONSOLE_LOG("write 0x%ld to unknown address 0x%lx of vpd\n", value, addr);
    }

    // Preventing unused warnings
    (void)vpmu_cpu_context;
    (void)vpmu_cpu_env;
}

static const MemoryRegionOps vpmu_dev_ops = {
    .read = special_read,
    .write = special_write,
    .endianness = DEVICE_NATIVE_ENDIAN,
};

static int vpmu_dev_init_sysbus(SysBusDevice *dev)
{
    vpmu_state_t *status = OBJECT_CHECK(vpmu_state_t,
            dev,
            vpmu_dev_name);

    status->VPMU = &GlobalVPMU;

    memory_region_init_io(&status->iomem,
            OBJECT(status),
            &vpmu_dev_ops,
            status,
            vpmu_dev_name,
            VPMU_IOMEM_SIZE);

    sysbus_init_mmio(dev,
            &status->iomem);
    return 0;
}

void vpmu_dev_init(uint32_t);
void vpmu_dev_init(uint32_t base)
{
    CONSOLE_LOG("init vpmu device on addr 0x%x. \n", base);

    sysbus_create_simple(vpmu_dev_name,
            base,
            NULL);
}

static const VMStateDescription vpmu_vmstate = {
    .name = vpmu_dev_name,
    .version_id = 1,
    .minimum_version_id = 1,
    .minimum_version_id_old = 1,
    .fields = (VMStateField[]) {
        VMSTATE_UINT8_ARRAY(data,
                vpmu_state_t,
                VPMU_IOMEM_SIZE),
        VMSTATE_END_OF_LIST()
    }
};

static void vpmu_dev_reset(DeviceState *dev)
{
}

static void vpmu_dev_class_init(ObjectClass *class, void *data)
{
    DeviceClass *dc = DEVICE_CLASS(class);
    SysBusDeviceClass *sub = SYS_BUS_DEVICE_CLASS(class);

    sub->init = vpmu_dev_init_sysbus;
    dc->vmsd = &vpmu_vmstate;
    dc->reset = vpmu_dev_reset;
}

static const TypeInfo vpmu_dev_info = {
    .name          = vpmu_dev_name,
    .parent        = TYPE_SYS_BUS_DEVICE,
    .instance_size = sizeof(vpmu_state_t),
    .class_init    = vpmu_dev_class_init,
};

static void vpmu_dev_register_types(void)
{
    type_register_static(&vpmu_dev_info);
}

type_init(vpmu_dev_register_types);
