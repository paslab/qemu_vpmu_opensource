#include "vpmu.h"
#include "efd.h"

FILE *vpmu_log_file = NULL;
struct VPMU GlobalVPMU;
#ifdef CONFIG_ANDROID
struct android_dev_stat adev_stat = {
    .cpu_util       = 0,
    .lcd_brightness = NULL,
    .audio_buf      = NULL,
    .audio_on       = 0, // 0:off others:on
    .net_mode       = 1, // 0:off 1:Wifi 2:3G
    .net_on         = 0, // 0:off 1:on
    .net_packets    = NULL,
    .gps_status     = NULL, // 0:off 1:idle 2:active
    .battery_sim    = 1, // 0:off 1:on
    .energy_left    = 0, // mJ
    .bat_ac_online  = NULL, // 0:offline 1:online
    .bat_capacity   = NULL,
};
#endif

void *vpmu_shm_allocate(const char *name, int size)
{
    int  fd;
    void *addr;
    char full_path[256];

    DBG("shm_open: %s\n", name);
    if (name[0] == '/')
        ERR_MSG("vpmu_shm_allocate: name contains '/'\n");

    append_string(full_path, "/dev/shm/");
    append_string(full_path, (char *)name);
    if (access(full_path, F_OK ) != -1) {
        // file exists
        shm_unlink(name);
        //return -1;
    }

    /* Create a new memory object */
    fd = shm_open(name, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR | S_IXUSR);
    if (fd == -1) {
        ERR_MSG("shm open failed: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    /* Set the memory object's size */
    if (ftruncate(fd, size) == -1) {
        ERR_MSG("ftruncate: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    /* Map the memory object */
    addr = mmap(0, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (addr == MAP_FAILED) {
        ERR_MSG("mmap failed: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    DBG("shm map addr is 0x%lx\n", (uint64_t)addr);

    return addr;
}

void append_string(char *head, const char *tail)
{
    int i, j;

    for (i = 0; i < 256 && head[i] != '\0'; i++);
    for (j = 0; i < 256 && tail[j] != '\0'; i++, j++)
        head[i] = tail[j];
    head[i] = '\0';
}

static void vpmu_initiate_attached_simulators(struct json_object *root)
{
    VPMU_Simulator *sim = GlobalVPMU.sims;
    int i;
    const char *str;

    for (i = 0; i < GlobalVPMU.num_of_sims; i++) {
        if(sim[i].fun[SIM_FUN_INIT] != NULL) {
            switch(sim[i].type) {
                case SIM_TYPE_CACHE:
                    str = get_str_object(root, "cache");
                    break;
                case SIM_TYPE_BRANCH:
                    str = get_str_object(root, "branch");
                    break;
                default:
                    str = NULL;
            }
            DBG("VPMU simulator: init \"%s\" id = %d\n", sim[i].description, i);
            sim[i].fun[SIM_FUN_INIT]((void *)str);
        }
    }
}

static void vpmu_sync_attached_simulators(void)
{
    VPMU_Simulator *sim = GlobalVPMU.sims;
    int i;

    for (i = 0; i < GlobalVPMU.num_of_sims; i++) {
        if (sim[i].fun[SIM_FUN_SYNC] != NULL) {
            DBG("VPMU simulator: sync \"%s\" id = %d\n", sim[i].description, i);
            sim[i].fun[SIM_FUN_SYNC](NULL);
        }
    }
}

static void vpmu_release_attached_simulators(void)
{
    VPMU_Simulator *sim = GlobalVPMU.sims;
    int i;

    for (i = 0; i < GlobalVPMU.num_of_sims; i++) {
        if (sim[i].fun[SIM_FUN_FREE] != NULL) {
            DBG("VPMU simulator: release \"%s\" id = %d\n", sim[i].description, i);
            sim[i].fun[SIM_FUN_FREE](NULL);
        }
    }
}

static uint64_t null_func(void)
{
    //CONSOLE_LOG("(VPMU): No such hardware event (%s return 0)\n", __FUNCTION__);
    return 0;
}

static int vpmu_attach_simulator(
        enum simulator_type type, 
        char* description, 
        uint64_t (*cb[5])(void), 
        int number_of_event, 
        uint64_t init(void *), 
        uint64_t free(void *), 
        uint64_t reset(void *), 
        uint64_t sync(void *),
        uint64_t dump(void *)
        )
{
    int id = GlobalVPMU.num_of_sims;
    int i;
    GlobalVPMU.num_of_sims++;

    DBG("\tVPMU simulator: %s registered ID = %d event NO. = 0x%x to 0x%x\n", 
        description, 
        id, 
        (id * 5),
        (id * 5) + number_of_event - 1);

    GlobalVPMU.sims[id].type = type;
    strcpy(GlobalVPMU.sims[id].description, description);
    GlobalVPMU.sims[id].fun[SIM_FUN_INIT] = (init) ? init : NULL;
    GlobalVPMU.sims[id].fun[SIM_FUN_FREE] = (free) ? free : NULL;
    GlobalVPMU.sims[id].fun[SIM_FUN_RESET] = (reset) ? reset : NULL;
    GlobalVPMU.sims[id].fun[SIM_FUN_SYNC] = (sync) ? sync : NULL;
    GlobalVPMU.sims[id].fun[SIM_FUN_DUMP] = (dump) ? dump : NULL;

    for(i = 3;i < number_of_event;i++) {
        GlobalVPMU.sims[id].cb[i] = cb[i];
    }
    for(; i < 8; i++) {
        GlobalVPMU.sims[id].cb[i] = null_func;
    }
    return id++;
}

void VPMU_update_slowdown_ratio(double value)
{
    static int count = 0;
    if (count == 0) {
        GlobalVPMU.slowdown_ratio = value;
        count++;
    }
}

char *load_file(const char* filename)
{
    FILE *fp = fopen(filename, "rt");
    size_t length;
    char *data;

    if(!fp) return NULL;

    // get file length
    fseek(fp, 0, SEEK_END);
    length = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    // read program source
    data = (char *)malloc(length + 1);
    length = fread(data, sizeof(char), length, fp);
    data[length] = '\0';

    return data;
}

int get_json_int(struct json_object *root, char *field)
{
    struct json_object *element = NULL;

    if (!json_object_object_get_ex(root, field, &element))
        ERR_MSG("Field \"%s\" does not exist\n", field);
    return json_object_get_int(element);
}

int64_t get_json_int64(struct json_object *root, char *field)
{
    struct json_object *element = NULL;

    if (!json_object_object_get_ex(root, field, &element))
        ERR_MSG("Field \"%s\" does not exist\n", field);
    return (int64_t)json_object_get_int64(element);
}

const char *get_json_str(struct json_object *root, char *field)
{
    struct json_object *element = NULL;

    if (!json_object_object_get_ex(root, field, &element)) {
        ERR_MSG("Field \"%s\" does not exist\n", field);
        return NULL;
    }

    return json_object_get_string(element);
}

const char *get_str_object(struct json_object *root, char *field)
{
    struct json_object *element = NULL;

    if (!json_object_object_get_ex(root, field, &element)) {
        ERR_MSG("Field \"%s\" does not exist\n", field);
        return NULL;
    }

    return json_object_to_json_string(element);
}

int check_json_field(struct json_object *root, char *field)
{
    struct json_object *element = NULL;

    if (!json_object_object_get_ex(root, field, &element)) {
        return 0;
    }

    return 1;
}

#ifdef CONFIG_VPMU_VFP
static void load_vfp_config(const char *filename)
{
    char *str = NULL;
    struct json_object *root = NULL, *element = NULL;

    str = load_file(filename);
    if (str == NULL) {
        CONSOLE_LOG("Could not find vfp config file:%s\n", filename);
        exit(EXIT_FAILURE);
    }
    root = json_tokener_parse(str);
    free(str);

    //Load cycles from json
    if (!json_object_object_get_ex(root, "cycle", &element))
        ERR_MSG("Field \"cycle\" does not exist\n");
    json_object_object_foreach(element, key0, val0) {
        arm_vfp_instr_time[get_index_of_arm_vfp_inst(key0)] = json_object_get_int(val0);
    }
    //Load latencies from json
    if (!json_object_object_get_ex(root, "latency", &element))
        ERR_MSG("Field \"latency\" does not exist\n");
    json_object_object_foreach(element, key1, val1) {
        arm_vfp_latency[get_index_of_arm_vfp_inst(key1)] = json_object_get_int(val1);
    }
}
#endif

void set_arm_inst_cycles(struct json_object *root)
{
    struct json_object *element = NULL;

    if (!json_object_object_get_ex(root, "instruction", &element))
        ERR_MSG("Field \"instruction\" does not exist\n");

    json_object_object_foreach(element, key, val) {
        arm_instr_time[get_index_of_arm_inst(key)] = json_object_get_int(val);
    }
}

#ifdef CONFIG_VPMU_SET
static void vpmu_process_tracking_init()
{
    char kernel_path[1024] = {0};
    char *path = getenv("VPMU_SET_PATH");

    if (path == NULL || path[0] == 0 || access(path, F_OK)) {
        ERR_MSG("SET device: init failed - invalid environment variable \"VPMU_SET_PATH\"\n");
        ERR_MSG("VPMU_SET_PATH:%s\n", path);
        exit(EXIT_FAILURE);
        return;
    }
    else {
        snprintf(kernel_path, sizeof(kernel_path), "%s/vmlinux", path);
        if (access(kernel_path, R_OK)) {
            ERR_MSG("Could not find \"vmlinux\" in %s\n", path);
            ERR_MSG("SET tracking would not work\n\n");
            exit(EXIT_FAILURE);
        }
    }

    GlobalVPMU.switch_to_addr = 0;
    GlobalVPMU.execve_addr = 0;
    /* Get special kernel functions (used for dynamic instrumention of Linux kernel) */
    /* Using EFD to obtain the information about object file */
    EFD *efd = efd_open_elf(kernel_path);
    if (efd == NULL)
        return ;

    /* Push special functions into hash table */
    for (int i = 0; i < efd_get_sym_num(efd); i++) {
        if ((efd_get_sym_type(efd, i) == STT_FUNC)
                && (efd_get_sym_vis(efd, i) == STV_DEFAULT)
                && (efd_get_sym_shndx(efd, i) != SHN_UNDEF)) {
#define THUMB_INSN_MASK 0xfffffffe
            uint32_t vaddr = efd_get_sym_value(efd, i) & THUMB_INSN_MASK;
            char *funcName = efd_get_sym_name(efd, i);
            if (strcmp(funcName, "do_execve") == 0)
                GlobalVPMU.execve_addr = vaddr;
            if (strcmp(funcName, "__switch_to") == 0)
                GlobalVPMU.switch_to_addr = vaddr;
            if (strcmp(funcName, "do_exit") == 0)
                GlobalVPMU.exit_addr = vaddr;
            if (strcmp(funcName, "wake_up_new_task") == 0)
                GlobalVPMU.wake_up_new_task_addr = vaddr;
            if (strcmp(funcName, "do_fork") == 0)
                GlobalVPMU.do_fork_addr = vaddr;
        }
    }
}
#endif

static struct json_object * VPMU_load_json(void)
{
    char *str = NULL;
    char *config_file_path = getenv("VPMU_CONFIG");
    char filename[256] = {0};
    struct json_object *root = NULL, *element = NULL;

    if (config_file_path == NULL) {
        ERR_MSG("Environment variable \"VPMU_CONFIG\" does not set yet!\n");
        exit(EXIT_FAILURE);
    }
    //Prepare path and load file for VPMU config
    strcpy(filename, config_file_path);
    append_string(filename, "/default.json");
    str = load_file(filename);
    if (str == NULL) {
        ERR_MSG("Could not find config file:%s\n", filename);
        ERR_MSG("Please make sure you have run \"source ./setenv.sh\" " \
                "or \"export VPMU_CONFIG=[PATH TO CONFIG FILE]\"\n");
        exit(EXIT_FAILURE);
    }
    root = json_tokener_parse(str);
    free(str);

#ifdef CONFIG_VPMU_SET
    //Setup SET
    if (!json_object_object_get_ex(root, "SET", &element)) {
        ERR_MSG("Field \"SET\" does not exist\n");
        exit(EXIT_FAILURE);
    }
    if (json_object_object_get_ex(element, "process_name", NULL)) {
        strcpy(GlobalVPMU.traced_process_name, get_json_str(element, "process_name"));
    }
    GlobalVPMU.traced_process_model = get_json_int(element, "timing_model") << 28;
#endif

    //Setup CPU models
    if (!json_object_object_get_ex(root, "cpu_model", &element)) {
        ERR_MSG("Field \"cpu_model\" does not exist\n");
        exit(EXIT_FAILURE);
    }
    strcpy(GlobalVPMU.cpu_model.name, get_json_str(element, "name"));
    GlobalVPMU.cpu_model.frequency  = get_json_int(element, "frequency");
    GlobalVPMU.cpu_model.dual_issue = get_json_int(element, "dual_issue");
    //We read num_cores from command line argument. DO NOT set the value here!

    //Set up GPU model if there's one
    if (json_object_object_get_ex(root, "gpu_model", &element)) {
        strcpy(GlobalVPMU.gpu_model.name, get_json_str(element, "name"));
        GlobalVPMU.gpu_model.frequency  = get_json_int(element, "frequency");
    }

    set_arm_inst_cycles(root);

#ifdef CONFIG_VPMU_VFP
    strcpy(filename, config_file_path);
    append_string(filename, "/");
    append_string(filename, get_json_str(root, "vfp_instruction_file"));
    load_vfp_config(filename);
#endif

    return root;
}

static void vpmu_sync_configs_from_simulators(void)
{
    GlobalVPMU.cache_model = vpmu_get_cache_config();
}

void VPMU_sync(void)
{
    vpmu_sync_attached_simulators();
}

void VPMU_reset(void)
{
    int i = 0;
    GlobalVPMU.need_tb_flush = 1;
    GlobalVPMU.USR_icount = 0;
    GlobalVPMU.SYS_icount = 0;
    GlobalVPMU.FIQ_icount = 0;
    GlobalVPMU.IRQ_icount = 0;
    GlobalVPMU.SVC_icount = 0;
    GlobalVPMU.ABT_icount = 0;
    GlobalVPMU.UND_icount = 0;
    GlobalVPMU.sys_call_icount = 0;
    GlobalVPMU.sys_rest_icount=0;

    GlobalVPMU.USR_ldst_count = 0;
    GlobalVPMU.SVC_ldst_count = 0;
    GlobalVPMU.IRQ_ldst_count = 0;
    GlobalVPMU.sys_call_ldst_count = 0;
    GlobalVPMU.sys_rest_ldst_count = 0;

    GlobalVPMU.USR_load_count = 0;
    GlobalVPMU.SVC_load_count = 0;
    GlobalVPMU.IRQ_load_count = 0;
    GlobalVPMU.sys_call_load_count = 0;
    GlobalVPMU.sys_rest_load_count = 0;

    GlobalVPMU.USR_store_count = 0;
    GlobalVPMU.SVC_store_count = 0;
    GlobalVPMU.IRQ_store_count = 0;
    GlobalVPMU.sys_call_store_count = 0;
    GlobalVPMU.sys_rest_store_count = 0;

    GlobalVPMU.timer_interrupt_return_pc = 0;
    GlobalVPMU.state = 0;
    GlobalVPMU.timer_interrupt_exception = 0;

    GlobalVPMU.ticks = 0;
    GlobalVPMU.cpu_idle_time_ns = 0;
    GlobalVPMU.eet_ns = 0;
    GlobalVPMU.last_ticks = 0;
    GlobalVPMU.epc_mpu = 0;
    GlobalVPMU.branch_count = 0;
    GlobalVPMU.iomem_count = 0;
    GlobalVPMU.iomem_access_flag = 0;

    GlobalVPMU.timing_model = 0;

    GlobalVPMU.swi_fired = 0;
    GlobalVPMU.swi_fired_pc = 0;
    GlobalVPMU.swi_enter_exit_count = 0;

    GlobalVPMU.dump_fp = NULL;
    /*Modelsel*/
    GlobalVPMU.hot_dcache_read_count = 0;
    GlobalVPMU.hot_dcache_write_count = 0;
    GlobalVPMU.hot_icache_count = 0;
    GlobalVPMU.total_tb_visit_count = 0;
    GlobalVPMU.cold_tb_visit_count = 0;
    GlobalVPMU.hot_tb_visit_count = 0;

#ifdef CONFIG_VPMU_VFP
    GlobalVPMU.VFP_BASE=0;
    for(i = 0; i < ARM_VFP_INSTRUCTION_TOTAL_COUNTS; i++){
        GlobalVPMU.VFP_count[i]=0;
    }
#endif
    for(i = 0; i< ARM_INSTRUCTION_TOTAL_COUNTS; i++){
        GlobalVPMU.ARM_count[i]=0;
    }

    GlobalVPMU.reduce_ticks = 0;

    for(i = 0; i < GlobalVPMU.num_of_sims; i++) {
        if (GlobalVPMU.sims[i].fun[SIM_FUN_RESET])
            GlobalVPMU.sims[i].fun[SIM_FUN_RESET](NULL);
    }
}


static void vpmu_init_core()
{
    uint64_t (*cb[5])(void); //function pointers for callback functions of simulators
    struct json_object *root;
    CONSOLE_LOG("VPMU configurations:\n");

    GlobalVPMU.mpu_freq = GlobalVPMU.cpu_model.frequency;
    GlobalVPMU.mpu_power = ((0x10000011 >> 24) - 0x0b) * 4.21806 + 156.97284;
    //evo0209 : set the slowdown ratio 1 by default
    VPMU_update_slowdown_ratio(1);
    CONSOLE_LOG("\tDivide frequency ratio: %.2f\n", GlobalVPMU.slowdown_ratio);

    /* initialize android devices state pointer */
#ifdef CONFIG_ANDROID
    GlobalVPMU.adev_stat_ptr = &adev_stat;
#endif
    cb[0] = vpmu_L1_dcache_access_count;
    cb[1] = vpmu_L1_dcache_miss_count;
    cb[2] = vpmu_L1_dcache_read_miss_count;
    cb[3] = vpmu_L1_dcache_write_miss_count;
    vpmu_attach_simulator(
            SIM_TYPE_CACHE, "Cache Simulator", 
            cb, 4, 
            VPMU_cache_init, 
            VPMU_cache_free, 
            VPMU_cache_reset, 
            VPMU_cache_sync,
            VPMU_cache_dump
            );

    cb[0] = vpmu_branch_predict_correct;
    cb[1] = vpmu_branch_predict_wrong;
    vpmu_attach_simulator(
            SIM_TYPE_BRANCH, "Branch Predictor", 
            cb, 2, 
            VPMU_branch_init, 
            VPMU_branch_free, 
            VPMU_branch_reset, 
            VPMU_branch_sync,
            VPMU_branch_dump
            );

    root = VPMU_load_json();
#ifdef CONFIG_VPMU_SET
    vpmu_process_tracking_init();
#endif

    vpmu_initiate_attached_simulators(root);
    vpmu_sync_configs_from_simulators();

    //After this line, the configs from simulators are synced!!!
    CONSOLE_LOG("\tCPU model    : %s\n", GlobalVPMU.cpu_model.name);
    CONSOLE_LOG("\t# cores      : %d\n", GlobalVPMU.cpu_model.num_cores);
    CONSOLE_LOG("\tfrequency    : %"PRIu64"\n", GlobalVPMU.cpu_model.frequency);
    CONSOLE_LOG("\tdual issue   : %s\n", GlobalVPMU.cpu_model.dual_issue ? "y" : "n");
    CONSOLE_LOG("\tCache model  : %s\n", GlobalVPMU.cache_model.name);
    CONSOLE_LOG("\t# levels     : %d\n", GlobalVPMU.cache_model.levels);
    CONSOLE_LOG("\tlatency      : \n");
    for (int i = GlobalVPMU.cache_model.levels; i > 0; i--) {
        CONSOLE_LOG("\t    L%d  : %d\n", i, GlobalVPMU.cache_model.latency[i]);
    }

    // Create the performance monitoring thread, which dump the selected events.
#ifdef CONFIG_VPMU_SET
    //pthread_create( &perf_mntr_thread, NULL, vpmu_perf_monitoring_thread, s->VPMU);
#endif
}


/* opt == 0 (print all the status) return value is 0
 * opt != 0 (return whether the specific simulator is on
 * return 1(on) 0(off) */
int8_t vpmu_simulator_status(VPMU* vpmu, uint32_t opt)
{
    unsigned int ctrl_reg = vpmu->timing_model;
    if(opt == 0) {
        ctrl_reg & VPMU_INSN_COUNT_SIM ?
            CONSOLE_LOG("o : "):CONSOLE_LOG("x : ");
        CONSOLE_LOG("Instruction Simulation\n");

        ctrl_reg & VPMU_DCACHE_SIM ?
            CONSOLE_LOG("o : "):CONSOLE_LOG("x : ");
        CONSOLE_LOG("Data Cache Simulation\n");

        ctrl_reg & VPMU_ICACHE_SIM ?
            CONSOLE_LOG("o : "):CONSOLE_LOG("x : ");
        CONSOLE_LOG("Insn Cache Simulation\n");

        ctrl_reg & VPMU_BRANCH_SIM ?
            CONSOLE_LOG("o : "):CONSOLE_LOG("x : ");
        CONSOLE_LOG("Branch Predictor Simulation\n");

        ctrl_reg & VPMU_PIPELINE_SIM ?
            CONSOLE_LOG("o : "):CONSOLE_LOG("x : ");
        CONSOLE_LOG("Pipeline Simulation\n");

        ctrl_reg & VPMU_MODEL_SELECT ?
            CONSOLE_LOG("o : "):CONSOLE_LOG("x : ");
        CONSOLE_LOG("JIT Model Selection\n");

        ctrl_reg & VPMU_SAMPLING ?
            CONSOLE_LOG("o : "):CONSOLE_LOG("x : ");
        CONSOLE_LOG("VPMU sampling mechanism\n");
        return 0;
    }
    else {
        switch(opt)
        {
            case VPMU_INSN_COUNT_SIM:
                if(ctrl_reg & VPMU_INSN_COUNT_SIM)
                    return 1;
                else
                    return 0;
            case VPMU_DCACHE_SIM:
                if(ctrl_reg & VPMU_DCACHE_SIM)
                    return 1;
                else
                    return 0;
            case VPMU_ICACHE_SIM:
                if(ctrl_reg & VPMU_ICACHE_SIM)
                    return 1;
                else
                    return 0;
            case VPMU_BRANCH_SIM:
                if(ctrl_reg & VPMU_BRANCH_SIM)
                    return 1;
                else
                    return 0;
            case VPMU_PIPELINE_SIM:
                if(ctrl_reg & VPMU_PIPELINE_SIM)
                    return 1;
                else
                    return 0;
            case VPMU_SAMPLING:
                if(ctrl_reg & VPMU_SAMPLING)
                    return 1;
                else
                    return 0;
            default:
                DBG("(VPMU) No such simulator\n");
                return -1;
        }
    }
}

//TODO fix the timing correctness from the cache simulation
void VPMU_dump_result()
{
    VPMU* vpmu = &GlobalVPMU;

    VPMU_cache_sync(NULL);
    VPMU_branch_sync(NULL);
    toc(&(vpmu->start_time), &(vpmu->end_time));

    CONSOLE_LOG("==== Program Profile ====\n\n");
    CONSOLE_LOG("   === QEMU/ARM ===\n");
    VPMU_DUMP_READABLE_MSG(vpmu);
}

void enable_QEMU_log()
{
    int mask = (1 << 1); //This means CPU_LOG_TB_IN_ASM from cpu-all.h

#ifdef CONFIG_QEMU_VERSION_0_15
    loglevel |= mask;
    logfile = vpmu_log_file;
#else
    qemu_loglevel |= mask;
    qemu_logfile = vpmu_log_file;
#endif
}

void disable_QEMU_log()
{
    int mask = (1 << 1); //This means CPU_LOG_TB_IN_ASM from cpu-all.h

#ifdef CONFIG_QEMU_VERSION_0_15
    loglevel &= ~mask;
    logfile = NULL;
#else
    qemu_loglevel &= ~mask;
    qemu_logfile = NULL;
#endif
}

void vpmu_core_init(int argc, char **argv)
{
    if (vpmu_log_file == NULL)
        vpmu_log_file = fopen("/tmp/vpmu.log", "w");

    //this would let print system support comma.
    setlocale(LC_NUMERIC, "");
    //enable_QEMU_log();
    GlobalVPMU.thpool = thpool_init(1);
    DBG("VPMU: Thread Pool Initialized");
    vpmu_init_core();
    CONSOLE_LOG("VPMU: Initialized\n");
}

static void __attribute__((destructor)) vpmu_core_destory(void) {
    if (vpmu_log_file)
        fclose(vpmu_log_file);

    vpmu_release_attached_simulators();
    DBG("VPMU: Simulators Released\n");
    thpool_destroy(GlobalVPMU.thpool);
    DBG("VPMU: Thread Pool Destroyed\n");
    CONSOLE_LOG("VPMU: Terminated\n");
}











//Not used functions
#if 0 //This might be used for parsing insn
void get_insn_parser(unsigned int insn, ExtraTBInfo* s)
{

    /* See Chapter 12 of the ARM920T Reference Manual for details about clock cycles */

    /* first check for invalid condition codes */
    if (( insn >> 28) == 0xf)/* Unconditional instructions.  */
    {
        if ((insn >> 25) == 0x7d) {  /* BLX */
            s->all_changes_to_PC_count += 1 ;
            goto Exit;
        }
    }
    /*bx b bl*/
    switch ((insn >> 25) & 7)
    {
        case 0:
            /* bx  */
            if ((insn & 0x0f900000) == 0x01000000)
            {         switch ((insn >> 4) & 15)
                {
                    case 1:
                        if ((insn & 0x0ffffff0) == 0x01200010)  /* branch/exchange */ /* bx */
                        {
                            s->all_changes_to_PC_count += 1 ;
                            goto Exit;
                        }
                }
            }
        case 5:  /* branch and branch+link */
            s->all_changes_to_PC_count += 1 ;
            goto Exit;
    }
    /* if rd is PC(r15) we do the counting, otherwise we don't*/
    /*LDM LDR MOV*/
    if( __builtin_expect(((insn >> 12) & 0xf) == 15 || ((insn>>16)&0xf) == 15 , 0) )
    {  /* other cases */
        switch ((insn >> 25) & 7)
        {
            case 0: /* bit[27-25] */
                if ((insn & 0x00000090) == 0x00000090)  /* Multiplies, extra load/store, Table 3-2 */
                {
                    /* XXX: TODO: Add support for multiplier operand content penalties in the translator */

                    if ((insn & 0x0fc000f0) == 0x00000090)   /* 3-2: Multiply (accumulate) */
                    {
                        int  Rn = (insn >> 12) & 15;

                        if ((insn & 0x00200000) != 0) {  /* MLA */
                            ;
                        } else {   /* MLU */
                            if (Rn != 0)      /* UNDEFINED */
                                goto Exit;
                        }
                        /* cycles=2+m, assume m=1, this should be adjusted at interpretation time */
                        ;
                    }
                    else if ((insn & 0x0f8000f0) == 0x00800090)  /* 3-2: Multiply (accumulate) long */
                    {
                        if ((insn & 0x00200000) != 0) { /* SMLAL & UMLAL */
                            ;
                        }
                        /* else SMLL and UMLL */

                        /* cucles=3+m, assume m=1, this should be adjusted at interpretation time */
                        ;
                    }
                    else if ((insn & 0x0fd00ff0) == 0x01000090)  /* 3-2: Swap/swap byte */
                    {

                        ;
                    }
                    else if ((insn & 0x0e400ff0) == 0x00000090)  /* 3-2: load/store halfword, reg offset */
                    {

                        ;
                        if ((insn & 0x00100000) != 0){}  /* it's a load, there's a 2-cycle interlock */
                            
                    }
                    else if ((insn & 0x0e400ff0) == 0x00400090)  /* 3-2: load/store halfword, imm offset */
                    {

                        ;
                        if ((insn & 0x00100000) != 0){}  /* it's a load, there's a 2-cycle interlock */
                            
                    }
                    else if ((insn & 0x0e500fd0) == 0x000000d0) /* 3-2: load/store two words, reg offset */
                    {
                        /* XXX: TODO: Enhanced DSP instructions */
                    }
                    else if ((insn & 0x0e500fd0) == 0x001000d0) /* 3-2: load/store half/byte, reg offset */
                    {

                        ;
                        if ((insn & 0x00100000) != 0){}  /* load, 2-cycle interlock */
                            
                    }
                    else if ((insn & 0x0e5000d0) == 0x004000d0) /* 3-2: load/store two words, imm offset */
                    {
                        /* XXX: TODO: Enhanced DSP instructions */
                    }
                    else if ((insn & 0x0e5000d0) == 0x005000d0) /* 3-2: load/store half/byte, imm offset */
                    {

                        ;
                        if ((insn & 0x00100000) != 0){}  /* load, 2-cycle interlock */
                            
                    }
                    else
                    {
                        /* UNDEFINED */
                    }
                }
                else if ((insn & 0x0f900000) == 0x01000000)  /* Misc. instructions, table 3-3 */
                {
                    switch ((insn >> 4) & 15)
                    {
                        case 0:
                            if ((insn & 0x0fb0fff0) == 0x0120f000) /* move register to status register */
                            {
                                ;
                            }
                            break;

                        case 1:
                            if ( ((insn & 0x0ffffff0) == 0x01200010) ||  /* branch/exchange */
                                    ((insn & 0x0fff0ff0) == 0x01600010) )   /* count leading zeroes */
                            {
                                ;
                            }
                            break;

                        case 3:
                            if ((insn & 0x0ffffff0) == 0x01200030)   /* link/exchange */
                            {
                                ;
                            }
                            break;

                        default:
                            /* TODO: Enhanced DSP instructions */
                            ;
                    }
                }
                else  /* Data processing */
                {
                    int  Rd = (insn >> 12) & 15;

                    if(Rd==15)
                        s->all_changes_to_PC_count += 1;
                    if ((insn & 0x10)) {   /* register-controlled shift => 1 cycle penalty */
                        ;
                    }
                }
                break;

            case 1:
                if ((insn & 0x01900000) == 0x01900000)
                {
                    /* either UNDEFINED or move immediate to CPSR */
                }
                else  /* Data processing immediate */
                {
                    int  Rn = (insn >> 12) & 15;
                    if(Rn==15)
                        s->all_changes_to_PC_count += 1 ;
                }
                break;

            case 2:  /* load/store immediate */
                {

                    ;
                    if (insn & 0x00100000) {  /* LDR */
                        int  Rd = (insn >> 12) & 15;

                        if (Rd == 15)  /* loading PC */
                            s->all_changes_to_PC_count += 1 ;
                        else{}
                            
                    }
                }
                break;

            case 3:
                if ((insn & 0x10) == 0)  /* load/store register offset */
                {
                    int  Rn = (insn >> 16) & 15;

                    

                    if (insn & 0x00100000) {  /* LDR */
                        if(Rn==15)
                            s->all_changes_to_PC_count += 1 ;
                        else{}
                            
                    }
                }
                /* else UNDEFINED */
                break;

            case 4:  /* load/store multiple */
                {
                    unsigned int mask = (insn & 0xffff);
                    int       count;

                    for (count = 0; mask; count++)
                        mask &= (mask-1);

                    ;

                    if (insn & 0x00100000)  /* LDM */
                    {
                        int  nn;

                        if (insn & 0x8000) {  /* loading PC */
                            s->all_changes_to_PC_count += 1 ;
                        } else {  /* not loading PC */
                            ;
                        }
                        /* create defs, all registers locked until the end of the load */
                        for (nn = 0; nn < 15; nn++)
                            if ((insn & (1U << nn)) != 0){}
                                
                    }
                    else  /* STM */
                    {
                      ;
                    }
                }
                break;

            case 5:  /* branch and branch+link */
                break;

            case 6:  /* coprocessor load/store */
                {
                    if (insn & 0x00100000){}
                        

                    /* XXX: other things to do ? */
                }
                break;

            default: /* i.e. 7 */
                /* XXX: TODO: co-processor related things */
                ;
        }
    }
Exit:
    ;
}
#endif

