#include "vpmu.h"

void vpmu_update_eet_epc(void)
{
    uint64_t time = 0;

    switch(GlobalVPMU.timing_model & (0xf<<28))
    {
        case TIMING_MODEL_A:
            time = (vpmu_total_insn_count() - GlobalVPMU.last_ticks) * TARGET_AVG_CPI
                / (GlobalVPMU.cpu_model.frequency / 1000.0);
            GlobalVPMU.last_ticks = vpmu_total_insn_count();
            break;
        case TIMING_MODEL_B:
        case TIMING_MODEL_C:
        case TIMING_MODEL_D:
        case TIMING_MODEL_E:
            //evo0209
        case TIMING_MODEL_F:
        case TIMING_MODEL_G:
            time = (vpmu_cycle_count() - GlobalVPMU.last_ticks)
                / (GlobalVPMU.cpu_model.frequency / 1000.0);
            GlobalVPMU.last_ticks = vpmu_cycle_count();

            //evo0209 : for debug
            //printf("vpmu_cycle_count: %llu, total_cycle: %llu\n",
            //      vpmu_cycle_count(),
            //      vpmu_estimated_pipeline_execution_time_ns() +
            //      vpmu_estimated_io_memory_access_time_ns() +
            //      vpmu_estimated_sys_memory_access_time_ns());
            //printf("pipeline %llu, miss %llu, io %llu\n",
            //      vpmu_pipeline_cycle_count(),
            //      vpmu_sys_memory_access_cycle_count(),
            //      vpmu_io_memory_access_cycle_count());
            break;
        default:
            DBG("Wrong Model Number\n");
            break;
    }
    //evo0209 : [Bug] the eet_ns is not match the sum of total pipeline + mem + io
    //GlobalVPMU.eet_ns += time;
    GlobalVPMU.epc_mpu += time * GlobalVPMU.mpu_power / 1000000.0;
}


/* return micro second */
uint32_t vpmu_estimated_execution_time(void)
{
    vpmu_update_eet_epc();
    return GlobalVPMU.eet_ns / 1000;
}
