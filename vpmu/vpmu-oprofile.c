/* Added by Chiaheng. 2010' 9' 24.
 * VPMU for Oprofile setting.
 * It's used accompanied with the mechanism described in op_model_itripac.c.
 * In oprofile, it used "reset_counter" to keep track of overflow value, which
 * is set by the user. When the performance counter reaches the overflow value,
 * the counter will generate the interrupt and set up overflow flag.
 * struct pmu_counter {
 *     volatile unsigned long ovf;
 *     unsigned long reset_counter;
 * };
 * It works when the CONFIG_VPMU_OPROFILE flag is set to true.
 */

#include "vpmu.h"

#ifdef CONFIG_VPMU_OPROFILE
struct cp14_pmu_state* cp14_pmu_state_pt;
spinlock_t vpmu_ovrflw_chk_lock;

static uint32_t pxa2xx_clkpwr_read(void *opaque, int op2, int reg, int crm)
{
	struct cp14_pmu_state *s = (struct cp14_pmu_state *) opaque;

	switch (reg) {
		case 6: /* Clock Configuration register */
			return s->clkcfg;

		case 7: /* Power Mode register */
			return 0;

		default:
			printf("%s:Integrator Bad register 0x%x\n", __FUNCTION__, reg);
			break;
	}
	return 0;
}

static void pxa2xx_clkpwr_write(void *opaque, int op2, int reg, int crm,
		uint32_t value)
{
	/* paslab : temporarily remove this part */
#if 0
	struct cp14_pmu_state *s = (struct cp14_pmu_state *) opaque;
	static const char *pwrmode[8] = {
		"Normal", "Idle", "Deep-idle", "Standby",
		"Sleep", "reserved (!)", "reserved (!)", "Deep-sleep",
	};

	switch (reg) {
		case 6: /* Clock Configuration register */
			s->clkcfg = value & 0xf;
			if (value & 2)
				printf("%s: CPU frequency change attempt\n", __FUNCTION__);
			break;

		case 7: /* Power Mode register */
			if (value & 8)
				printf("%s: CPU voltage change attempt\n", __FUNCTION__);
			switch (value & 7) {
				case 0:
					/* Do nothing */
					break;

				case 1:
					/* Idle */
					if (!(s->cm_regs[CCCR >> 2] & (1 << 31))) { /* CPDIS */
						cpu_interrupt(s->env, CPU_INTERRUPT_HALT);
						break;
					}
					/* Fall through.  */

				case 2:
					/* Deep-Idle */
					cpu_interrupt(s->env, CPU_INTERRUPT_HALT);
					s->pm_regs[RCSR >> 2] |= 0x8;   /* Set GPR */
					goto message;

				case 3:
					s->env->uncached_cpsr =
						ARM_CPU_MODE_SVC | CPSR_A | CPSR_F | CPSR_I;
					s->env->cp15.c1_sys = 0;
					s->env->cp15.c1_coproc = 0;
					s->env->cp15.c2_base0 = 0;
					s->env->cp15.c3 = 0;
					s->pm_regs[PSSR >> 2] |= 0x8;   /* Set STS */
					s->pm_regs[RCSR >> 2] |= 0x8;   /* Set GPR */
					/*                                                                                                                
					 * The scratch-pad register is almost universally used
					 * for storing the return address on suspend.  For the
					 * lack of a resuming bootloader, perform a jump
					 * directly to that address.
					 */
					memset(s->env->regs, 0, 4 * 15);
					s->env->regs[15] = s->pm_regs[PSPR >> 2];

#if 0
					buffer = 0xe59ff000;    /* ldr     pc, [pc, #0] */
					cpu_physical_memory_write(0, &buffer, 4);
					buffer = s->pm_regs[PSPR >> 2];
					cpu_physical_memory_write(8, &buffer, 4);
#endif

					/* Suspend */
					cpu_interrupt(cpu_single_env, CPU_INTERRUPT_HALT);

					goto message;

				default:
message:
					printf("%s: machine entered %s mode\n", __FUNCTION__,
							pwrmode[value & 7]);
			}
			break;

		default:
			printf("%s: Bad register 0x%x\n", __FUNCTION__, reg);
			break;
	}
#endif
	DBG("pxa2xx_clkpwr_read: not handled function.\n");
	return;
}

/* Read Performance Monitoring Registers. */
static uint32_t pxa2xx_perf_read(void *opaque, int op2, int reg, int crm)
{
	struct cp14_pmu_state *s = (struct cp14_pmu_state *) opaque;
	uint32_t temp_value=0;
	int64_t tmp;

	/* vpmu points to GlobalVPMU */
	//VPMU *vpmu = s->vpmu_t;

	switch (reg) {
		case CPPMNC:
			temp_value=s->pmnc;
			DBG("pxa2xx_perf_read: read PMNC reg=%u.\n", temp_value);
			break;

		case CPCCNT:
			//shocklink if (s->pmnc & 1)
			tmp = qemu_get_clock(vm_clock);
			temp_value = (uint32_t) tmp;
			DBG("pxa2xx_perf_read: read CCNT reg=%lld.\n", tmp);
			break;

		case CPFLAG:
			temp_value=s->flag;
			DBG("pxa2xx_perf_read: read FLAG reg=%u.\n", temp_value);
			break;

		case CPEVTSEL:
			temp_value=s->evtsel;
			DBG("pxa2xx_perf_read: read EVTSEL reg=%u.\n", temp_value);
			break;

		case CPINTEN:
			temp_value=s->inten;
			DBG("pxa2xx_perf_read: read INTEN reg=%u.\n", temp_value);
			break;

		default:
			ERR_MSG("%s: Bad register 0x%x\n", __FUNCTION__, reg);
			break;
	}
	return temp_value;
}

static void pxa2xx_perf_write(void *opaque, int op2, int reg, int crm, uint32_t value)
{
	struct cp14_pmu_state *s = (struct cp14_pmu_state *) opaque;
	/* Bit mask of valid registers managed by FLAG register. */
	const uint32_t FLAG_MASK                = 0x0000001F;

	switch (reg) {
		case CPPMNC:
			//fprintf(stderr,"write pmnc=%d\n",value);
			s->pmnc = value;
			DBG("%s: write PMNC=%x.\n",__FUNCTION__, value);
			break;

		case CPCCNT:
			ERR_MSG( "%s: write CCNT=%x. Not Handled.\n", __FUNCTION__, value);
			break;

		case CPINTEN:
			s->inten=value;
			DBG("%s: write INTEN=%x.\n", __FUNCTION__, value);
			break;

		case CPFLAG:
			{
				/* Write to FLAG:
				 * 1 = clear the bit.
				 * 0 = no change.
				 * Obtain the registers, which we want to reset its value.
				 */
				uint32_t tmp    = value & FLAG_MASK;
				tmp             = (~tmp) & s->flag;
				s->flag         = tmp;
				DBG("%s: write FLAG=%x.\n", __FUNCTION__,  tmp);
				/* Pull down the interrupt line. */
				vpmu_perf_counter_overflow_handled(s);
				break;
			}

		case CPEVTSEL:
			/* The value is corresponding to the event number */
			s->evtsel=value;
			DBG("%s: write EVTSEL=%x.\n", __FUNCTION__, value);
			break;

		default:
			ERR_MSG( "%s: Bad register 0x%x.\n", __FUNCTION__, reg);
			break;
	}
}

void pas_cp14_write(void *opaque, int op2, int reg, int crm, uint32_t value)
{
	struct cp14_pmu_state *s = (struct cp14_pmu_state *) opaque;
	/* vpmu points to GlobalVPMU */
	VPMU *vpmu = s->vpmu_t;

	DBG("%s: write to CP 14 cmd -> CRn=%d, CRm=%d. value=%x\n", __FUNCTION__, reg, crm, value);

	switch (crm) {
		case 0:
			pxa2xx_clkpwr_write(opaque, op2, reg, crm, value);
			break;
		case 1:
			pxa2xx_perf_write(opaque, op2, reg, crm, value);
			break;
		case 2: /* xsc2_read_pmc */
			switch (reg) {
				case CPPMN0:
					s->pmn[0]=value;
					DBG("%s: write PMN0=%x.\n", __FUNCTION__, value);

					/* VPMU for Oprofile setting.
					 * This variable is used to remember the current value of performance data.
					 */
#ifdef CONFIG_VPMU_OPROFILE
					s->pmn_reset_value[0]=vpmu_hardware_event_select(s->evtsel, vpmu);
					DBG("\t pmn_reset_value[0]=%u, global vpmu[%d]=%u.\n",
							s->pmn_reset_value[0], s->evtsel, vpmu_hardware_event_select(s->evtsel, vpmu));
#endif

					return;

				case CPPMN1:
					s->pmn[1]=value;
					DBG( "%s: write PMN1=%u. Not Handled.\n", __FUNCTION__, value);
					return;

				case CPPMN2:
					s->pmn[2]=value;
					DBG( "%s: write PMN2=%u. Not Handled.\n", __FUNCTION__, value);
					return;

				case CPPMN3:
					s->pmn[3]=value;
					DBG( "%s: write PMN3=%u. Not Handled.\n", __FUNCTION__, value);
					return;

				default:
					ERR_MSG( "%s: Bad register 0x%d. Only support PMN0-3.\n", __FUNCTION__, reg);
					return;
			}
			/* Fall through */
		default:
			ERR_MSG( "%s: Bad register (CRm value) 0x%x\n", __FUNCTION__, reg);
			break;
	}
}

uint32_t pas_cp14_read(void *opaque, int op2, int reg, int crm)
{
	static uint64_t time;
	struct cp14_pmu_state *s = (struct cp14_pmu_state *) opaque;
	/* vpmu points to GlobalVPMU */
	VPMU *vpmu = s->vpmu_t;
	uint32_t temp_value=0;

	DBG("%s: read from CP 14 cmd -> CRn=%d, CRm=%d.\n", __FUNCTION__, reg, crm);

	switch (crm) {
		case 0:
			return pxa2xx_clkpwr_read(opaque, op2, reg, crm);

			/* READ PMNC, INTEN, FLAG, EVTSEL registers. */
		case 1:
			return pxa2xx_perf_read(opaque, op2, reg, crm);
		case 2:
			//fprintf(stderr,"!!s->pmnc=%d!!!\n",s->pmnc);
			switch (reg) {
				case CPPMN0:
					{
						if(s->pmnc&1)
							DBG("%s: read PMN0, but all counters are not enabled, PMNC=%u\n",
									__FUNCTION__, s->pmnc);
#ifndef CONFIG_VPMU_OPROFILE
						return vpmu_hardware_event_select(s->evtsel, vpmu);
#else
						temp_value = vpmu_hardware_event_select(s->evtsel, vpmu);

						DBG("%s: event[%d]=%u, pmn_reset_value[0]=%u, counter[0]=%u.\n",
								__FUNCTION__, s->evtsel, (temp_value - s->pmn_reset_value[0] +  s->pmn[0]),
								s->pmn_reset_value[0], s->pmn[0]);

						return temp_value - s->pmn_reset_value[0] + s->pmn[0];
#endif

#if 0
						/* Instruction Miss Count event 0x00
						 * 不知為何perfex -e 0 會使得s->evtsel變成-1*/
						if(s->evtsel==-1)
						{
							return (uint32_t)GlobalVPMU.icache->miss[D4XINSTRN];
						}   
						/* Data sheet model : report estimated time */
						if(s->evtsel==0x01)
						{
							return vpmu_estimated_execution_time();

							/* deprecated
							   static uint32_t previousFetch=1,previousMiss=1,previousTick=1,
							   nowFetch=1,nowMiss=1,nowTick=1,
							   diffFetch=1,diffMiss=1,diffTick=1;
							   static char __FirstTime__=0;

							   nowFetch=GlobalVPMU.dcache->fetch[D4XREAD]+ GlobalVPMU.dcache->fetch[D4XWRITE];
							   nowMiss=GlobalVPMU.dcache->miss[D4XREAD]+GlobalVPMU.dcache->miss[D4XWRITE]+GlobalVPMU.icache->miss[D4XINSTRN];
							   nowTick=GlobalVPMU.ticks;
							   diffFetch=nowFetch-previousFetch;
							   diffMiss=nowMiss-previousMiss;
							   diffTick=nowTick-previousTick;

							   static uint32_t time=0;
							   if(__FirstTime__==1)
							   time+=(int)((diffFetch*14.5+diffMiss*360) / 1000) + (int)(diffTick/520);
							   else
							   time=0;

							   previousFetch=GlobalVPMU.dcache->fetch[D4XREAD]+ GlobalVPMU.dcache->fetch[D4XWRITE];
							   previousMiss=GlobalVPMU.dcache->miss[D4XREAD]+GlobalVPMU.dcache->miss[D4XWRITE]+GlobalVPMU.icache->miss[D4XINSTRN];
							   previousTick=GlobalVPMU.ticks;
							//fprintf(stderr,"time=%d",time);

							if(__FirstTime__==1)
							return time;
							else
							{
							__FirstTime__=1;
							return 0;
							}*/
							/*return (uint32_t)(
							  (GlobalVPMU.dcache->fetch[D4XREAD]+ GlobalVPMU.dcache->fetch[D4XWRITE])*14.5//hit latency
							  +(GlobalVPMU.dcache->miss[D4XREAD]+GlobalVPMU.dcache->miss[D4XWRITE]+GlobalVPMU.icache->fetch[D4XINSTRN])*360//miss penalty
							  )/1000
							  +(int)GlobalVPMU.ticks/520;*/

						}
						if(s->evtsel==0x03)
							return vpmu_branch_predict(PREDICT_CORRECT);
						if(s->evtsel==0x04)
							return vpmu_branch_predict(PREDICT_WRONG);
						if(s->evtsel==0x05)
							return vpmu_branch_insn_count();
						if(s->evtsel==0x07)
							return (uint32_t)vpmu_total_insn_count();
						if(s->evtsel==0x0A)
							return (uint32_t)vpmu_dcache_access_count();
						if(s->evtsel==0x0B)
						{   /*fprintf(stderr,"MS=%.1f#%.1f#%.1f#%.1f#%.1f COMP=%.1f %.1f CAP=%.1f %.1f Conf=%.1f %.1f\n"
							  ,GlobalVPMU.dcache->miss[D4XREAD]
							  ,GlobalVPMU.dcache->miss[D4XWRITE]
							  ,GlobalVPMU.dcache->miss[D4XINSTRN]
							  ,GlobalVPMU.dcache->miss[D4XMISC]
							  ,GlobalVPMU.dcache->miss[D4PREFETCH]
							  ,GlobalVPMU.dcache->comp_miss[D4XREAD]
							  ,GlobalVPMU.dcache->comp_miss[D4XWRITE]
							  ,GlobalVPMU.dcache->cap_miss[D4XREAD]
							  ,GlobalVPMU.dcache->cap_miss[D4XWRITE]
							  ,GlobalVPMU.dcache->conf_miss[D4XREAD]
							  ,GlobalVPMU.dcache->conf_miss[D4XWRITE]);
							 */
							//fprintf(stderr,"pxa2xx_perf_read return %d\n",(uint32_t)(GlobalVPMU.dcache->miss[D4XREAD]+GlobalVPMU.dcache->miss[D4XWRITE]));
							return vpmu_dcache_miss_count();
						}
						if(s->evtsel==0x0D)
							return GlobalVPMU.all_changes_to_PC_count;
						/* Virtual Power Device events, added by gkk886. */
						if(s->evtsel == 0xff)
						{
							return (uint32_t)(vpmu_total_insn_count()*4.2*(1/1000000.0)*
									((GlobalVPMU.cap/1000000.0)*
									 (GlobalVPMU.volt_info[GlobalVPMU.mult-15]/1000000.0)*
									 (GlobalVPMU.volt_info[GlobalVPMU.mult-15]/1000000.0)+
									 (GlobalVPMU.base_power)/
									 (GlobalVPMU.freq_table[0])));
						}
#endif
					}
				case CPPMN1:
				case CPPMN2:
				case CPPMN3:
					time=vpmu_estimated_execution_time_ns();
					DBG("%s: read PMN1/PMN2/PMN3 - second=%llu. (CRn=%d) Not Handled.\n",
							__FUNCTION__, time/1000000000, reg);
					return time/1000000000;
				default:
					ERR_MSG( "%s: Bad register 0x%d. Only support PMN0-3.\n", __FUNCTION__, reg);
					break;
			}
			/* Fall through */
		default:
			ERR_MSG( "%s: Bad register (CRm value) 0x%x\n", __FUNCTION__, reg);
			break;
	}
	return 0;
}

/*-------------------------------------
 * VPMU for Oprofile feature.
 * This function is invoked whenever "host_alarm_handler()"
 * in vl.c is called.
 *
 * This function checks if the "virtualized" PMN counters are overflowed.
 * If so, it will set up FLAG register and raise interrupt.
 * IMPORTANT: Note that we currently handles PMN0.
 *
 * Descirption of the behaviors of PMN0-3 counters.
 * When an event counter reaches its maximum value
 * 0xFFFFFFFF, the next event it needs to count causes it to roll over to zero
 * and set its corresponding overflow flag (bit 1, 2, 3 or 4) in FLAG. An
 * interrupt request is generated when its corresponding interrupt enable
 * (bit 1, 2, 3 or 4) is set in INTEN.
 *
 * For more detail of Performance Monitoring Registers please refer to: Ch. 11 in
 * "3rd Generation Intel XScale Microarchitecture Developer’s Manual_31628302.pdf".
 *
 *-------------------------------------*/
//#include <time.h>
void vpmu_perf_counter_overflow_testing (void *opaque)
{
    struct      cp14_pmu_state* cp14_pmu_pt = (struct cp14_pmu_state*) opaque;
    /* Mask to check the E bit in PMNC register. */
    const uint32_t PMNC_Ebit_MASK           = 0x00000001;
    /* Mask to select the mask for PMN0 register. */
    const uint32_t PMN0_MASK                = 0x00000002;
    /*
     * cnt_value keeps the reset value set by Oprofile User.
     * I.e., the value that user specified (inteval) when starts Oprofile.
     * reset_value keeps the snapshot global VPMU value, which is the baseline
     * for counting the performance result of the program.
     * cur_value always gets the latest value from global VPMU.
     *
     * The simulated counter is called "virtual" counter.
     * The value of "virtualized" counter = cur_value - reset_value + cnt_value.
     */
    uint32_t    cnt_value           = 0,
                reset_value         = 0,
                cur_value           = 0,
                vir_cnt_value       = 0,
                vpmu_value_inteval  = 0,
                vir_value_interval  = 0;

    /* Obtaining the latest values. */
    cnt_value           = cp14_pmu_pt->pmn[0];
    reset_value         = cp14_pmu_pt->pmn_reset_value[0];
    cur_value           = vpmu_hardware_event_select(cp14_pmu_pt->evtsel,
            cp14_pmu_pt->vpmu_t);
    /* Calculate the content of the "virtual" counter seen by Oprofile.
     * Currently, the variable is deprecated.
     */
    vir_cnt_value       = cur_value - reset_value + cnt_value;

    /* Calculate the differences observed by the Global VPMU counter. */
    vpmu_value_inteval   = cur_value - reset_value;
    /* Calculate the expected value, specified by the user. */
    vir_value_interval  = ((uint32_t)0xFFFFFFFF - cnt_value);

    /* Check if the "virtual" counter is overflowed. */
    if ( vpmu_value_inteval > vir_value_interval )
    {
        //spin_lock(vpmu_ovrflw_chk_lock);
        /* If the corresponding bit is set in INTEN resiter,
         * raise the interrupt after set up the FLAG register.
         * Check if the interrupt of PMN0 register is enabled in
         * Interrupt Enable Register (INTEN) && check if the PMN counters are activated.
         */
        if ( (cp14_pmu_pt->inten & PMN0_MASK) && (cp14_pmu_pt->pmnc & PMNC_Ebit_MASK) )
        {
            /* When the counter is overflowed, set up the overflow flag (0x00000010)
             * in FLAG register. Currently, only PMN0 is monitored.
             */
            cp14_pmu_pt->flag |= PMN0_MASK;
            qemu_irq_raise(QLIST_FIRST(&vpmu_t.head)->irq);

            DBG("%s: Interrupt triggered by PMN0 overflow.\n", __FUNCTION__);
        }

        /* Reset cnt_value to current global VPMU data. */
        cp14_pmu_pt->pmn_reset_value[0] = cur_value;
        //spin_unlock(vpmu_ovrflw_chk_lock);

        DBG("\tPMN0 Overflowed. VPMU experienced offset=%u, interested interval=%u.\n", \
                vpmu_value_inteval, vir_value_interval);
    }


    /* Used to calculate the frequency of this function to be called.
     * As it turns out, on the Ubuntu 9.10 with Intel(R) Core(TM) i7 CPU 920  @ 2.67GHz,
     * the frequency is about 0.10 second.
     */
    //static uint32_t time_cnt=0;
    //time_t t1=time(NULL);
    //printf("%d seconds elapsed, counter=%u\n", t1, time_cnt++);
}

/*-------------------------------------
 * VPMU for Oprofile feature.
 * This function is invoked after OProfile done with handling the
 * performance counter overflow interrupt.
 *
 * This function is invoked in pxa2xx_perf_write(), when Oprofile
 * tries to write clear the FLAG bits.
 *
 *
 *------------------------------------*/
void vpmu_perf_counter_overflow_handled (void *opaque)
{
    struct      cp14_pmu_state* cp14_pmu_pt = (struct cp14_pmu_state*) opaque;
    /* Mask to select the mask for PMN0 register. */
    const uint32_t PMN0_MASK                = 0x00000002;

    /* Pull down the interrupt for the PMN0 when ...
     * the corresponding bit is set in INTEN resiter and
     * the corresponding bit is cleared in FLAG register.
     */
    if ( (cp14_pmu_pt->inten & PMN0_MASK) && !(cp14_pmu_pt->flag & PMN0_MASK) )
    {
        qemu_irq_lower(QLIST_FIRST(&vpmu_t.head)->irq);

        DBG("%s: PMN0 overflow interrupt has been serviced.\n", __FUNCTION__);
    }

}

static void __attribute__((constructor)) vpmu_oprofile_init(void) {

}

#endif

