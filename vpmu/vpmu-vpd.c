#include "vpmu.h"

uint32_t vpmu_vpd_mpu_power(void)
{
    /* evo0209 fixed : check the power estimation mode is mpu or peripheral */
    if (VPMU_enabled)
    {
        vpmu_update_eet_epc();
        return GlobalVPMU.epc_mpu;
    }
#ifdef CONFIG_ANDROID
    else
        return (uint32_t)(4.34 * adev_stat.cpu_util);
#else
    else
        return 1;
    /*may add more power models*/
#endif
}

#ifdef CONFIG_ANDROID
uint32_t vpmu_vpd_lcd_power(void)
{
    //return (uint32_t)((vpmu_estimated_execution_time()/1000.0)*
    //        (*(adev_stat.lcd_brightness) ? *(adev_stat.lcd_brightness)*2.4+287.3 : 0.0));
    return (uint32_t)(*(adev_stat.lcd_brightness) ? *(adev_stat.lcd_brightness)*2.4+287.3 : 0.0);
}

uint32_t vpmu_vpd_audio_power(void)
{
    //return (uint32_t)((vpmu_estimated_execution_time()/1000.0)*(adev_stat.audio_on ? 384.62 : 0.0));
    return (uint32_t)(adev_stat.audio_on ? 384.62 : 0.0);
}

uint32_t vpmu_vpd_net_power(void)
{
    if(adev_stat.net_mode == 1) //Wifi mode
        //return (uint32_t)((vpmu_estimated_execution_time()/1000.0)*(adev_stat.net_on ? 720.0 : 38.0));
        return (uint32_t)(adev_stat.net_on ? 720.0 : 38.0);
    else if(adev_stat.net_mode == 2) //3G mode
        //return (uint32_t)((vpmu_estimated_execution_time()/1000.0)*(adev_stat.net_on ? 401.0 : 0.0));
        return (uint32_t)(adev_stat.net_on ? 401.0 : 0.0);
    else //off
        return 0;
}

uint32_t vpmu_vpd_gps_power(void)
{
    if(*(adev_stat.gps_status) == 2) //active
        //return (uint32_t)((vpmu_estimated_execution_time()/1000.0)*(429.55));
        return (uint32_t)(429.55);
    else if(*(adev_stat.gps_status) == 1) //idle
        //return (uint32_t)((vpmu_estimated_execution_time()/1000.0)*(173.55));
        return (uint32_t)(173.55);
    else //off
        return 0;
}
#endif

uint32_t vpmu_vpd_total_power(void)
{
#ifdef CONFIG_ANDROID
    return vpmu_vpd_mpu_power()+vpmu_vpd_lcd_power()+vpmu_vpd_audio_power()+
        vpmu_vpd_net_power()+vpmu_vpd_gps_power();
#else
    return 0;
#endif
}
