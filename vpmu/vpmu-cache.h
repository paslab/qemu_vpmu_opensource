#ifndef _VPMU_CACHE_H_
#define _VPMU_CACHE_H_

//Maximum cores VPMU support to emulate
//This is a copy from vpmu.h to make this header independent from other headers.
#ifndef VPMU_MAX_CPU_CORES
#define VPMU_MAX_CPU_CORES 64
#endif

enum Cache_Data_Index{
    CACHE_READ, 
    CACHE_WRITE, 
    CACHE_READ_MISS, 
    CACHE_WRITE_MISS, 
    SIZE_OF_CACHE_INDEX};

enum Cache_Data_Level{
    NOT_USED,
    L1_CACHE, 
    L2_CACHE, 
    L3_CACHE, 
    MAX_LEVEL_OF_CACHE};

//The data/states of each simulators
typedef struct Cache_Data {
    //[level][core][r/w miss/hit]
    uint64_t inst_cache[MAX_LEVEL_OF_CACHE][VPMU_MAX_CPU_CORES][SIZE_OF_CACHE_INDEX];
    uint64_t data_cache[MAX_LEVEL_OF_CACHE][VPMU_MAX_CPU_CORES][SIZE_OF_CACHE_INDEX];
} Cache_Data;

typedef struct VPMU_CACHE_MODEL {
    char name[64];
    //number of layers this cache configuration has
    int levels;
    //cache latency information
    int latency[MAX_LEVEL_OF_CACHE];
    //cache block size information
    int d_log2_blocksize[MAX_LEVEL_OF_CACHE];
    int d_log2_blocksize_mask[MAX_LEVEL_OF_CACHE];
    //cache block size information
    int i_log2_blocksize[MAX_LEVEL_OF_CACHE];
    int i_log2_blocksize_mask[MAX_LEVEL_OF_CACHE];
    //data cache: true -> write allocate; false -> non write allocate
    int d_write_alloc[MAX_LEVEL_OF_CACHE];
    //data cache: true -> write back; false -> write through
    int d_write_back[MAX_LEVEL_OF_CACHE];
} VPMU_Cache_Model;

#endif

