#ifndef _VPMU_H_
#define _VPMU_H_
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <inttypes.h>
//JSON
#include <json-c/json.h>
//Shared Memory Related Headers
#include <unistd.h>
#include <sys/shm.h>
//file control options
#include <fcntl.h>
//End of Shared Memory Related Headers

//Multi threading Headers
#include <pthread.h>
//data types
#include <sys/types.h>
//End of Multi threading Headers

//signaling Headers
#include <signal.h>
#include <semaphore.h>
//memory management declarations
#include <sys/mman.h>
//readable error messages
#include <errno.h>

//C localization functions for ' support in printf
#include <locale.h>

//QEMU configurations
#include "config-host.h"

//likely, unlikely
#ifdef CONFIG_QEMU_VERSION_0_15
#include "osdep.h"
#else
#include "qemu/osdep.h"
#endif

//QEMU log system
#include "qemu/log.h"

//vpmu arm instruction headers
#include "vpmu-arm-inst.h"
//vpmu cache information structures and enums
#include "vpmu/vpmu-cache.h"
//lighting ring buffer header
#include "ringbuffer.h"
//Thread pool library support
#include "thpool.h"

#define COMPILER_BARRIER() asm volatile("" ::: "memory")

#define VPMU_POST_JOB(sem_mutex_t, call_before_function) do {\
    call_before_function;\
    sem_post(sem_mutex_t);       /* up semaphore */\
}while(0)

#define VPMU_WAIT_JOB(sem_mutex_t, call_back_function) do {\
    sem_wait(sem_mutex_t);       /* down semaphore */\
    call_back_function;\
}while(0)

//This is a macro with callback for making a batch.
#define VPMU_PUSH_JOB(sem_mutex_t, call_before_function) do {\
    static unsigned char _local_counter = 1;\
    call_before_function;\
    if (_local_counter == 0) {    /* Signal per 256 requests */\
        sem_post(sem_mutex_t); /* up semaphore */\
    }\
    _local_counter++;\
}while(0)

//This is a macro with callback for making a batch.
#define VPMU_SHM_PUSH_JOB(pkg, num, call_before_function) do {\
    static unsigned char _local_counter = 1;\
    call_before_function;\
    if (_local_counter == 0) { /* Signal per 256 requests */\
        int _ii;\
        for (_ii = 0; _ii < num; _ii++) /* Big overhead for fast signaling */\
            sem_post(&pkg[_ii].cache_job_semaphore); /* up semaphore */\
    }\
    _local_counter++;\
}while(0)

#define VPMU_SHM_POST_JOB(pkg, num, call_before_function) do {\
    call_before_function;\
    int _ii;\
    for (_ii = 0; _ii < num; _ii++)\
        sem_post(&pkg[_ii].cache_job_semaphore);       /* up semaphore */\
}while(0)


#define VPMU_LOOP(L_CONDITION, call_back) do {\
    int _index;\
    for (_index = 0; L_CONDITION; _index++) {\
        call_back;\
    }\
}while(0)

#ifdef CONFIG_VPMU_VFP
extern uint32_t arm_vfp_instr_time[];
extern uint32_t arm_vfp_latency[];
extern const char const *arm_vfp_instr_names[];
void print_vfp_count(void);
ARM_VFP_Instructions get_index_of_arm_vfp_inst(const char *);
#endif //CONFIG_VPMU_VFP

#ifdef CONFIG_VPMU_OPROFILE
/* Support VPMU for Oprofile feature. */
/* Working only when CONFIG_VPMU_OPROFILE flag is ture. */
// Added by Chiaheng. 2010' 9' 23.
#define CCCR   0x00  /* Core Clock Configuration register */
#define CKEN   0x04  /* Clock Enable register */
#define OSCC   0x08  /* Oscillator Configuration register */
#define CCSR   0x0c  /* Core Clock Status register */

/* Performace Monitoring Registers */
#define CPPMNC    0  /* Performance Monitor Control register */
#define CPCCNT    1  /* Clock Counter register */
#define CPINTEN   4  /* Interrupt Enable register */
#define CPFLAG    5  /* Overflow Flag register */
#define CPEVTSEL  8  /* Event Selection register */

#define CPPMN0    0  /* Performance Count register 0 */
#define CPPMN1    1  /* Performance Count register 1 */
#define CPPMN2    2  /* Performance Count register 2 */
#define CPPMN3    3  /* Performance Count register 3 */

typedef struct cp14_pmu_state {
    /* Power management */
    uint32_t pm_base;
    uint32_t pm_regs[0x40];

    /* Clock management */
    uint32_t cm_base;
    uint32_t cm_regs[4];
    uint32_t clkcfg;
    /* Performance monitoring */
    uint32_t pmnc;
    uint32_t evtsel;
    uint32_t pmn[4];
    uint32_t flag;
    uint32_t inten;
    struct VPMU * vpmu_t;
    /* VPMU for Oprofile */
    /* Check vpmu-device.c */
    uint32_t pmn_reset_value[4];
} CP14_PMU_STATE;

uint32_t pas_cp14_read(void* , int32_t , int32_t , int32_t );
void pas_cp14_write(void *, int32_t , int32_t , int32_t , uint32_t );
void vpmu_perf_counter_overflow_testing (void *);
void vpmu_perf_counter_overflow_handled (void *);
#endif //CONFIG_VPMU_OPROFILE


//========================  VPMU Common Definitions  ========================
#ifndef VPMU_BASE_ADDR
    #define VPMU_BASE_ADDR 0xf1000000
#endif
//Maximum cores VPMU support to emulate
#ifndef VPMU_MAX_CPU_CORES
#define VPMU_MAX_CPU_CORES 64
#endif

#define VPMU_IOMEM_SIZE 0x2000 /* 0x0000~0x0fff is used by vpd */
// Define maximum supported events (event ids) defined in "struct VPMU".
#define MAX_VPMU_SUPPORT_EVENT  256
#define CUR_VPMU_SUPPORT_EVENT  46
#define EVENT_ON                1
#define EVENT_OFF               0

// Timing model selector
#define TIMING_MODEL_A 0x1<<28
#define TIMING_MODEL_B 0x2<<28
#define TIMING_MODEL_C 0x3<<28
#define TIMING_MODEL_D 0x4<<28
#define TIMING_MODEL_E 0x5<<28
#define TIMING_MODEL_F 0x6<<28
#define TIMING_MODEL_G 0x7<<28

// Mode selector
#define VPMU_INSN_COUNT_SIM 0x1<<0
#define VPMU_DCACHE_SIM     0x1<<1
#define VPMU_ICACHE_SIM     0x1<<2
#define VPMU_BRANCH_SIM     0x1<<3
#define VPMU_PIPELINE_SIM   0x1<<4
#define VPMU_MODEL_SELECT   0x1<<5
#define VPMU_SAMPLING       0x1<<6

// MISCs
#define ARM_PLD 0xff

//These are ISR related
/* timer interrupt interval = 10 ms = 10000 us */
//#define TIMER_INTERRUPT_INTERVAL 10000
#define NUM_FIND_SYMBOLS 1

//These are branch related
#define PREDICT_CORRECT 0
#define PREDICT_WRONG 1
#define BRANCH_PREDICT_SYNC 2

//These are cache related
#define CACHE_PACKET_READ      0x00
#define CACHE_PACKET_WRITE     0x01
#define CACHE_PACKET_INSTRN    0x02

//TODO These should be loaded from configuration file, not static assigned.
/* target board cache latency to esitmate execution time */
#define CACHE_MISS_LATENCY 30 //cycles
#define TARGET_AVG_CPI 1.51189302527

// Android related
#ifdef CONFIG_ANDROID
#define VPD_BAT_CAPACITY 1150
#define VPD_BAT_VOLTAGE 3.7
#endif

//Each model defines its own packet type here
#define VPMU_PACKET           0x0
//Values above 0xFF00 belongs to control packets
#define VPMU_PACKET_BARRIER   0xFFFF
#define VPMU_PACKET_SYNC_DATA 0xFFEF
#define VPMU_PACKET_DUMP_INFO 0xFFDF
#define VPMU_PACKET_RESET     0xFFCF

#ifdef CONFIG_VPMU_USE_CPU_FENCE
    // Prevent CPU reordering
    #define VPMU_MEM_FENCE() asm volatile("mfence" ::: "memory")
#else
    // Prevent compiler reordering
    #define VPMU_MEM_FENCE() asm volatile("" ::: "memory")
#endif
//==========================  VPMU Common Macros  ===========================
#define IS_VPMU_CONTROL_PACKET(_type) (_type >= 0xFF00)

#ifdef CONFIG_VPMU_DEBUG_MSG
#define DBG(str,...) do{\
    fprintf(stderr, str,##__VA_ARGS__); \
    fflush(stderr);\
}while(0)
#else
#define DBG(str,...) {}
#endif

#define BASH_COLOR_RED     "\033[0;31m"
#define BASH_COLOR_GREEN   "\033[0;32m"
#define BASH_COLOR_YELLOW  "\033[0;33m"
#define BASH_COLOR_BLUE    "\033[0;34m"
#define BASH_COLOR_PURPLE  "\033[0;35m"
#define BASH_COLOR_CYAN    "\033[0;36m"
#define BASH_COLOR_NONE    "\033[0;00m"
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#define ERR_MSG(str,...) do{\
    fprintf(stderr, \
            BASH_COLOR_RED"ERROR"\
            BASH_COLOR_NONE"("\
            BASH_COLOR_PURPLE"%s: "\
            BASH_COLOR_GREEN"%d"\
            BASH_COLOR_NONE") : ", __FILENAME__, __LINE__); \
    fprintf(stderr, str,##__VA_ARGS__); \
    fflush(stderr);\
}while(0)

#define QEMU_MONITOR_LOG(str,...) do{\
    qemu_chr_printf(vpmu_t.vpmu_monitor, str "\r", ##__VA_ARGS__);\
    qemu_chr_printf(serial_hds[0],str "\r", ##__VA_ARGS__);\
}while(0)

#define FILE_LOG(str,...) do{\
    fprintf(vpmu_log_file, str, ##__VA_ARGS__);\
}while(0)

#define CONSOLE_LOG(str,...) \
    fprintf(stderr,str,##__VA_ARGS__)

#define vpmu_model_has(model, vpmu) (vpmu.timing_model & (model))

#define vpmu_wall_clock_period(vpmu) \
    ((vpmu->end_time.tv_sec - vpmu->start_time.tv_sec) \
     * 1000000000 \
     + (vpmu->end_time.tv_nsec - vpmu->start_time.tv_nsec))

#define macro_str(str) #str

#define READ_FROM_GUEST_KERNEL(_cs, _addr, _offset) \
    ((uint8_t *)vpmu_get_phy_addr_global((void *)_cs, (uintptr_t)_addr) + _offset)

#define READ_BYTE_FROM_GUEST(_cs, _addr, _offset) \
    *((uint8_t *)READ_FROM_GUEST_KERNEL(_cs, _addr, _offset))
#define READ_INT_FROM_GUEST(_cs, _addr, _offset) \
    *((uint32_t *)READ_FROM_GUEST_KERNEL(_cs, _addr, _offset))
#define READ_LONG_FROM_GUEST(_cs, _addr, _offset) \
    *((uint64_t *)READ_FROM_GUEST_KERNEL(_cs, _addr, _offset))

//=======================  VPMU Common Enumerations  ========================
enum PACKET_PROCESSOR_TYPE{
    PROCESSOR_CPU,
    PROCESSOR_GPU,
    ALL_PROC
};

enum unknown{
    TICK_SCHED_TIMER = 0,
    //RUN_TIMER_SOFTIRQ = 0,
    //GOLDFISH_TIMER_INTERRUPT,
};

enum simulator_fun_type{
    SIM_FUN_INIT,
    SIM_FUN_FREE,
    SIM_FUN_RESET,
    SIM_FUN_SYNC,
    SIM_FUN_DUMP
};

enum simulator_type{
    SIM_TYPE_CACHE,
    SIM_TYPE_BRANCH
};

enum VPMU_EVENT_ID_MAPPING {
    // Performance Events To Be Monitored.
    // Instructions related events.
    TOT_INST=0,
    USR_INST,
    SVC_INST,
    IRQ_INST,
    REST_INST,
    TOT_LD_ST_INST,
    TOT_USR_LD_ST_INST,
    TOT_SVC_LD_ST_INST,
    TOT_IRQ_LD_ST_INST,
    TOT_REST_LD_ST_INST,
    TOT_LD_INST,
    TOT_USR_LD_INST,
    TOT_SVC_LD_INST,
    TOT_IRQ_LD_INST,
    TOT_REST_LD_INST,
    TOT_ST_INST,
    TOT_USR_ST_INST,
    TOT_SVC_ST_INST,
    TOT_IRQ_ST_INST,
    TOT_REST_ST_INST,
    PC_CHANGE,
    // Branch related events.
    BRANCH_INST=32,
    BRANCH_PREDICT_CORRECT,
    BRANCH_PREDICT_WRONG,
    // Cache realted events.
    // Please make sure you have turned on the cache simulation.,
    DCACHE_ACCESSES_CNT=48,
    DCACHE_WRITE_MISS,
    DCACHE_READ_MISS,
    DCACHE_MISS,
    ICACHE_ACCESSES_CNT,
    ICACHE_MISS,
    // Memory acess.
    SYSTEM_MEM_ACCESS=64,
    IO_MEM_ACCESS,
    // Estimated execution time,
    ELAPSED_CPU_CYC=80,
    ELAPSED_EXE_TIME,
    ELAPSED_PIPELINE_CYC,
    ELAPSED_PIPELINE_EXE_TIME,
    ELAPSED_SYSTEM_MEM_CYC,
    ELAPSED_SYSTEM_MEM_ACC_TIME,
    ELAPSED_IO_MEM_CYC,
    ELAPSED_IO_MEM_ACC_TIME,
    // Energy events,
    MPU_ENERGY=240,
    //DSP1_ENERGY,
    //DSP2_ENERGY,
    LCD_ENERGY=243,
    AUDIO_ENERGY,
    NET_ENERGY,
    GPS_ENERGY,
    TOTAL_ENERGY=255
};

//========================  VPMU Common Structures  =========================
// The top structure of VPMU configurations and performance counters.
typedef struct VPMU_Simulator {
    uint64_t (*fun[8])(void *);
    uint64_t (*cb[8])(void);
    char description[64];
    void *opaque;
    enum simulator_type type;
} VPMU_Simulator;

typedef struct VPMU {
    //Thread pool management
    threadpool thpool;

    void *cs;
    char traced_process_name[256];
    uint32_t traced_process_pid[32], num_traced_threads;
    uint32_t traced_process_model;
    uintptr_t execve_addr, switch_to_addr, exit_addr;
    uintptr_t wake_up_new_task_addr, do_fork_addr;

    struct _CPU_MODEL {
        char name[64];
        int32_t num_cores;
        uint64_t frequency;
        int32_t dual_issue;
    } cpu_model;
    struct _GPU_MODEL {
        char name[64];
        int32_t num_cores;
        uint64_t frequency;
    } gpu_model;
    //Simulator information should be retrived from each simulator.
    //Do not read the configuration from file directly for consistency,
    //because simulators might use it's own configuration format.
    VPMU_Cache_Model cache_model;
    //A pointer holding the data from cache simulators
    Cache_Data *cache_data;

#ifdef CONFIG_VPMU_VFP
    //tianman
    uint64_t VFP_count[ARM_VFP_INSTRUCTION_TOTAL_COUNTS];
    uint64_t VFP_BASE;
#endif
    uint64_t ARM_count[ARM_INSTRUCTION_TOTAL_COUNTS];

    /* TODO: remove unused varibles SYS/FIQ/ABT/UND_icount*/
    /* TODO: define a struct contains
     * icount/ldst_count/load_count/store_count for all the 7 ARM modes */
    uint64_t USR_icount;
    uint64_t SYS_icount;
    uint64_t FIQ_icount;
    uint64_t IRQ_icount;
    uint64_t SVC_icount;
    uint64_t ABT_icount;
    uint64_t UND_icount;
    uint64_t sys_call_icount;
    /* insn count of FIQ+IRQ+ABT+UND+SYS */
    uint64_t sys_rest_icount;

    uint64_t USR_ldst_count;
    uint64_t SVC_ldst_count;
    uint64_t IRQ_ldst_count;
    uint64_t sys_call_ldst_count;
    /* ldst count of FIQ+IRQ+ABT+UND+SYS */
    uint64_t sys_rest_ldst_count;

    uint64_t USR_load_count;
    uint64_t SVC_load_count;
    uint64_t IRQ_load_count;
    uint64_t sys_call_load_count;
    /* load count of FIQ+IRQ+ABT+UND+SYS */
    uint64_t sys_rest_load_count;

    uint64_t USR_store_count;
    uint64_t SVC_store_count;
    uint64_t IRQ_store_count;
    uint64_t sys_call_store_count;
    /* store count of FIQ+IRQ+ABT+UND+SYS */
    uint64_t sys_rest_store_count;

    /*=== Christine ADD ===*/
    uint64_t load_and_store_count;
    uint64_t tlb_access_count;
    uint64_t tlb_miss_count;


    /* for timer interrupt */
    uint32_t  timer_interrupt_return_pc;
    char state;
    char timer_interrupt_exception;
    char need_tb_flush;

    /* slowdown guest timing for correct timer interrupt generation */
    double slowdown_ratio;

    /* last halted vm_clock is saved here for idle time caculation */
    uint64_t cpu_halted_vm_clock;
    uint64_t last_vm_clock;

    //uint64_t ticks;
    uint64_t ticks;
    uint64_t cpu_idle_time_ns;
    uint64_t eet_ns; //estimated execution time in ns
    uint64_t last_ticks; //tmp data for calculate eet_ns
    uint32_t epc_mpu; //estimated power consumption for mpu
    uint64_t branch_count;

    uint64_t iomem_count;
    uint8_t  iomem_access_flag;//It's a flag indicating IO access


    /* evo0209 : for dual issue check */
    uint64_t reduce_ticks;

    /* branch prediction */
    uint32_t branch_predict_correct;
    uint32_t branch_predict_wrong;

    /* timing model selector : 
     * bit[31-28] (model selector) bit[27-0] (simulator on-off table) */
    uint32_t timing_model;

    /*Christine ADD : Create a buffer to save timestamps for
      both VPMU and Firefox OS emulator.*/
    int32_t *timestamp_ptr;
    uint64_t* guess_time;     //assume both of them are queues.
    uint64_t* actual_time;
    uint64_t* eet_ptr;
    pthread_t eet_pid;
    sem_t SignalSendingDone;  //use to make sure the signal sends
    //only one at one time.


    char swi_fired;
    uint32_t swi_fired_pc; 
    uint32_t swi_enter_exit_count;

    /*
     * Virtual Power Device Info
     * added by gkk886
     */
    double mpu_power;
    /* MPU frequency */
    int32_t mpu_freq;
#ifdef CONFIG_ANDROID
    /* Android devices state*/
    struct android_dev_stat *adev_stat_ptr;
#endif
    FILE *dump_fp;
    /* call back function for more simulators */
    /* 10 simulators at most */
    struct VPMU_Simulator sims[10];
    int32_t num_of_sims;
    /* For measuring the emulation time */
    struct timespec start_time, end_time;

    /*Modelsel*/
    uint64_t total_tb_visit_count;
    uint64_t cold_tb_visit_count;
    uint64_t hot_tb_visit_count;
    uint64_t hot_dcache_read_count;
    uint64_t hot_dcache_write_count;
    uint64_t hot_icache_count;

} VPMU;

typedef struct DSPInfo
{
    uint64_t cycle_count;
    uint64_t mem_read_size_count;
    uint64_t mem_write_size_count;
} DSPInfo;

#ifdef CONFIG_ANDROID
typedef struct android_dev_stat {
    uint32_t cpu_util;
    uint32_t *lcd_brightness;
    int32_t *audio_buf;
    uint32_t audio_on; // 0:off others:on
    char net_mode; // 0:off 1:Wifi 2:3G
    char net_on; // 0:off 1:on
    uint32_t *net_packets;
    uint32_t *gps_status; // 0:off 1:idle 2:active
    char battery_sim; // 0:off 1:on
    uint32_t energy_left; // mJ
    int32_t *bat_ac_online; // 0:offline 1:online
    int32_t *bat_capacity;
} AndroidDeviceState;
#endif

// A structure to extend TB info for accumulating counters when executing each TB.
typedef struct ExtraTBInfo {
    uint8_t       insn_count;
    uint8_t       load_count;
    uint8_t       store_count;
    uint8_t       alu_count;
    uint8_t       bit_count;   //shift, and, or, xor
    uint8_t       fpu_count;
    uint8_t       has_branch;
    uint16_t      ticks;
    uint16_t      size_insns;
    uint32_t      start_addr;

#ifdef CONFIG_VPMU_VFP
    //tianman
    uint8_t       vfp_locks[32];
    uint16_t      vfp_base;
    uint8_t       vfp_count[ARM_VFP_INSTRUCTION_TOTAL_COUNTS];
#endif
    uint8_t       arm_count[ARM_INSTRUCTION_TOTAL_COUNTS];
    uint64_t      insn_buf[2]; //For future 64 bits ARM insns
    uint8_t       insn_buf_index;
    uint16_t      dual_issue_reduce_ticks;

    //Modelsel
    uint8_t       hot_ref_counter;
    uint32_t      last_visit;
    uint32_t      cold_hot_boundary;
    double        hot_icache_miss_rate;
    uint16_t      num_of_cacheblks;
} ExtraTBInfo;

//==================  VPMU Common Externs(Outer Variables) ==================
// File struct to dump VPMU information
extern FILE *vpmu_log_file;
// A structure storing information of PMU. This is called via cp14.
extern struct cp14_pmu_state* cp14_pmu_state_pt;
// A table with all instruction cycles
extern uint32_t arm_instr_time[ARM_INSTRUCTION_TOTAL_COUNTS];
// A table of ISR address
extern uint32_t ISR_addr[NUM_FIND_SYMBOLS];

extern volatile int32_t VPMU_enabled;
// A structure storing VPMU configuration
extern struct VPMU GlobalVPMU;

//========================  VPMU Common Prototypes  =========================
static inline int32_t clog2 (uint32_t x)
{
    int32_t i;
    for (i = -1;  x != 0;  i++)
        x >>= 1;
    return i;
}




uintptr_t vpmu_get_phy_addr_global(void *, uintptr_t);
size_t vpmu_copy_from_guest(void *, uintptr_t, const size_t, void *);
void vpmu_model_setup(VPMU* vpmu, uint32_t model);
void append_string(char *head, const char *tail);
void VPMU_update_slowdown_ratio(double value);
void VPMU_sync(void);
void VPMU_reset(void);
int8_t vpmu_simulator_status(VPMU* vpmu, uint32_t opt);
void VPMU_dump_result(void);
void enable_QEMU_log(void);
void disable_QEMU_log(void);
void get_insn_parser(uint32_t insn, ExtraTBInfo* s);


void pas_cp14_write(void *opaque, int32_t op2, int32_t reg, int32_t crm, uint32_t value);
uint32_t pas_cp14_read(void *opaque, int32_t op2, int32_t reg, int32_t crm);
void vpmu_perf_counter_overflow_testing (void *opaque);
void vpmu_perf_counter_overflow_handled (void *opaque);

uint64_t vpmu_total_insn_count(void);
uint64_t vpmu_total_ldst_count(void);
uint64_t vpmu_total_load_count(void);
uint64_t vpmu_total_store_count(void);
uint64_t vpmu_branch_insn_count(void);
uint64_t vpmu_branch_predict(uint8_t request);
uint64_t vpmu_branch_predict_correct(void);
uint64_t vpmu_branch_predict_wrong(void);
uint64_t vpmu_sys_mem_access_cycle_count(void);
uint64_t vpmu_io_mem_access_cycle_count(void);
uint64_t vpmu_cycle_count(void);
uint64_t vpmu_pipeline_cycle_count(void);
uint64_t vpmu_estimated_pipeline_time_ns(void);
uint64_t vpmu_estimated_sys_mem_time_ns(void);
uint64_t vpmu_estimated_io_mem_time_ns(void);
uint64_t vpmu_estimated_execution_time_ns(void);
uint64_t vpmu_L1_dcache_access_count(void);
uint64_t vpmu_L1_dcache_miss_count(void);
uint64_t vpmu_L1_dcache_read_miss_count(void);
uint64_t vpmu_L1_dcache_write_miss_count(void);
uint64_t vpmu_L2_dcache_access_count(void);
uint64_t vpmu_L2_dcache_miss_count(void);
uint64_t vpmu_L2_dcache_read_miss_count(void);
uint64_t vpmu_L2_dcache_write_miss_count(void);
uint64_t vpmu_L1_icache_access_count(void);
uint64_t vpmu_L1_icache_miss_count(void);
VPMU_Cache_Model vpmu_get_cache_config(void);
void VPMU_DUMP_READABLE_MSG(VPMU *vpmu);
void vpmu_dump_machine_state_v2(void *opaque);


uint32_t vpmu_vpd_mpu_power(void);
uint32_t vpmu_vpd_lcd_power(void);
uint32_t vpmu_vpd_audio_power(void);
uint32_t vpmu_vpd_net_power(void);
uint32_t vpmu_vpd_gps_power(void);
uint32_t vpmu_vpd_total_power(void);

void *vpmu_shm_allocate(const char *name, int32_t size);

void vpmu_update_eet_epc(void);
uint32_t vpmu_estimated_execution_time(void);

uint64_t VPMU_cache_init(void *);
uint64_t VPMU_cache_free(void *);
uint64_t VPMU_cache_reset(void *);
uint64_t VPMU_cache_sync(void *);
uint64_t VPMU_cache_dump(void *);
uint64_t VPMU_cache_sync_callback(void (*callback)(void *), void *);
void     VPMU_cache_sync_none_blocking(void);

uint64_t VPMU_branch_init(void *);
uint64_t VPMU_branch_free(void *);
uint64_t VPMU_branch_reset(void *);
uint64_t VPMU_branch_sync(void *);
uint64_t VPMU_branch_dump(void *);

int32_t get_json_int(struct json_object *, char *);
int64_t get_json_int64(struct json_object *, char *);
const char *get_json_str(struct json_object *, char *);
const char *get_str_object(struct json_object *, char *);
int32_t check_json_field(struct json_object *, char *);
//QEMU needs
void cache_ref(uint8_t, uint8_t, uint32_t, uint16_t, uint16_t);
void branch_ref(uint8_t, uint32_t, uint32_t, uint32_t);
void vpmu_core_init(int32_t argc, char **argv);

void load_arm_inst_cycles(struct json_object *);
ARM_Instructions get_index_of_arm_inst(const char *);

void tic(struct timespec *start);
uint64_t toc(struct timespec *start, struct timespec *end);
void print_color_time(const char *str, uint64_t time);

void model_sel_ref(uint8_t, uint8_t, uint32_t, uint16_t, uint16_t, ExtraTBInfo *);
#endif
