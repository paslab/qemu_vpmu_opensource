#include "vpmu.h"

inline void tic(struct timespec *t1)
{
    clock_gettime(CLOCK_REALTIME, t1);
}

inline uint64_t toc(struct timespec *t1, struct timespec *t2)
{
    uint64_t period = 0;

    clock_gettime(CLOCK_REALTIME, t2);
    period = t2->tv_nsec - t1->tv_nsec;
    period += (t2->tv_sec - t1->tv_sec) * 1000000000;

    return period;
}

void print_color_time(const char *str, uint64_t time)
{
    CONSOLE_LOG(BASH_COLOR_GREEN); //Color Code - Green 
    CONSOLE_LOG("%s: %0.3lf ms", str, (double)time / 1000000.0);
    CONSOLE_LOG(BASH_COLOR_NONE"\n\n"); //Terminate Color Code
}


