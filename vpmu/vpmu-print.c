#include "vpmu.h"

//tianman
#ifdef CONFIG_VPMU_VFP
void print_vfp_count(void)
{
#define etype(x) macro_str(x)
    int i;
    uint64_t counted = 0;
    uint64_t total_counted = 0;
    static const char *str_arm_vfp_instructions[] = { ARM_INSTRUCTION };

    for (i = 0; i < ARM_VFP_INSTRUCTION_TOTAL_COUNTS; i++) {
        if (GlobalVPMU.VFP_count[i] > 0) {
            CONSOLE_LOG( "%s: %llu ", 
                    str_arm_vfp_instructions[i], 
                    GlobalVPMU.VFP_count[i]);
            CONSOLE_LOG( "need = %d spend cycle = %llu\n", 
                    arm_vfp_instr_time[i], 
                    GlobalVPMU.VFP_count[i] * arm_vfp_instr_time[i]);

            if (i < (ARM_VFP_INSTRUCTION_TOTAL_COUNTS - 2)){
                counted += GlobalVPMU.VFP_count[i];
                total_counted += GlobalVPMU.VFP_count[i] * arm_vfp_instr_time[i];
            }

        }
    }
    //CONSOLE_LOG( "total latency: %llu\n", GlobalVPMU.VFP_BASE);
    //CONSOLE_LOG( "Counted instructions: %llu\n", counted);
    //CONSOLE_LOG( "total Counted cycle: %llu\n", total_counted);
    CONSOLE_LOG("VFP : total latency: %llu\n", GlobalVPMU.VFP_BASE);
    CONSOLE_LOG("VFP : Counted instructions: %llu\n", counted);
    CONSOLE_LOG("VFP : total Counted cycle: %llu\n", total_counted);
#undef etype
}
#endif

uint64_t vpmu_total_insn_count(void)
{
    return GlobalVPMU.USR_icount
        + GlobalVPMU.SVC_icount
        + GlobalVPMU.sys_call_icount
        + GlobalVPMU.sys_rest_icount;
}

uint64_t vpmu_total_ldst_count(void)
{
    return GlobalVPMU.USR_ldst_count
        + GlobalVPMU.SVC_ldst_count
        + GlobalVPMU.IRQ_ldst_count
        + GlobalVPMU.sys_call_ldst_count
        + GlobalVPMU.sys_rest_ldst_count;
}

uint64_t vpmu_total_load_count(void)
{
    return GlobalVPMU.USR_load_count
        + GlobalVPMU.SVC_load_count
        + GlobalVPMU.IRQ_load_count
        + GlobalVPMU.sys_call_load_count
        + GlobalVPMU.sys_rest_load_count;
}

uint64_t vpmu_total_store_count(void)
{
    return GlobalVPMU.USR_store_count
        + GlobalVPMU.SVC_store_count
        + GlobalVPMU.IRQ_store_count
        + GlobalVPMU.sys_call_store_count
        + GlobalVPMU.sys_rest_store_count;
}

uint64_t vpmu_branch_insn_count(void)
{
    return GlobalVPMU.branch_count;
}

uint64_t vpmu_branch_predict(uint8_t request)
{
    /* VPMU sync with branch predictor */
    VPMU_branch_sync(NULL);
    if(request == PREDICT_CORRECT)
        return GlobalVPMU.branch_predict_correct;
    if(request == PREDICT_WRONG)
        return GlobalVPMU.branch_predict_wrong;
    CONSOLE_LOG("%s" "unexpected condition\n",__FUNCTION__);
    return 0;
}

uint64_t vpmu_branch_predict_correct(void)
{
    return vpmu_branch_predict(PREDICT_CORRECT);
}

uint64_t vpmu_branch_predict_wrong(void)
{
    return vpmu_branch_predict(PREDICT_WRONG);
}

uint64_t vpmu_cycle_count(void)
{
    return GlobalVPMU.ticks 
        + vpmu_sys_mem_access_cycle_count() 
        + vpmu_io_mem_access_cycle_count(); 
}

uint64_t vpmu_pipeline_cycle_count(void)
{
    return GlobalVPMU.ticks;
}

uint64_t vpmu_estimated_pipeline_time_ns(void)
{
    /* FIXME...
     * Use last_pipeline_cycle_count to save tmp data
     * as did above by using GlobalVPMU.last_ticks.
     */
    return vpmu_pipeline_cycle_count() / (GlobalVPMU.cpu_model.frequency / 1000.0);
}

uint64_t vpmu_estimated_sys_mem_time_ns(void)
{
    /* FIXME...
     * Use last_pipeline_cycle_count to save tmp data
     * as did above by using GlobalVPMU.last_ticks.
     */
    return vpmu_sys_mem_access_cycle_count() / (GlobalVPMU.cpu_model.frequency / 1000.0);
}

uint64_t vpmu_estimated_io_mem_time_ns(void)
{
    /* FIXME...
     * Use last_pipeline_cycle_count to save tmp data
     * as did above by using GlobalVPMU.last_ticks.
     */
    return vpmu_io_mem_access_cycle_count() / (GlobalVPMU.cpu_model.frequency / 1000.0);
}

uint64_t vpmu_estimated_execution_time_ns(void)
{
    //evo0209 : [Bug] the eet_ns is not match the sum of total pipeline + mem + io
    //vpmu_update_eet_epc();
    //return GlobalVPMU.eet_ns;
    return vpmu_estimated_pipeline_time_ns()
           + vpmu_estimated_io_mem_time_ns()
           + vpmu_estimated_sys_mem_time_ns();
}

void VPMU_DUMP_READABLE_MSG(VPMU *vpmu)
{
    int i;

//These two are for making info formatable and maintainable
#define CONSOLE_U64(str, val) CONSOLE_LOG(str" %'"PRIu64"\n", (uint64_t)val)
#define CONSOLE_TME(str, val) CONSOLE_LOG(str" %'lf sec\n", (double)val / 1000000000.0)
    CONSOLE_LOG("Instructions:\n");
    CONSOLE_U64(" Total instruction count       :", vpmu_total_insn_count());
    CONSOLE_U64("  ->User mode insn count       :", vpmu->USR_icount);
    CONSOLE_U64("  ->Supervisor mode insn count :", vpmu->SVC_icount);
    CONSOLE_U64("  ->IRQ mode insn count        :", vpmu->IRQ_icount);
    CONSOLE_U64("  ->Other mode insn count      :", vpmu->sys_rest_icount);
    CONSOLE_U64(" Total ld/st instruction count :", vpmu_total_ldst_count());
    CONSOLE_U64("  ->User mode ld/st count      :", vpmu->USR_ldst_count);
    CONSOLE_U64("  ->Supervisor mode ld/st count:", vpmu->SVC_ldst_count);
    CONSOLE_U64("  ->IRQ mode ld/st count       :", vpmu->IRQ_ldst_count);
    CONSOLE_U64("  ->Other mode ld/st count     :", vpmu->sys_rest_ldst_count);
    CONSOLE_U64(" Total load instruction count  :", vpmu_total_load_count());
    CONSOLE_U64("  ->User mode load count       :", vpmu->USR_load_count);
    CONSOLE_U64("  ->Supervisor mode load count :", vpmu->SVC_load_count);
    CONSOLE_U64("  ->IRQ mode load count        :", vpmu->IRQ_load_count);
    CONSOLE_U64("  ->Other mode load count      :", vpmu->sys_rest_load_count);
    CONSOLE_U64(" Total store instruction count :", vpmu_total_store_count());
    CONSOLE_U64("  ->User mode store count      :", vpmu->USR_store_count);
    CONSOLE_U64("  ->Supervisor mode store count:", vpmu->SVC_store_count);
    CONSOLE_U64("  ->IRQ mode store count       :", vpmu->IRQ_store_count);
    CONSOLE_U64("  ->Other mode store count     :", vpmu->sys_rest_store_count);
    CONSOLE_LOG("Memory:\n");
    CONSOLE_U64("  ->System memory access       :", (vpmu_L1_dcache_miss_count() 
                                                 + vpmu_L1_icache_miss_count()));
    CONSOLE_U64("  ->System memory cycles       :", vpmu_sys_mem_access_cycle_count());
    CONSOLE_U64("  ->I/O memory access          :", vpmu->iomem_count);
    CONSOLE_U64("  ->I/O memory cycles          :", vpmu_io_mem_access_cycle_count());
    CONSOLE_U64("Total Cycle count              :", vpmu_cycle_count());
    //Remember add these infos into L1I READ
    CONSOLE_LOG("Model Selection:\n");
    CONSOLE_U64("  ->JIT icache access          :", (vpmu->hot_icache_count));
    CONSOLE_U64("  ->JIT dcache access          :", (vpmu->hot_dcache_read_count + vpmu->hot_dcache_write_count));
    CONSOLE_U64("  ->VPMU icache access         :", vpmu_L1_icache_access_count());
    CONSOLE_U64("  ->VPMU icache misses         :", vpmu_L1_icache_miss_count());
    CONSOLE_U64("  ->VPMU dcache access         :", vpmu_L1_dcache_access_count());
    CONSOLE_U64("  ->VPMU dcache read misses    :", vpmu_L1_dcache_read_miss_count());
    CONSOLE_U64("  ->VPMU dcache write misses   :", vpmu_L1_dcache_write_miss_count());
    CONSOLE_U64("  ->hotTB                      :", GlobalVPMU.hot_tb_visit_count);
    CONSOLE_U64("  ->coldTB                     :", GlobalVPMU.cold_tb_visit_count);

    for (i = 0; i < GlobalVPMU.num_of_sims; i++) {
        if(GlobalVPMU.sims[i].fun[SIM_FUN_DUMP] != NULL)
            GlobalVPMU.sims[i].fun[SIM_FUN_DUMP](NULL);
        // This sync would ensure serializtion of iterating dump from vpmu-core.c
        // Wait till the simulator finishes printing
        if(GlobalVPMU.sims[i].fun[SIM_FUN_SYNC] != NULL)
            GlobalVPMU.sims[i].fun[SIM_FUN_SYNC](NULL);
    }

    CONSOLE_LOG("\n");
    CONSOLE_LOG("Timing Info:\n");
    CONSOLE_TME("  ->Pipeline                   :", vpmu_estimated_pipeline_time_ns());
    CONSOLE_TME("  ->System memory              :", vpmu_estimated_sys_mem_time_ns());
    CONSOLE_TME("  ->I/O memory                 :", vpmu_estimated_io_mem_time_ns());
    CONSOLE_TME("Estimated execution time       :", vpmu_estimated_execution_time_ns());

    CONSOLE_LOG("\n");
    CONSOLE_TME("Emulation Time :", vpmu_wall_clock_period(vpmu));
    CONSOLE_LOG("MIPS           : %'0.2lf\n\n", (double)vpmu_total_insn_count() 
                                              / (vpmu_wall_clock_period(vpmu) / 1000.0));
#undef CONSOLE_TME
#undef CONSOLE_U64
}

