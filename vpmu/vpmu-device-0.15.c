/*
 * Virtual Performance Monitor Unit
 * Copyright (c) 2009 PAS Lab, CSIE & GINM, National Taiwan University, Taiwan.
 */
#include <sys/syscall.h>
#include <sys/types.h>
#include <unistd.h>

#include "sysemu.h"
#include "net.h"
#include <string.h>
#include "qemu-queue.h"
#include "qemu-thread.h"
#include "qemu-char.h"
#include "console.h"
#include "config-host.h"
//#include "qemu-lock.h"
#include "qemu-timer.h"
#include "../hw/sysbus.h"
#include "../hw/hw.h"
#include "../hw/devices.h"
#include "../hw/boards.h"
#include "../hw/arm-misc.h"

#include "vpmu.h"
#include "vpmu-module.h"
//#include "SET/SET_event.h"

/******************************
 * Global variables.
 *
 */
/* QEMU realted device information, such as BUS, IRQ, VPMU strucutre. */
typedef struct VPMUState
{
    SysBusDevice     busdev;
    qemu_irq         irq;
    VPMU             *VPMU;
    QLIST_ENTRY(VPMUState) entries;
}VPMUState;
typedef struct VPMUState_t 
{
    CharDriverState *vpmu_monitor;
    QLIST_HEAD(VPMUStateList, VPMUState) head;
}VPMUState_t;

/* Global variables for VPMU counters. */
//extern struct VPMU GlobalVPMU;
VPMUState_t vpmu_t;

//chiaheng
#ifdef CONFIG_SET 
//#include "vpmu_sampling.c"
#endif

#ifdef CONFIG_ANDROID
#include "goldfish_device.h" //goldfish_battery_set_prop()
#include "power_supply.h" //POWER_SUPPLY_PROP_CAPACITY
extern struct android_dev_stat adev_stat;
#endif

/* Indicator of status of VPMU. */
volatile int VPMU_enabled = 0;

void vpmu_init(uint32_t base, qemu_irq irq);

/* turn "on" simulator don't turn simulator off */
static void vpmu_simulator_turn_on(VPMU* vpmu, uint32_t mask)
{
    vpmu->timing_model |= mask;
}

static void vpmu_model_setup(VPMU* vpmu, uint32_t model)
{
    vpmu->timing_model = model;

    switch (model) {
        case TIMING_MODEL_A:
            vpmu_simulator_turn_on(vpmu, VPMU_INSN_COUNT_SIM);
            break;
        case TIMING_MODEL_B:
            vpmu_simulator_turn_on(vpmu, VPMU_INSN_COUNT_SIM | VPMU_PIPELINE_SIM);
            break;
        case TIMING_MODEL_C:
            vpmu_simulator_turn_on(vpmu, VPMU_INSN_COUNT_SIM | VPMU_DCACHE_SIM
                    | VPMU_ICACHE_SIM | VPMU_PIPELINE_SIM);
            break;
        case TIMING_MODEL_D:
            vpmu_simulator_turn_on(vpmu, VPMU_PIPELINE_SIM | VPMU_DCACHE_SIM
                    | VPMU_ICACHE_SIM);
            break;
        case TIMING_MODEL_E:
            vpmu_simulator_turn_on(vpmu, VPMU_INSN_COUNT_SIM | VPMU_DCACHE_SIM
                    | VPMU_ICACHE_SIM | VPMU_BRANCH_SIM | VPMU_PIPELINE_SIM);
            break;
            //evo0209 : fixed for SET plus insn_count_sim
        case TIMING_MODEL_F:
            vpmu_simulator_turn_on(vpmu, VPMU_INSN_COUNT_SIM | VPMU_PIPELINE_SIM);
            break;
        case TIMING_MODEL_G:
            vpmu_simulator_turn_on(vpmu, VPMU_INSN_COUNT_SIM | VPMU_DCACHE_SIM
                    | VPMU_ICACHE_SIM | VPMU_PIPELINE_SIM);
            break;
        default:
            DBG("function \"%s\" : nothing to setup\n",__FUNCTION__);
            break;
    }

    //evo0209
    tic(&(vpmu->start_time));
}

/* paslab : monitoring init */
static uint32_t special_read(void *opaque, target_phys_addr_t addr)
{  
    uint64_t ret = 0;

    CONSOLE_LOG("read qemu device at addr=0x%x\n", addr);
    switch(addr) {
        case VPMU_MMAP_GETCLOCKTIME:
            GlobalVPMU.eet_ptr[0] = 0;
            GlobalVPMU.eet_ptr[1] = 0;

            break;	
        case VPMU_MMAP_GETTIMER:
            GlobalVPMU.eet_ptr[0] = vpmu_estimated_execution_time_ns();
            ret = GlobalVPMU.eet_ptr[1];
            break;	
        default:
            DBG("read unknow address 0x%.4x of vpd\n", addr);
    }

    return ret;
}

/* paslab : addr was 32-bit addr in 0.9.1 but offset in 0.10.5 ??? */
//extern CharDriverState *serial_hds[MAX_SERIAL_PORTS];
static void special_write(void *opaque, target_phys_addr_t addr, uint32_t value) 
{
    static int find_entry = 0;
#ifdef CONFIG_ANDROID
    static int count = 0, timer = 0;
    static uint32_t last_packets = 0, last_audio = 0;
#endif
    VPMU *vpmu=((VPMUState *)opaque)->VPMU;

    CONSOLE_LOG("write qemu device at addr=0x%x value=%d\n", addr, value);
    switch(addr) {
        case VPMU_MMAP_ENABLE:
            if(value == 0 || (value <= 10 && value >= 2))
            {
                VPMU_reset();
                VPMU_enabled = 1;

                switch(value)
                {
                    case 2:
                        vpmu_model_setup(vpmu, TIMING_MODEL_A);
                        vpmu_simulator_status(vpmu, 0);
                        break;
                    case 3:
                        vpmu_model_setup(vpmu, TIMING_MODEL_B);
                        vpmu_simulator_status(vpmu, 0);
                        break;
                    case 4:
                        vpmu_model_setup(vpmu, TIMING_MODEL_C);
                        vpmu_simulator_status(vpmu, 0);
                        break;
                    case 5:
                        vpmu_model_setup(vpmu, TIMING_MODEL_D);
                        vpmu_simulator_status(vpmu, 0);
                        break;
                    case 6:
                        vpmu_model_setup(vpmu, TIMING_MODEL_E);
                        vpmu_simulator_status(vpmu, 0);
                        break;
                        //tianman
                    case 7:
#ifdef CONFIG_SET
                        int ii;
                        for (ii = 0; ii < 512; ii++) {
                            if (pid_base[ii][0] == 1)
                                CONSOLE_LOG("PID: %d, Process_name: %s\n", 
                                        ii, pid_name_base[ii]);
                        }
#endif
                        break;
                    case 8:
                        vpmu_model_setup(vpmu, TIMING_MODEL_E);
                        //JIT model selection
                        vpmu_simulator_turn_on(vpmu, VPMU_MODEL_SELECT);
                        vpmu_simulator_status(vpmu, 0);
                        break;
                    default:
                        CONSOLE_LOG("Use default timing model\n");
                        vpmu_model_setup(vpmu, TIMING_MODEL_A);
                        vpmu_simulator_status(vpmu, 0);
                        break;
                }
            }   
            else if(value == 1)
            { 
                VPMU_sync();
                toc(&(vpmu->start_time), &(vpmu->end_time));

                if((vpmu->dump_fp) != NULL)
                    fclose(vpmu->dump_fp);
                vpmu->dump_fp = NULL;
                VPMU_dump_result();

                VPMU_enabled = 0;
#ifdef CONFIG_VPMU_VFP
                //	FILE_LOG("Before print_vfp_count \n");
                print_vfp_count();
#endif

            } else if(value == 11) {
                toc(&(vpmu->start_time), &(vpmu->end_time));
                //unsigned long long time = vpmu_wall_clock_period(vpmu);
                VPMU_dump_result();

                tic(&(vpmu->start_time));
            } else if (value == 16) {
                CONSOLE_LOG("total power: %d\n", vpmu_vpd_total_power());
                CONSOLE_LOG("  mpu power: %d\n", vpmu_vpd_mpu_power());
            }
            break;
        case VPMU_MMAP_BYPASS_ISR_ADDR:
            if (find_entry < NUM_FIND_SYMBOLS)
            {
                /* timer irq entry of target OS */
                ISR_addr[find_entry] = value;
                DBG( "ISR_addr[%d] = %8x\n", find_entry, value);
                find_entry++;
            }
            break;
        case 0x0011:
            CONSOLE_LOG("Total instruction count : %" PRIu64 " \n", 
                    vpmu_total_insn_count());
            break;
        case 0x0012:
            CONSOLE_LOG("Total branch instruction count : %" PRIu64 " \n", 
                    vpmu_branch_insn_count());
            break;
        case 0x0013:
            CONSOLE_LOG("Data memory access count : %" PRIu64 " \n", 
                    vpmu_L1_dcache_access_count());
            break;
        case 0x0014:
            CONSOLE_LOG("Cycle count : %" PRIu64 " \n", vpmu_cycle_count());
            break;
        case 0x0015:
            CONSOLE_LOG("Estimated execution time : %u\n", 
                    vpmu_estimated_execution_time());
            break;
        case VPMU_MMAP_POWER_ENABLE:
            CONSOLE_LOG("VPMU status : %s\n", VPMU_enabled ? "enable" : "disable");
            break;
#ifdef CONFIG_ANDROID
        case VPMU_MMAP_BYPASS_CPU_UTIL:
            if(count == 0) {
                //chiaheng
                //DBG( "VPMU: start power sampling\n");
                adev_stat.energy_left = VPD_BAT_CAPACITY * VPD_BAT_VOLTAGE * 3600 *
                    (*(adev_stat.bat_capacity)/100.0);
                last_packets = *(adev_stat.net_packets);
            }
            else if(count % 2 == 0) {
                adev_stat.cpu_util = value;
                adev_stat.audio_on = last_audio + *(adev_stat.audio_buf);
                if(adev_stat.net_mode == 1) { //Wifi mode
                    if(*(adev_stat.net_packets) - last_packets > 8) {
                        adev_stat.net_on = 1;
                    }
                    else {
                        adev_stat.net_on = 0;
                    }
                }
                else if(adev_stat.net_mode == 2) { //3G mode
                    if(*(adev_stat.net_packets) > last_packets) {
                        timer = 0;
                        adev_stat.net_on = 1;
                    }
                    else { //*(adev_stat.net_packets) == last_packets
                        timer++;
                        if(timer >= 6) adev_stat.net_on = 0;
                    }
                }
                last_packets = *(adev_stat.net_packets);
                if(*(adev_stat.bat_ac_online) == 0 && adev_stat.battery_sim == 1) {
                    if(adev_stat.energy_left > vpmu_vpd_total_power())
                        adev_stat.energy_left -= vpmu_vpd_total_power();
                    else
                        adev_stat.energy_left = 0;
                    goldfish_battery_set_prop(0,
                            POWER_SUPPLY_PROP_CAPACITY, 
                            adev_stat.energy_left * 100.0 
                            / (VPD_BAT_CAPACITY * VPD_BAT_VOLTAGE * 3600));
                }
            }
            else {
                last_audio = *(adev_stat.audio_buf);
            }
            count++;
            break;
        case VPMU_MMAP_SELECT_NET_MODE:
            if(value >= 0 || value <= 2)
                adev_stat.net_mode = value;
            break;
        case VPMU_MMAP_BATTERY_ENABLE:
            if(value == 0 || value == 1)
                adev_stat.battery_sim = value;
            break;
#endif
        default:
            CONSOLE_LOG("write 0x%d to unknow address 0x%.4x of vpd\n", value, addr);
    }
}

static CPUReadMemoryFunc *special_read_func[] = {
    special_read,
    special_read,
    special_read
};

static CPUWriteMemoryFunc *special_write_func[] = {
    special_write,
    special_write,
    special_write
};

static int vpmu_init_sysbus(SysBusDevice *dev)
{
    int iomemtype = 0;
    VPMUState *s = FROM_SYSBUS(VPMUState, dev);

    CharDriverState* chr = qdev_init_chardev(&dev->qdev);

    s->VPMU = &GlobalVPMU;
    vpmu_t.vpmu_monitor= chr ?: qemu_chr_open("vpmu", "vc", NULL);
    QLIST_INSERT_HEAD(&vpmu_t.head, s, entries);

#ifdef CONFIG_ANDROID
    iomemtype = cpu_register_io_memory(
            special_read_func, 
            special_write_func, 
            s);
#else
    iomemtype = cpu_register_io_memory(
            special_read_func, 
            special_write_func, 
            s, 
            DEVICE_NATIVE_ENDIAN);
#endif

    sysbus_init_mmio(dev, VPMU_IOMEM_SIZE, iomemtype);

    sysbus_init_irq(dev, &s->irq);

    return 0;
}

static void vpmu_register_devices(void)
{
    QLIST_INIT(&vpmu_t.head);
    sysbus_register_dev("vpmu", sizeof(VPMUState), vpmu_init_sysbus);
}

void vpmu_init(uint32_t base, qemu_irq irq)
{
    DeviceState *dev;
    SysBusDevice *s;

    CONSOLE_LOG("init vpmu device on addr 0x%x. \n", base);
    dev = qdev_create(NULL, "vpmu");
    qdev_init_nofail(dev);
    s = sysbus_from_qdev(dev);
    sysbus_mmio_map(s, 0, base);
    sysbus_connect_irq(s, 0, irq);
}

device_init(vpmu_register_devices);
