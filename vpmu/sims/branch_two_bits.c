void two_bits_branch_predictor(Branch_Data *data, Branch_Reference *ref)
{
    int taken = ref->taken;

    switch (data->predictor)
    {
        /* predict not taken */
        case 0:
            if (taken) {
                data->predictor = 1;
                data->wrong++;
            } else
                data->correct++;
            break;
        case 1:
            if (taken) {
                data->predictor = 3;
                data->wrong++;
            } else {
                data->predictor = 0;
                data->correct++;
            }
            break;
            /* predict taken */
        case 2:
            if (taken) {
                data->predictor = 3;
                data->correct++;
            } else {
                data->predictor = 0;
                data->wrong++;
            }
            break;
        case 3:
            if (taken)
                data->correct++;
            else {
                data->predictor = 2;
                data->wrong++;
            }
            break;
        default:
            ERR_MSG("predictor error\n");
            exit(1);
    }
    //if(taken)
    //    DBG("taken\n");
    //else
    //    DBG("not taken\n");
    //DBG("predictor state=%d right=%u wrong=%u\n", predictor, branch_predict_correct, branch_predict_wrong);
}

//This is for registering the entry function of your own algorithm->
void two_bits_register(void) {
    //This name maps to the one in JSON configuration->
    branch_sim_table[model_num].name = strdup("two_bits");
    //This is the entry function of your own algorithm->
    branch_sim_table[model_num].entry = two_bits_branch_predictor;
    CONSOLE_LOG("\tRegister model: %s\n", branch_sim_table[model_num].name);
    model_num++;
}

