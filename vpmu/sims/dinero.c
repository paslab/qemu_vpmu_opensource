#include <sys/prctl.h>
#include "../d4-7/d4.h"

#define LRU           ((char *)"LRU")
#define FIFO          ((char *)"FIFO")
#define RANDOM        ((char *)"RANDOM")
#define DEMAND_ONLY   ((char *)"DEMAND_ONLY")
#define ALWAYS        ((char *)"ALWAYS")
#define MISS          ((char *)"MISS")
#define TAGGED        ((char *)"TAGGED")
#define LOAD_FORWARD  ((char *)"LOAD_FORWARD")
#define SUB_BLOCK     ((char *)"SUB_BLOCK")
#define IMPOSSIBLE    ((char *)"IMPOSSIBLE")
#define NEVER         ((char *)"NEVER")
#define NO_FETCH      ((char *)"NO_FETCH")

#define MAX_D4_CACHES 128

/*    Sample cache topology
 *            L2
 *          /    \
 *        L1      L1
 *      D   I   D   I
 * ----------------------------
 *  [L2, L1D, L1D, L1I, L1I]
 *  is the array representing the tree topology above
 */
typedef struct D4_CACHE_CONFIG {
    d4cache *cache;
    enum Cache_Data_Level level;
} D4_CACHE_CONFIG;

// This is a structure that sums the dinero cache data
// It's only used in the following situations
// 1. Synchronizing the data from dinero to VPMU
// 2. Printing the results of dinero cache simulation
typedef struct {
    double fetch_data, fetch_read, fetch_alltype;
    double data, data_read, data_alltype;
} Demand_Data;

static d4cache *d4_cache_leaf[MAX_D4_CACHES] = {0};
static D4_CACHE_CONFIG d4_cache[MAX_D4_CACHES] = {{0}};
static uint32_t num_cores[MAX_D4_CACHES] = {0};
static uint32_t core_num_table[MAX_D4_CACHES] = {0};
static uint32_t d4_num_caches = 0;
static uint32_t d4_levels = 0;
static int flag_json_config_has_processor[ALL_PROC] = {0};

static d4cache* d4_mem_create(void)
{
    d4cache *mem = d4new( NULL );

    if (mem == NULL)
        ERR_MSG("Main mem error \n" );
    mem->name = strdup("dinero Main Memory ");

    return mem ;
}

static void reset_single_d4_cache(d4cache *c)
{
    //Clear the whole memory region of counters, including
    //General Misses:    fetch, miss, blockmiss,
    //Compulsory Misses: comp_miss, comp_blockmiss
    //Capacity Misse:    scap_miss, cap_blockmiss
    //Conflict Misses:   conf_miss, conf_blockmiss
    memset(c->fetch, 0, sizeof(double) * (2 * D4NUMACCESSTYPES) * 9);
    c->multiblock = 0;
    c->bytes_read = 0;
    c->bytes_written = 0;
}

static Demand_Data inline calculate_data(d4cache *c)
{
    Demand_Data d;

    d.fetch_data = c->fetch[D4XMISC] + c->fetch[D4XREAD] + c->fetch[D4XWRITE];
    d.fetch_read = c->fetch[D4XMISC] + c->fetch[D4XREAD] + c->fetch[D4XINSTRN];
    d.fetch_alltype = d.fetch_read + c->fetch[D4XWRITE];
    d.data = c->miss[D4XMISC] + c->miss[D4XREAD] + c->miss[D4XWRITE];
    d.data_read = c->miss[D4XMISC] + c->miss[D4XREAD] + c->miss[D4XINSTRN];
    d.data_alltype = d.data_read + c->miss[D4XWRITE];

    return d;
}

static void inline sync_cache_data(Cache_Data *data)
{
    int i, level, processor;
    Demand_Data d;

    for (processor = 0; processor < ALL_PROC; processor++) {
        // Loop through from L1 to max level of current cache configuration
        for (level = L1_CACHE; level <= d4_levels; level++) {
            // Loop through all the processor cores
            for (i = 0; i < num_cores[processor]; i++) {
                int index;

                index = core_num_table[processor] + // the offset of processor
                        num_cores[processor] +      // the offset of i-cache
                        i;                          // the offset of core
                d = calculate_data(d4_cache_leaf[index]);
                data->inst_cache[level][i][CACHE_READ] = d.fetch_alltype;
                data->inst_cache[level][i][CACHE_WRITE] = 0;
                data->inst_cache[level][i][CACHE_READ_MISS] = d.data_alltype;
                data->inst_cache[level][i][CACHE_WRITE_MISS] = 0;

                index = core_num_table[processor] + // the offset of processor
                        0 +                         // the offset of d-cache
                        i;                          // the offset of core
                d = calculate_data(d4_cache_leaf[index]);
                data->data_cache[level][i][CACHE_READ] = d.fetch_read;
                data->data_cache[level][i][CACHE_WRITE] =
                  d4_cache_leaf[index]->fetch[D4XWRITE];
                data->data_cache[level][i][CACHE_READ_MISS] = d.data_read;
                data->data_cache[level][i][CACHE_WRITE_MISS] =
                  d4_cache_leaf[index]->miss[D4XWRITE];
            }
        }
    }
}

static void inline sync_dump_info(Cache_Data *data, int id)
{
    int i;

    sync_cache_data(data);

    CONSOLE_LOG("  [%d] type : dinero\n", id);
    // Dump info
    CONSOLE_LOG("       (Miss Rate)   " \
                "|    Access Count    " \
                "|   Read Miss Count  " \
                "|  Write Miss Count  " \
                "|\n");
    // Memory
    d4cache *c = d4_cache[0].cache;
    Demand_Data d = calculate_data(c);
    CONSOLE_LOG("    -> memory (%0.2lf) |%'20" PRIu64 "|%'20" PRIu64 "|%'20" PRIu64 "|\n",
                (double)0.0,
                (uint64_t)(d.fetch_read),
                (uint64_t)0,
                (uint64_t)0);

    for (i = 1; i < MAX_D4_CACHES && d4_cache[i].cache != NULL; i++) {
        d4cache *c = d4_cache[i].cache;
        Demand_Data d = calculate_data(c);

        if (c->flags & D4F_RO) {
            // i-cache
            CONSOLE_LOG(
                    "    -> L%d-I   (%0.2lf) |%'20"PRIu64"|%'20"PRIu64"|%'20"PRIu64"|\n",
                    d4_cache[i].level,
                    (double)d.data_read / d.fetch_read,
                    (uint64_t)(d.fetch_read),
                    (uint64_t)(d.data_read),
                    (uint64_t)0);
        }
        else {
            // d-cache
            CONSOLE_LOG(
                    "    -> L%d-D   (%0.2lf) |%'20"PRIu64"|%'20"PRIu64"|%'20"PRIu64"|\n",
                    d4_cache[i].level,
                    d.data_alltype / d.fetch_alltype,
                    (uint64_t)(d.fetch_alltype),
                    (uint64_t)(d.data_read),
                    (uint64_t)(c->miss[D4XWRITE]));
        }
    }
}

//Handles types of packets
static void inline packet_processor(Cache_Package *pkg, Cache_Reference *ref, int id)
{
    int i;
    int index = 0;
    d4memref d4_ref;
#ifdef CONFIG_VPMU_DEBUG_MSG
    static uint64_t debug_packet_num_cnt = 0;
    debug_packet_num_cnt++;
#endif

    //Calculate the index of target cache reference index
    if (ref->type == CACHE_PACKET_INSTRN)
        index = core_num_table[ref->processor] + //the offset of processor
            num_cores[ref->processor] +          //the offset of i-cache
            ref->core;                           //the offset of core
    else
        index = core_num_table[ref->processor] + //the offset of processor
            0 +                                  //the offset of d-cache
            ref->core;                           //the offset of core
    d4_ref.address = ref->addr;
    d4_ref.accesstype = (uint8_t)ref->type;
    d4_ref.size = ref->size;

    //DBG("ENQ: CORE=%d TYPE=%d ADDR=%x SIZE=%d\n", ref->core, d4_ref.accesstype, d4_ref.address, d4_ref.size);
    //DBG("ENQ: index=%d (%x)\n", index, d4_cache_leaf[index]);
    //Every simulators should handle VPMU_BARRIER_PACKET to support synchronization
    //The implementation depends on your own packet type and writing style
    switch (ref->type) {
        case VPMU_PACKET_BARRIER:
            sync_cache_data(&pkg->data);
            break;
        case VPMU_PACKET_SYNC_DATA:
            //Sync only ensure the data in the array is up to date to and not ahead of
            //the time packet processor receive the sync packet.
            //Slave can stealthily do simulations as long as it does not have a pending
            //sync job.
            while(pkg->synced_flag) usleep(1); //Block till last sync finished
            sync_cache_data(&pkg->data);
            pkg->sync_counter++;  //Increase the timestamp of counter
            pkg->synced_flag = 1; //Set up the flag
            break;
        case VPMU_PACKET_DUMP_INFO:
            shm_wait_token(shm_token, id);
#ifdef CONFIG_VPMU_DEBUG_MSG
            DBG("  [%d] %'"PRIu64" packets received\n", id, debug_packet_num_cnt);
            debug_packet_num_cnt = 0;
#endif
            sync_dump_info(&pkg->data, id);
            shm_pass_token(shm_token);
            break;
        case VPMU_PACKET_RESET:
            for (i = 0; i < MAX_D4_CACHES; i++)
                if (d4_cache[i].cache) reset_single_d4_cache(d4_cache[i].cache);
            break;
        case CACHE_PACKET_READ:
        case CACHE_PACKET_WRITE:
        case CACHE_PACKET_INSTRN:
            //DBG("index=%d, %x\n", index, d4_cache_leaf[index]);
            //Ignore all packets if this configuration does not support
            if (unlikely(num_cores[ref->processor] == 0)) return;
            //Error check
            if (likely(d4_cache_leaf[index]))
                d4ref(d4_cache_leaf[index], d4_ref);
            break;
        default:
            ERR_MSG("Unexpected packet in cache simulators\n");
    }
}

static void dinero_work(Cache_Package *pkg, int id)
{
    static Cache_Reference local_buffer[256];
    const int local_buffer_size = sizeof(local_buffer)  / sizeof(Cache_Reference);
    int num_refs = 0;

    while(1) {
        VPMU_WAIT_JOB(&pkg->cache_job_semaphore,
            //DBG("waked %lx, index=%lu-%lu\n",
            //    (unsigned long)pkg,
            //    cache_trace_buffer_t->start[id],
            //    cache_trace_buffer_t->end);
            //Keep draining jobs till it's empty
            while (likely(shm_isBufferNotEmpty(cache_trace_buffer_t, id))) {
                shm_bulkRead(
                    cache_trace_buffer_t,     //Pointer to ringbuffer
                    id,                       //Index to self
                    local_buffer,             //Pointer to local(private) buffer
                    local_buffer_size,        //#elements of local(private) buffer
                    sizeof(Cache_Reference),  //Size of each elements
                    num_refs);                //#elements read successfully
                for (int i = 0; i < num_refs; i++)
                    packet_processor(pkg, &local_buffer[i], id);
            }
        );
    }

    return ;
}

static void set_d4_cache(d4cache *c, int extra_flag, const char *key, const char *val)
{
#define IF_KEY_IS(_k, _callback) \
    if (strcmp(key, _k) == 0) {\
        _callback ;\
        return ;\
    }

    DBG("\t%s: %s\n", key, val);

    IF_KEY_IS("name",         c->name = strdup(val));
    IF_KEY_IS("processor",    c->name = strdup(val));
    IF_KEY_IS("blocksize",    c->lg2blocksize = clog2(atoi(val)));
    IF_KEY_IS("subblocksize", c->lg2subblocksize = clog2(atoi(val)));
    IF_KEY_IS("size",         c->lg2size = clog2(atoi(val)));
    IF_KEY_IS("assoc",        c->assoc = atoi(val));
    IF_KEY_IS("split_3c_cnt", c->flags |= atoi(val) ? D4F_CCC : 0);
    c->flags |= extra_flag;

    IF_KEY_IS("prefetch_abortpercent",
        c->prefetch_abortpercent = atoi(val));
    IF_KEY_IS("prefetch_distance",
        c->prefetch_distance = atoi(val));

    IF_KEY_IS("replacement",
        c->name_replacement = strdup(val);
        if (strcmp(val, LRU) == 0)
            c->replacementf = d4rep_lru;
        else if (strcmp(val, FIFO) == 0)
            c->replacementf = d4rep_fifo;
        else if (strcmp(val, RANDOM) == 0)
            c->replacementf = d4rep_random;
        else
            ERR_MSG("JSON: not a valid option\n %s: %s\n", key, val);
    );

    IF_KEY_IS("prefetch",
        c->name_prefetch = strdup(val);
        if (strcmp(val, DEMAND_ONLY) == 0)
            c->prefetchf = d4prefetch_none;
        else if (strcmp(val, ALWAYS) == 0)
            c->prefetchf = d4prefetch_always;
        else if (strcmp(val, MISS) == 0)
            c->prefetchf = d4prefetch_miss;
        else if (strcmp(val, TAGGED) == 0)
            c->prefetchf = d4prefetch_tagged;
        else if (strcmp(val, LOAD_FORWARD) == 0)
            c->prefetchf = d4prefetch_loadforw;
        else if (strcmp(val, SUB_BLOCK) == 0)
            c->prefetchf = d4prefetch_subblock;
        else
            ERR_MSG("JSON: not a valid option\n %s: %s\n", key, val);
    );

    IF_KEY_IS("walloc",
        c->name_walloc = strdup(val);
        if (strcmp(val, IMPOSSIBLE) == 0)
            c->wallocf = d4walloc_impossible;
        else if (strcmp(val, ALWAYS) == 0)
            c->wallocf = d4walloc_always;
        else if (strcmp(val, NEVER) == 0)
            c->wallocf = d4walloc_never;
        else if (strcmp(val, NO_FETCH) == 0)
            c->wallocf = d4walloc_nofetch;
        else
            ERR_MSG("JSON: not a valid option\n %s: %s\n", key, val);
    );

    IF_KEY_IS("wback",
        c->name_wback = strdup(val);
        if (strcmp(val, IMPOSSIBLE) == 0)
            c->wbackf = d4wback_impossible;
        else if (strcmp(val, ALWAYS) == 0)
            c->wbackf = d4wback_always;
        else if (strcmp(val, NEVER) == 0)
            c->wbackf = d4wback_never;
        else if (strcmp(val, NO_FETCH) == 0)
            c->wbackf = d4wback_nofetch;
        else
            ERR_MSG("JSON: not a valid option\n %s: %s\n", key, val);
    );

    ERR_MSG("JSON: option not found\n %s: %s\n", key, val);
#undef IF_KEY_IS
}

static d4cache *parse_and_set(json_object *root, D4_CACHE_CONFIG *parent, int extra_flag, int level)
{
    json_object *obj;
    json_object_iter iter;
    d4cache *child = NULL;

    if (json_object_object_get_ex(root, "name", &obj) ||
            json_object_object_get_ex(root, "processor", &obj)) {
        DBG("level:%d ->  %s\n", level, json_object_get_string(obj));
        //Create a new cache configuration
        child = d4new(parent->cache);
        d4_cache[d4_num_caches].cache = child;
        d4_cache[d4_num_caches].level = level;
        d4_num_caches++;
        json_object_object_foreachC(root, iter) {
            //Skip the attribute next
            if (strcmp(iter.key, "next") == 0) continue;
            set_d4_cache(child, extra_flag, iter.key, json_object_get_string(iter.val));
        }
    }

    return child;
}

static int get_processor_index(json_object *obj)
{
    json_object *element;

    json_object_object_get_ex(obj, "processor", &element);
    if (strcmp(json_object_get_string(element), "CPU") == 0)
        return PROCESSOR_CPU;
    else if (strcmp(json_object_get_string(element), "GPU") == 0)
        return PROCESSOR_GPU;

    return -1;
}

//BFS search to form an one dimensional array
static void recursively_parse_json(json_object *root, D4_CACHE_CONFIG *c, int level)
{
    int i, index, local_index = 0;
    json_object *element, *obj;
    json_type type = json_object_get_type(root);

    if (level == NOT_USED) return ; //There's no level 0 cache
    if (type == json_type_array) {
        local_index = d4_num_caches;
        for (i = 0; i < json_object_array_length(root); i++) {
            obj = json_object_array_get_idx(root, i);
            parse_and_set(obj, c, 0x00, level);
        }
        for (i = 0; i < json_object_array_length(root); i++) {
            obj = json_object_array_get_idx(root, i);
            // If there's next level of cache topology, dive into it!
            if (json_object_object_get_ex(obj, "next", &element))
                recursively_parse_json(element, &d4_cache[local_index], level - 1);
        }
    }
    else {
        json_object_object_get_ex(root, "d-cache", &obj);
        int leaf_index = core_num_table[get_processor_index(obj)];
        flag_json_config_has_processor[get_processor_index(obj)] = 1;
        if (json_object_object_get_ex(root, "d-cache", &obj)) {
            index = get_processor_index(obj);
            for (i = 0; i < num_cores[index]; i++) {
                d4_cache_leaf[leaf_index] = parse_and_set(obj, c, 0x00, level);
                leaf_index++;
            }
        }
        if (json_object_object_get_ex(root, "i-cache", &obj)) {
            index = get_processor_index(obj);
            for (i = 0; i < num_cores[index]; i++) {
                d4_cache_leaf[leaf_index] = parse_and_set(obj, c, D4F_RO, level);
                leaf_index++;
            }
        }
    }
}

static void sync_back_config_to_vpmu(Cache_Package *pkg, json_object *root)
{
    json_object *topology;

    //Clear the VPMU cache configuration setting before setting it.
    memset(&pkg->cache_model, 0, sizeof(VPMU_Cache_Model));
    //Copy the model name to VPMU
    strcpy(pkg->cache_model.name, get_json_str(root, "name"));
    pkg->cache_model.levels = d4_levels;
    for (int i = L1_CACHE; i <= d4_levels; i++) {
        char field_str[128];

        sprintf(field_str, "l%d_cache_miss_lat", i);
        pkg->cache_model.latency[i] = get_json_int(root, field_str);
        DBG("%s: %d\n", field_str, pkg->cache_model.latency[i]);
    }

    // Pass some cache configurations to VPMU. Ex: blocksize, walloc, wback
    if (json_object_object_get_ex(root, "topology", &topology)) {
        //The first element in the array must be CPU cache model
        json_object *obj = json_object_array_get_idx(topology, 0);
        //Go through the tree of the CPU cache model
        for (int i = pkg->cache_model.levels; i > 0; i--) {
            json_object *element = obj;

            if (json_object_object_get_ex(element, "next", &obj)) {
                //Data Cache
                pkg->cache_model.d_log2_blocksize[i] =
                    clog2(get_json_int(element, "blocksize"));
                pkg->cache_model.d_log2_blocksize_mask[i] = ~((1 <<
                    clog2(get_json_int(element, "blocksize"))) - 1);
                if (strcmp(get_json_str(element, "walloc"), "ALWAYS") == 0)
                    pkg->cache_model.d_write_alloc[i] = 1;
                if (strcmp(get_json_str(element, "wback"), "ALWAYS") == 0)
                    pkg->cache_model.d_write_back[i] = 1;
            } else {
                //Leaf nodes
                json_object *target;

                //Data Cache
                json_object_object_get_ex(element, "d-cache", &target);
                pkg->cache_model.d_log2_blocksize[i] =
                    clog2(get_json_int(target, "blocksize"));
                pkg->cache_model.d_log2_blocksize_mask[i] = ~((1 <<
                    clog2(get_json_int(target, "blocksize"))) - 1);
                if (strcmp(get_json_str(target, "walloc"), "ALWAYS") == 0)
                    pkg->cache_model.d_write_alloc[i] = 1;
                if (strcmp(get_json_str(target, "wback"), "ALWAYS") == 0)
                    pkg->cache_model.d_write_back[i] = 1;

                //Instruction Cache
                json_object_object_get_ex(element, "i-cache", &target);
                pkg->cache_model.i_log2_blocksize[i] =
                    clog2(get_json_int(target, "blocksize"));
                pkg->cache_model.i_log2_blocksize_mask[i] = ~((1 <<
                    clog2(get_json_int(target, "blocksize"))) - 1);
            }
        }
    }

    return ;
}

//Each simulator is responsible to synchronize the required configurations back to VPMU
static void dinero_init_json(Cache_Package *pkg, const char *str)
{
    char field_str[128];
    json_object *root;
    json_object *topology;
    root = json_tokener_parse(str);

    //Parse JSON config
    d4_levels = get_json_int(root, "levels");
    if (json_object_object_get_ex(root, "topology", &topology)) {
        d4_cache[d4_num_caches].cache = d4_mem_create();
        d4_cache[d4_num_caches].level = NOT_USED;
        d4_num_caches++;
        recursively_parse_json(topology, &d4_cache[0], d4_levels);
    }
    sync_back_config_to_vpmu(pkg, root);

    return ;
}

void dinero_main(void *ptr, int index, int num_cpu_cores, int num_gpu_cores)
{
    char process_name[128] = {0};

    DBG("VPMU worker process (cache-simulator-%d)  : start\n", index);
    sprintf(process_name, "cache-simulator-%d", index);
    prctl(PR_SET_NAME, process_name);
    num_cores[PROCESSOR_CPU] = num_cpu_cores;
    num_cores[PROCESSOR_GPU] = num_gpu_cores;
    for (int i = 1; i < MAX_D4_CACHES; i++) {
        core_num_table[i] =
            num_cores[i - 1] * 2 +  //*2 for icache and dcache
            core_num_table[i - 1];
    }

    dinero_init_json(&pkg[index], (const char *)ptr);
    //Reset the configurations depending on json contents.
    //Ex: some configuration might miss GPU topology while num_gpu_core are set
    for (int i = 0; i < ALL_PROC; i++) {
        if (flag_json_config_has_processor[i] == 0)
            num_cores[PROCESSOR_GPU] = 0;
    }
    if (d4setup() != EXIT_SUCCESS) {
        ERR_MSG("Something wrong with dinero cache\n");
        exit(1);
    }
    dinero_work(&pkg[index], index);
    //It should never return!!! Just exit
    exit(0);
}

//This is for registering the entry function of your own algorithm.
void dinero_register(void) {
    //This name maps to the one in JSON configuration.
    cache_sim_table[model_num].name = strdup("dinero");
    //This is the entry function of your own algorithm.
    cache_sim_table[model_num].entry = dinero_main;
    CONSOLE_LOG("\tRegister model: %s\n", cache_sim_table[model_num].name);
    model_num++;
}
