void one_bit_branch_predictor(Branch_Data *data, Branch_Reference *ref)
{
    int taken = ref->taken;

    switch (data->predictor) {   
        /* predict not taken */
        case 0: 
            if (taken) { 
                data->predictor = 1;
                data->wrong++;
            } else
                data->correct++;
            break;
            /* predict taken */
        case 1: 
            if (taken)
                data->correct++;
            else {
                data->predictor = 0;
                data->wrong++;
            }
            break;
        default:
            ERR_MSG("predictor error\n");
            exit(1);
    }
}

//This is for registering the entry function of your own algorithm.
void one_bit_register(void) {
    //This name maps to the one in JSON configuration.
    branch_sim_table[model_num].name = strdup("one_bit");
    //This is the entry function of your own algorithm.
    branch_sim_table[model_num].entry = one_bit_branch_predictor;
    CONSOLE_LOG("\tRegister model: %s\n", branch_sim_table[model_num].name);
    model_num++;
}

