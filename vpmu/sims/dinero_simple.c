#define LRU           ((char *)"LRU")
#define FIFO          ((char *)"FIFO")
#define RANDOM        ((char *)"random")
#define DEMAND_ONLY   ((char *)"demand only")
#define ALWAYS        ((char *)"always")
#define MISS          ((char *)"miss")
#define TAGGED        ((char *)"tagged")
#define LOAD_FORWARD  ((char *)"load forward")
#define SUB_BLOCK     ((char *)"sub block")
#define IMPOSSIBLE    ((char *)"impossible")
#define NEVER         ((char *)"never")
#define NO_FETCH      ((char *)"nofetch")

static d4cache *d4_dcache;
static d4cache *d4_icache;

static d4cache* d4_mem_create(void)
{
    d4cache *mem = d4new( NULL );

    if (mem == NULL)
        ERR_MSG("Main mem error \n" );
    mem->name = strdup("dinero Main Memory ");

    return mem ;
}

static void d4_cache_init(d4cache *c, int lev, int idu)
{
    c->name = (char *)malloc(30);
    if (c->name == NULL)
        CONSOLE_LOG("malloc failure initializing l%d%ccache\n", 
                    lev+1, idu==0?'u':(idu==1?'i':'d'));
    sprintf (c->name, "l%d-%ccache", lev+1, idu==0?'u':(idu==1?'i':'d'));

    c->flags |= GlobalVPMU.d4_level_doccc[idu][lev] ? D4F_CCC : 0;
    if (idu == 1)
        c->flags |= D4F_RO;
    c->lg2blocksize = clog2 (GlobalVPMU.d4_level_blocksize[idu][lev]);
    c->lg2subblocksize = clog2 (GlobalVPMU.d4_level_subblocksize[idu][lev]);
    c->lg2size = clog2 (GlobalVPMU.d4_level_size[idu][lev]);
    c->assoc = GlobalVPMU.d4_level_assoc[idu][lev];

    switch (GlobalVPMU.d4_level_replacement[idu][lev]) {
        default:  CONSOLE_LOG("replacement policy '%c' initialization botch\n", GlobalVPMU.d4_level_replacement[idu][lev]);
        case 'l': c->replacementf = d4rep_lru; c->name_replacement = LRU; break;
        case 'f': c->replacementf = d4rep_fifo; c->name_replacement = FIFO; break;
        case 'r': c->replacementf = d4rep_random; c->name_replacement = RANDOM; break;
    }

    switch (GlobalVPMU.d4_level_fetch[idu][lev]) {
        default:  CONSOLE_LOG("fetch policy '%c' initialization botch\n", GlobalVPMU.d4_level_fetch[idu][lev]);
        case 'd': c->prefetchf = d4prefetch_none; c->name_prefetch = DEMAND_ONLY; break;
        case 'a': c->prefetchf = d4prefetch_always; c->name_prefetch = ALWAYS; break;
        case 'm': c->prefetchf = d4prefetch_miss; c->name_prefetch = MISS; break;
        case 't': c->prefetchf = d4prefetch_tagged; c->name_prefetch = TAGGED; break;
        case 'l': c->prefetchf = d4prefetch_loadforw; c->name_prefetch = LOAD_FORWARD; break;
        case 's': c->prefetchf = d4prefetch_subblock; c->name_prefetch = SUB_BLOCK; break;
    }

    switch (GlobalVPMU.d4_level_walloc[idu][lev]) {
        default:  CONSOLE_LOG("write allocate policy '%c' initialization botch\n", GlobalVPMU.d4_level_walloc[idu][lev]);
        case 0:   c->wallocf = d4walloc_impossible; c->name_walloc = IMPOSSIBLE; break;
        case 'a': c->wallocf = d4walloc_always; c->name_walloc = ALWAYS; break;
        case 'n': c->wallocf = d4walloc_never; c->name_walloc = NEVER; break;
        case 'f': c->wallocf = d4walloc_nofetch; c->name_walloc = NO_FETCH; break;
    }

    switch (GlobalVPMU.d4_level_wback[idu][lev]) {
        default:  CONSOLE_LOG("write back policy '%c' initialization botch\n", GlobalVPMU.d4_level_wback[idu][lev]);
        case 0:   c->wbackf = d4wback_impossible; c->name_wback = IMPOSSIBLE; break;
        case 'a': c->wbackf = d4wback_always; c->name_wback = ALWAYS; break;
        case 'n': c->wbackf = d4wback_never; c->name_wback = NEVER; break;
        case 'f': c->wbackf = d4wback_nofetch; c->name_wback = NO_FETCH; break;
    }

    c->prefetch_distance = GlobalVPMU.d4_level_prefetch_distance[idu][lev] * GlobalVPMU.d4_level_subblocksize[idu][lev];
    c->prefetch_abortpercent = GlobalVPMU.d4_level_prefetch_abortpercent[idu][lev];
}


static void dinero_init(d4cache **dcache, d4cache **icache)
{
    int i = 0, lev = 0, idu = 0;
    d4cache *c = NULL,
            *ci = NULL,
            *cd = NULL;

    cd = ci = d4_mem_create();

    for (lev = GlobalVPMU.d4_maxlevel - 1;  lev >= 0;  lev--) {
        for (idu = 0;  idu < 3;  idu++) {
            if (GlobalVPMU.d4_level_size[idu][lev] != 0) {
                switch (idu) {
                    case 0: cd = ci = c = d4new(ci); break;    /* u */
                    case 1:      ci = c = d4new(ci); break;    /* i */
                    case 2:      cd = c = d4new(cd); break;    /* d */
                }
                if (c == NULL)
                    CONSOLE_LOG("cannot create level %d %ccache\n",
                                lev + 1, idu == 0 ? 'u':(idu == 1 ? 'i':'d'));
                d4_cache_init(c, lev, idu);
                GlobalVPMU.d4_levcache[idu][lev] = c;
            }
        }
    }

    i = d4setup();
    if (i != 0 ){
        CONSOLE_LOG("Cache Create Error %d\n" , i );
    }
    *dcache = cd;
    *icache = ci;
}

static void ResetCache(d4cache *d)
{
    d->fetch[D4XREAD] = 0;
    d->fetch[D4XWRITE] = 0;
    d->fetch[D4XINSTRN] = 0;
    d->miss[D4XREAD] = 0;
    d->miss[D4XWRITE] = 0;
    d->miss[D4XINSTRN] = 0;

    d->comp_miss[D4XREAD] = 0;
    d->cap_miss[D4XREAD]  = 0;
    d->conf_miss[D4XREAD] = 0;

    d->comp_miss[D4XINSTRN] = 0;
    d->cap_miss[D4XINSTRN] = 0;
    d->conf_miss[D4XINSTRN] = 0;

    d->comp_miss[D4XWRITE] = 0;
    d->cap_miss[D4XWRITE]  = 0;
    d->conf_miss[D4XWRITE] = 0;
}


//Handles types of packets
static void inline packet_processor(Cache_Reference *ref)
{
    int idu, lev;
    d4memref d4_ref;

    d4_ref.address = ref->addr;
    d4_ref.accesstype = (uint8_t)ref->type;
    d4_ref.size = ref->size;

    //DBG("ENQ: TYPE=%d ADDR=%x SIZE=%d\n", d4_ref.accesstype, d4_ref.address, d4_ref.size);
    //Every simulators should handle VPMU_BARRIER_PACKET to support synchronization
    //The implementation depends on your own packet type and writing style
    switch (ref->type) {
        case CACHE_PACKET_BARRIER:
            break;//Skip every barrier packet
        case CACHE_PACKET_DUMP_INFO:
            CONSOLE_LOG("Cache\n"); 
            break;
        case CACHE_PACKET_RESET:
            for (idu = 0; idu < 3; idu++)
                for (lev = 0; lev < 5; lev++)
                    if (GlobalVPMU.d4_levcache[idu][lev] != NULL)
                        ResetCache(GlobalVPMU.d4_levcache[idu][lev]);
            break;
        case CACHE_PACKET_READ:
        case CACHE_PACKET_WRITE:
            d4ref(d4_dcache, d4_ref);
            break;
        case CACHE_PACKET_INSTRN:
            d4ref(d4_icache, d4_ref);
            break;
        case CACHE_PACKET_MISC:
        case CACHE_PACKET_COPYB:
        case CACHE_PACKET_INVAL:
        default:
            ERR_MSG("Unexpected packet in cache simulators\n");
    }
}

static void *dinero_work(Cache_Package *pkg, int id)
{
    Cache_Reference ref;

    while(1) {
        VPMU_WAIT_JOB(&pkg->cache_job_semaphore, 
            //DBG("waked %x, index=%d-%d\n", pkg, cache_trace_buffer_t->start[id], cache_trace_buffer_t->end);
            //Keep draining jobs till it's empty
	    	while(likely(shm_isBufferNotEmpty(cache_trace_buffer_t, id))) {
                shm_bufferRead(cache_trace_buffer_t, id, ref);
                packet_processor(&ref);
            }
        );
    }
}

void dinero_main(void *ptr, int index)
{

    DBG("VPMU worker process (cache simulator %d)  : start\n", index);
    dinero_init(&d4_dcache, &d4_icache);
    dinero_work(&pkg[index], index);

    //It should never return!!! Just exit
    exit(0);
}

//This is for registering the entry function of your own algorithm.
void dinero_register(void) {
    //This name maps to the one in JSON configuration.
    cache_sim_table[model_num].name = strdup("dinero");
    //This is the entry function of your own algorithm.
    cache_sim_table[model_num].entry = dinero_main;
    DBG("\tRegister model: %s\n", cache_sim_table[model_num].name);
    model_num++;
}


