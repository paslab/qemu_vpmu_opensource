/*
 * SET_stack.c
 * Copyright (C) 2014 xvocker xvocker@gmail.com
 *
 * Distributed under terms of the LGPL license.
 */
#include "SET.h"
#include "SET_stack.h"

void stack_show(Stack *s)
{
	int i = 0;
	XV_DBG("stack:");
	for (i = s->size; i > s->size - 1000; i--) {
		fprintf(stderr, "0x%x, ", s->stack[i]);
	}
	fprintf(stderr, "\n");
}

int search(CallStackHashTable h, uint32_t key,  uint32_t value)
{
	int i = 0;
	/* Search all the slot in this hash table entry
	 * and return index of slot.*/
	while(i < h[key].size && i < MAX_SLOT_SIZE)
	{
		if (h[key].ret_entrys[i].tag == value)
			return i;
		i++;
	}
	return -1;
}

int insert_value(CallStackHashTable h, Stack *s, uint32_t value)
{
	int idx = 0;
	uint32_t key = HASH(value, MAX_ENTRY_SIZE);  
	ReturnEntry *entry;
	idx = search(h, key, value);

	/* prevent remove_value error */
	if (s->size >= MAX_STACK_SIZE)
	{
		stack_show(s);
		return -1;
	}

	/* (idx == -1) means we cannot find the value in hash table
	 * and will create an entry for this return value. Otherwise
	 * just increase the count of the return value entry. And tag
	 * the stack index into entry structure. */
//	XV_DBG("insert value 0x%x, stack index: %d\n", value, s->size);
	if (idx == -1) {
		//XV_DBG("create new slot key: %d, entry_size: %d\n", key, h[key].size);
		entry = &h[key].ret_entrys[h[key].size];

		if (h[key].size < MAX_SLOT_SIZE) {
			h[key].size += 1;
			entry->tag = value;
			entry->indexs[0] = s->size;
			entry->index_num = 1;
		}
		else {
			XV_DBG("SLOT FULL\n");
			return -1;
		}
		/* After insert return address entry has already done 
		 * push the return address into stack. */
		/*s->stack[stack_size] = value;
		s->size += 1;*/
	}
	else {
		//XV_DBG("Slot found key: %d, entry_size: %d\n", key, h[key].size);

		entry = &h[key].ret_entrys[idx];

		if (entry->index_num < MAX_INDEX_SIZE ) {
			entry->indexs[entry->index_num] = s->size;
			entry->index_num += 1;
		}
		else {
			XV_DBG("INDEX FULL\n");
			return -1;
		}
	}
	SET_callstack_push(s->stack, s->size, value);
//	stack_show(s);

	return s->size;
}

inline void _remove_value(CallStackHashTable h, uint32_t value)
{
	uint32_t key = HASH(value, MAX_ENTRY_SIZE);
	ReturnEntry *entry;

	int idx = search(h, key, value);
	int i;

	if (idx != -1) {
		//XV_DBG("remove slot found key: %d, entry_size: %d\n", key, h[key].size);
		entry = &h[key].ret_entrys[idx];
		/* Decrease entry slot count if count will decrease to 0 
		 * then remove this slot.*/
		if (entry->index_num == 1) {
			//XV_DBG("remove slot %d,%d\n", idx, h[key].size - 1, idx - 1);
			for (i = idx; i < h[key].size - 1; i++) {
				h[key].ret_entrys[i].tag = h[key].ret_entrys[i + 1].tag;
				h[key].ret_entrys[i].index_num = h[key].ret_entrys[i + 1].index_num;
				SWAP(h[key].ret_entrys[i].indexs, h[key].ret_entrys[i + 1].indexs);
			}
			h[key].size -= 1;
		}
		else {
		//	XV_DBG("decrease index_num: %d\n", entry->index_num);
			entry->index_num -= 1;
		}
	}
	else {
		XV_DBG("stack remove error\n");
	}
}

int remove_value(CallStackHashTable h, Stack *s, uint32_t value)
{
	int idx = 0;
	int ret = 0;
	int i;
	int stack_size = s->size;
	int stack_index = 0;
	uint32_t key = HASH(value, MAX_ENTRY_SIZE); 
	ReturnEntry *entry;

	idx = search(h, key, value);

	/* Cannot find return address in pc + 4 list.
	 * Check pc + 2 list
	 */
	if (idx == -1) {
		key = HASH(value - 2, MAX_SLOT_SIZE);
		idx = search(h, key, value);
	}

	if (idx != -1) {
		entry = &h[key].ret_entrys[idx];
		//XV_DBG("remove value 0x%x, index: %d, stack index: %d, stack_size: %d\n", value, entry->index_num, entry->indexs[entry->index_num-1], s->size);
//		XV_DBG("remove value 0x%x, stack index: %d\n", value, entry->indexs[entry->index_num-1]);
		/* Recursive pop stack */
		/* This method will lead error because recursive function call */
		/*if (entry->indexs[entry->index_num-1] < s->size - 1){
			ret = remove_value(h, s, s->stack[entry->indexs[entry->index_num-1] + 1]);

			if (ret == -1)
				return -1;
		}*/
		stack_index = entry->indexs[entry->index_num - 1];
		/* Iterative pop stack */
		for (i = s->size - 1; i >= stack_index; i--) {
			_remove_value(h, s->stack[i]);
		}

		/* Adjust stack_size after remove_value finished. */
		s->size = stack_index;
	}
	else {
		//XV_DBG("cannot found in hash table\n");
		return 0;
	}
	/* FIXED ME: The return value has to be number of pop */
	return (stack_size - stack_index);
}

CallStackHashTable hash_init(uint32_t entry_num, uint32_t set_num, uint32_t index_num)
{
	CallStackHashTable h = (CallStackHashTable)malloc(sizeof(CallStackHashEntry) * entry_num);
	int i, j;
	for (i = entry_num - 1; i >= 0; i--){
		h[i].size = 0;
		h[i].ret_entrys = (ReturnEntry *)malloc(sizeof(ReturnEntry) * set_num);
		for (j = set_num - 1; j >= 0; j--){
			h[i].ret_entrys[j].indexs = (uint32_t *)malloc(sizeof(uint32_t) * index_num);
			h[i].ret_entrys[j].tag = 0;
			h[i].ret_entrys[j].index_num = 0;
		}
	}
	return h;
}

void hash_reset(CallStackHashTable h, uint32_t entry_num)
{
//	XV_DBG("reset hash\n");
	int i = 0;
	for (i = entry_num - 1; i >= 0; i--) {
		h[i].size = 0;
	}
}

void hash_free(CallStackHashTable h, uint32_t entry_num, uint32_t set_num, uint32_t index_num)
{
	int i, j;
	for (i = entry_num - 1; i >= 0; i--) {
		for (j = set_num -1; j >= 0; j--) {
			free(h[i].ret_entrys[j].indexs);
		}
		free(h[i].ret_entrys);
	}
	free(h);
}
