#ifndef SET_CTRL_SAMPLING_H
#define SET_CTRL_SAMPLING_H

#include <pthread.h>

unsigned int getSampleInterval (void);
int getSampleEventID (void);
int isSampling (void);
char *getEventConfigArray (void);
int isEventMonitored (int index);

/* Get timing model used by SET. */
int getSETTimingModel(void);

#endif /* SET_CTRL_SAMPLING.H */
