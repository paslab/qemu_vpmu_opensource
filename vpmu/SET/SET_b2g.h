/*
 * SET_b2g.h
 * Copyright (C) 2014 xvocker <xvocker@gmail.com>
 *
 * Distributed under terms of the LGPL license.
 */

#ifndef SET_B2G_H
#define SET_B2G_H

#include "SET_kernel.h"

#define IS_HIDDEN_FUNC(efd, i) ((efd_get_sym_type(efd, i) == STT_FUNC) \
						&& (efd_get_sym_vis(efd, i) == STV_HIDDEN))

#define MAX_LINENO_SIZE	 20480

enum B2GEvent {
	PROLOGUE,
	EPILOGUE,
	RUNSCRIPT,
	INTERPRET
};

typedef struct B2GFunc {
    uint32_t    addr;
    enum B2GEvent event;
    FuncRange   *retFunc;
}B2GFunc;

typedef struct SETProcess SETProcess;

typedef struct JSFile {
	char		name[256];
	char		funcname[256];
	int			LineNo[MAX_LINENO_SIZE];
	int			index[MAX_LINENO_SIZE];
	int			count[MAX_LINENO_SIZE];
	int			LineNoSize;
} JSFile;

//#define B2G_EVENT_NUM 4
#define B2G_EVENT_NUM 3
#define B2G_TABLE_SIZE 11

int get_special_b2g_func(char *path, B2GFunc *funcTable, int size, uint32_t start, uint32_t end, uint32_t pgoff);
void SET_event_b2g(SETProcess *tracing_process, uint32_t target_addr, uint32_t return_addr);
void SET_event_js_entry(SETProcess *tracing_process, char *current_name, char *func_name, uint32_t line_no);
void SET_event_js_exit(SETProcess *tracing_process, char *current_name, char *func_name,uint32_t line_no);
void SET_event_js_agent_entry(char *);
void SET_event_js_agent_exit(char *);
void show_annotate_event_count(void);

#endif /* !SET_B2G_H */
