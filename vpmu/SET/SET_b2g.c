/*
 * SET_b2g.c
 * Copyright (C) 2014 xvocker xvocker@gmail.com
 *
 * Distributed under terms of the LGPL license.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "SET_object.h"
#include "SET_b2g.h"
#include "SET.h"
#include "SET_out.h"

#include "cpu.h"

#define JS_RETURN 0x80000000

typedef struct SET_struct {
	char filename[256];
	uint32_t lineno;
	char funcname[256];
}SET_struct;


static FuncRange prologue, epilogue;
static FuncRange runscript, interpret;

static uint32_t full_annotate_count;
static uint32_t real_annotate_count;
int get_special_b2g_func(char *path, B2GFunc *funcTable, int size, uint32_t start, uint32_t end, uint32_t pgoff)
{   
	/* Using EFD to obtain the information about object file */                                                             
	EFD *efd = efd_open_elf(path);
	if (efd == NULL)
		return -1;

	/* Push special functions into hash table */
	uint32_t i;
	char funcName[1024];
	uint32_t vaddr, vaddr_off = (efd->elf_hdr.e_type == ET_DYN) ? (start - (pgoff << TARGET_PAGE_BITS)) : 0;
	int idx, sym_num;
	enum B2GEvent event;
	sym_num = efd_get_sym_num(efd);

	idx = 0;
	for (i = 0; i < sym_num; i++) {
		if (IS_HIDDEN_FUNC(efd, i) || IS_FUNC(efd, i)) {

			vaddr = (efd_get_sym_value(efd, i) & THUMB_INSN_MASK) + vaddr_off;
			strncpy(funcName, efd_get_sym_name(efd, i), 1024);
			switch(funcName[8]) {
				case 'I':
					if (!strcmp(funcName, "_ZN2js16InterpreterFrame8prologueEP9JSContext")) {
						prologue.start = vaddr;
						prologue.end = vaddr + efd_get_sym_size(efd, i);
						event = PROLOGUE;
						XV_DBG("prologue 0x%x , 0x%x\n", prologue.start, prologue.end);
						break;
					}
					else if (!strcmp(funcName, "_ZN2js16InterpreterFrame8epilogueEP9JSContext")) {
						epilogue.start = vaddr;
						epilogue.end = vaddr + efd_get_sym_size(efd, i);
						event = EPILOGUE;
						XV_DBG("epilogue 0x%x , 0x%x\n", epilogue.start, epilogue.end);
						break;
					}
					else {
						continue;
					}
				case 'u':
					if (!strcmp(funcName, "_ZN2js9RunScriptEP9JSContextRNS_8RunStateE")) {
						runscript.start = vaddr;
						runscript.end = vaddr + efd_get_sym_size(efd, i);
						event = RUNSCRIPT;
						XV_DBG("runscript 0x%x , 0x%x\n", runscript.start, runscript.end);
						break;
					}
					else {
						continue;
					}
#if 0
				case 'r':
					if (!strcmp(funcName, "_ZL9InterpretP9JSContextRN2js8RunStateE")) {
						interpret.start = vaddr;
						interpret.end = vaddr + efd_get_sym_size(efd, i);
						event = INTERPRET;
						//XV_DBG("interpret 0x%x , 0x%x\n", interpret.start, interpret.end);
						break;
					}
					else {
						continue;
					}
#endif
				default:
					continue;
			}

			/* Linear probing for searching a free location */
			funcTable[idx].addr = vaddr;
			funcTable[idx].event = event;
			idx++;
		}
	}

	int num = 0;
	for (i = 0; i < B2G_TABLE_SIZE; i++) {

		if (funcTable[i].addr != 0) {

			switch (funcTable[i].event) {
				case PROLOGUE:
					if (prologue.start == 0 || prologue.end == 0 || prologue.end - prologue.start == 0) {
						return -1;
					}
					funcTable[i].retFunc = &prologue;
					num++;
					break;
				case EPILOGUE:
					if (epilogue.start == 0 || epilogue.end == 0 || epilogue.end - epilogue.start == 0) {
						return -1;
					}
					funcTable[i].retFunc = &epilogue;
					num++;
					break;
				case RUNSCRIPT:
					if (runscript.start == 0 || runscript.end == 0 || runscript.end - runscript.start == 0) {
						return -1;
					}
					funcTable[i].retFunc = &runscript;
					num++;
					break;
#if 0
				case INTERPRET:
					if (interpret.start == 0 || interpret.end == 0 || interpret.end - interpret.start == 0) {
						return -1;
					}
					funcTable[i].retFunc = &interpret;
					num++;
#endif
				default:
					break;
			}
		}
	}

	if (num != B2G_EVENT_NUM) {
		fprintf(stderr, "SET device: b2g function check error %d\n", num);
		return -1;
	}
	else {
		return 0;
	}
}


void SET_event_b2g(SETProcess *tracing_process, uint32_t target_addr, uint32_t return_addr)
{
	B2GFunc *b2gFuncTable = set.b2gFuncTable;
	int idx;
	int i, j;
	for (idx = 0; idx < B2G_TABLE_SIZE; idx++) {

		char name[256] = {0};
		char current_name[256] = {0};
		char funcname[256] = {0};
		char ch;
		int line_no;
		int type;

		if ((b2gFuncTable[idx].retFunc != NULL) && (return_addr > b2gFuncTable[idx].retFunc->start) && (return_addr <= b2gFuncTable[idx].retFunc->end)) {
			switch (b2gFuncTable[idx].event) {
				case PROLOGUE:
					//XV_DBG("jump in prologue\n");
					copy_string_from_gvm(cpu_single_env->regs[6], 0, (void *)name);
					line_no = (uint32_t) __ldl_mmu(cpu_single_env->regs[6] + 256, 0);
					copy_string_from_gvm(cpu_single_env->regs[6] + 260, 0, (void *)funcname);
					//XV_DBG("<- js_name: %s, line_no: %d, func: %s\n", name, line_no, funcname);
					SET_event_js_entry(tracing_process, name, funcname, line_no);
					break;
				case EPILOGUE:
					//XV_DBG("jump in epilogue\n");
					copy_string_from_gvm(cpu_single_env->regs[6], 0, (void *)name);
					line_no = (uint32_t) __ldl_mmu(cpu_single_env->regs[6] + 256, 0);
					copy_string_from_gvm(cpu_single_env->regs[6] + 260, 0, (void *)funcname);
					//XV_DBG("-> js_name: %s, line_no: %d, func: %s\n", name, line_no, funcname);
					SET_event_js_exit(tracing_process, name, funcname, line_no);
					break;
				case RUNSCRIPT:
					copy_string_from_gvm(cpu_single_env->regs[6], 0, (void *)name);
					line_no = (uint32_t) __ldl_mmu(cpu_single_env->regs[6] + 256, 0);
					copy_string_from_gvm(cpu_single_env->regs[6] + 260, 0, (void *)funcname);
					//XV_DBG("RUN_SCRIPT js_name: %s, line_no: %d, func: %s\n", name, line_no, funcname);
					break;
				case INTERPRET:
					copy_string_from_gvm(cpu_single_env->regs[6], 0, (void *)name);
					line_no = (uint32_t) __ldl_mmu(cpu_single_env->regs[6] + 256, 0);
					copy_string_from_gvm(cpu_single_env->regs[6] + 260, 0, (void *)funcname);
					//XV_DBG("INTERPRET js_name: %s, line_no: %d, func: %s\n", name, line_no, funcname);
					break;
				default:
					/* if this case show means you forget something */
					break;
			}
			real_annotate_count++;
			break;
		}
	}
	full_annotate_count++;
}

void SET_event_js_entry(SETProcess *tracing_process, char *current_name, char *func_name, uint32_t line_no)
{
	if (!(tracing_process->type & SET_PROCESS_JS))
		return;

	int i;
	JSFile *JSEntry;
	for (i = tracing_process->JSTableSize - 1; i >= 0; i--) {
		JSEntry = &tracing_process->JSTable[i];
		if (!strcmp( current_name, JSEntry->name)) {
			JSEntry->LineNo[JSEntry->LineNoSize] = line_no;
			JSEntry->index[JSEntry->LineNoSize] = tracing_process->js->size;
			JSEntry->count[JSEntry->LineNoSize]++;
			if (JSEntry->LineNoSize < MAX_LINENO_SIZE) {
				JSEntry->LineNoSize++;
			}
			else
				fprintf(stderr, "SET device: LineNo Overflow\n");

			break;
		}
	}

	if (i < 0) {
		JSEntry = &tracing_process->JSTable[tracing_process->JSTableSize];
		strcpy(JSEntry->name, current_name);
		JSEntry->LineNo[0] = line_no;
		JSEntry->index[0] = tracing_process->js->size;
		memset(JSEntry->count, 0, MAX_LINENO_SIZE);
		JSEntry->count[0]++;
		JSEntry->LineNoSize++;
		if (tracing_process->JSTableSize < MAX_OBJ_TABLE_SIZE)
			tracing_process->JSTableSize++;
		else
			fprintf(stderr, "SET device: JSTable Overflow\n");
	}

	SET_callstack_push(tracing_process->js->stack, tracing_process->js->size, line_no);
	SET_output_write_pid(tracing_process->output, tracing_process->pid);
	SET_output_write_js(tracing_process->output, line_no, current_name, func_name);
	//XV_DBG("entry: %s, no: %lu\n", current_name, line_no);
}

void SET_event_js_exit(SETProcess *tracing_process, char *current_name, char *func_name, uint32_t line_no)
{
	if (!(tracing_process->type & SET_PROCESS_JS))
		return;

	int i,j;
	JSFile *JSEntry;
	for (i = tracing_process->JSTableSize - 1; i >= 0; i--) {
		JSEntry = &tracing_process->JSTable[i];
		if (!strcmp( current_name, JSEntry->name)) {
			if (line_no > 100000 || line_no <= 0) {
				j = JSEntry->LineNoSize - 1;
				JSEntry->LineNo[j] = line_no;
				tracing_process->js->size = JSEntry->index[j];
				JSEntry->count[j]--;
				if (JSEntry->count[j] == 0)
					JSEntry->LineNoSize--;
				if (JSEntry->LineNoSize == 0)
					tracing_process->JSTableSize--;
				SET_output_write_pid(tracing_process->output, tracing_process->pid);
				SET_output_write_js(tracing_process->output, JSEntry->index[j] | JS_RETURN, current_name, func_name);
			}
			for (j = JSEntry->LineNoSize - 1; j >= 0; j--) {
				if (JSEntry->LineNo[j] == line_no) {
					tracing_process->js->size = JSEntry->index[j];
					JSEntry->count[j]--;
					if (JSEntry->count[j] == 0)
						JSEntry->LineNoSize--;
					if (JSEntry->LineNoSize == 0)
						tracing_process->JSTableSize--;
					SET_output_write_pid(tracing_process->output, tracing_process->pid);
					SET_output_write_js(tracing_process->output, JSEntry->index[j] | JS_RETURN, current_name, func_name);
					return;
				}
			}
		}
	}
}

void SET_event_js_agent_entry(char *name)
{
	SETProcess *tracing_process = set.tracing_process;
	SET_event_js_entry(tracing_process, name, "NULL",  0);
}
void SET_event_js_agent_exit(char *name)
{
	SETProcess *tracing_process = set.tracing_process;
	SET_event_js_exit(tracing_process, name, "NULL", 0);
}

void show_annotate_event_count()
{
	XV_DBG("full: %lu, real: %lu \n", full_annotate_count, real_annotate_count);
	full_annotate_count = 0;
	real_annotate_count = 0;
}

