#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> /* access() */
#include "SET.h"
/* FIXME ifdef CONFIG_VPMU ? */
#include "vpmu.h"

//tianman
#include "SET_event.h"

SET set;
char path_android_out[FULL_PATH_LEN];
char path_set_out[FULL_PATH_LEN];
//evo0209
int trace_kernel = 0;

void SET_init()
{
	//tianman
	int ii;
	for (ii = 0; ii < 512; ii++)
		pid_base[ii][0] = -1;

    if (set.is_start)
    {
        fprintf(stderr, "SET device: Already started\n");
        return;
    }

    /* Specify the directory where object files (executable or library) located */
    char *out = getenv("ANDROID_PRODUCT_OUT");
	DBG("ANDROID_PRODUCT_OUT:%s\n", out);
    if (out == NULL || out[0] == 0 || access(out, F_OK))
    {
        fprintf(stderr, "SET device: init failed - invalid environment variable \"ANDROID_PRODUCT_OUT\"\n");
        return;
    }
    else
        strncpy(path_android_out, out, sizeof(path_android_out));

    out = getenv("SET_OUT");
	DBG("SET_OUT:%s\n", out);
    if (out == NULL || out[0] == 0 || access(out, F_OK))
    {
        fprintf(stderr, "SET device: init failed - invalid environment variable \"SET_OUT\"\n");
        return;
    }
    else
        strncpy(path_set_out, out, sizeof(path_set_out));

    GlobalVPMU.switch_to_addr = 0;
    GlobalVPMU.execve_addr = 0;
    memset(&set, 0, sizeof(SET));
    /* Get special kernel functions (used for dynamic instrumention of Linux kernel) */
    char kernel_path[FULL_PATH_LEN];
    snprintf(kernel_path, sizeof(kernel_path), "%s/vmlinux", path_set_out);
    if (get_special_kernel_func(kernel_path, set.kerFuncTable, KER_TABLE_SIZE) != 0)
    {
        fprintf(stderr, "SET device: init failed - unable to dynamic instrument kernel functions\n");
        return;
    }
    else {
        /* Using EFD to obtain the information about object file */
        EFD *efd = efd_open_elf(kernel_path);
        if (efd == NULL)
            return ;

        /* Push special functions into hash table */
        for (int i = 0; i < efd_get_sym_num(efd); i++) {
            if (IS_FUNC(efd, i)) {
                uint32_t vaddr = efd_get_sym_value(efd, i) & THUMB_INSN_MASK;
                char *funcName = efd_get_sym_name(efd, i);
                if (strcmp(funcName, "do_execve") == 0)
                    GlobalVPMU.execve_addr = vaddr;
                if (strcmp(funcName, "__switch_to") == 0)
                    GlobalVPMU.switch_to_addr = vaddr;
                if (strcmp(funcName, "do_exit") == 0)
                    GlobalVPMU.exit_addr = vaddr;
            }
        }
    }

    /* Succeed */
    set.is_start = 1;
    atexit(SET_exit);
    fprintf(stderr, "SET device: start\n");

    /*=== Christine ADD ===*/
	fprintf(stdout,"create a monitor thread. \n");
	int time_interval = 0;
	pthread_create(&set.monitor_thread,NULL,SET_monitor_agent,(void*)&time_interval);

	//Also need to start VPMU monitor thread
	//VPMU_wake_sleep();

    /* FIXME */
    SETProcess *process = SET_create_process("plugin-container", SET_PROCESS_MMAP);

	//tianman : for trace kernel
	if (trace_kernel)
	{
		DBG("enable trace_kernel\n");
		set.model = 2;
    	//SET_event_bootup();
	}
	
    if (process != NULL)
        SET_attach_process(process);
}

void SET_exit()
{
    if (!set.is_start)
        return;

    set.is_start = 0;

    int i;
    for (i = 0; i < set.procTableSize; i++)
        SET_delete_process(set.procTable[i]);

    set.procTableSize = 0;

    SET_stop_vpmu();

    fprintf(stderr, "SET device: exit\n");
	
	/*Christine ADD : Make sure the thread terminates.*/
	pthread_join(set.monitor_thread,NULL);

}

/*=== Christine ADD ===*/
//It is responsible to the SIGUSR2 handling.
void TickSignalHandler(int signal,siginfo_t* info,void* context)
{
	DBG("In the signal handler.\n");
	SETProcess *process = set.tracing_process;

	if(set.tracing_process != NULL)
	{
		if ((*(process->tracing) > 0) && (process->output != NULL))
		{
			int kernel_event = process->kernel->size;
			DBG("kernel stack size= %d \n",kernel_event);
			int user_event   = process->user->size;
			DBG("user stack size= %d \n",user_event);
			int irq_event    = process->irq->size;
			DBG("irq stack size= %d \n",irq_event);
		}
	}

}

/*=== Christine ADD ===*/
//A monitor thread for sampling
void *SET_monitor_agent(void* interval)
{
	int* time_interval = (int*)interval;
	fprintf(stdout,"time interval is %d .\n",*time_interval);
	struct sigaction tick;
	tick.sa_sigaction = TickSignalHandler;	
	sigemptyset(&tick.sa_mask);
	tick.sa_flags = 0;
	if(sigaction(SIGUSR2,&tick,NULL)==-1)
	{
		fprintf(stderr,"signal error orrurs !!\n");
		return NULL;
	}
}

int SET_has_process_being_tracing()
{
    SETProcess *process;
    int i;
    for (i = 0; i < set.procTableSize; i++)
    {
        process = set.procTable[i];
		if ((*(process->tracing) > 0) && (process->output != NULL)){	
			return 1;
		}
    }
    return 0;
}

void SET_start_vpmu()
{
    /* If there are processes being tracing and VPMU already enabled, don't change VPMU model */
#ifdef CONFIG_VPMU
    if (VPMU_enabled)
        return;
#endif

    if (set.model == 0)
        return;
#ifdef CONFIG_VPMU
    VPMU_reset();
	DBG("VPMU device reset succeeded\n");
    switch(set.model)
    {
        case 1:
            //vpmu_model_setup(&GlobalVPMU, TIMING_MODEL_B);
			//evo0209
            vpmu_model_setup(&GlobalVPMU, TIMING_MODEL_F);
            VPMU_enabled = 1;
            fprintf(stderr, "SET device: Pipeline simulation\n");
            break;
        case 2:
            //vpmu_model_setup(&GlobalVPMU, TIMING_MODEL_D);
			//evo0209
            vpmu_model_setup(&GlobalVPMU, TIMING_MODEL_G);
            VPMU_enabled = 1;
            fprintf(stderr, "SET device: Pipeline and I/D cache simulation\n");
            break;
        case 3:
            vpmu_model_setup(&GlobalVPMU, TIMING_MODEL_E);
            VPMU_enabled = 1;
            fprintf(stderr, "SET device: Pipeline, I/D cache and branch simulation\n");
            break;
        default:
            VPMU_enabled = 0;
            set.model = 0;
            fprintf(stderr, "SET device: Wrong timing model!\n");
            return;
    }
	DBG("VPMU device start succeeded\n");
#endif
}

void SET_stop_vpmu()
{
#ifdef CONFIG_VPMU
    if (VPMU_enabled)
    {
        VPMU_enabled = 0;
		DBG("VPMU OUT\n");
		VPMU_dump_result();
        fprintf(stderr, "SET device: Disable VPMU\n");
    }
#endif
}

/* Check whether the C binary of the process exists in host OS or not */
static int SET_set_C_binary(SETProcess *process)
{
    /* Absolute path of this program */
    snprintf(process->path_c, sizeof(process->path_c), "%s/%s", path_set_out, process->name);
    if (access(process->path_c, F_OK))
    {
        snprintf(process->path_c, sizeof(process->path_c), "%s/symbols/system/bin/%s", path_android_out, process->name);
        if (access(process->path_c, F_OK))
        {
            fprintf(stderr, "SET device: Cannot find C program \"%s\"\n", process->name);
            return -1;
        }
    }
    return 0;
}

/* Check whether the kernel binary exists in host OS or not */
static int SET_set_kernel_binary(SETProcess *process)
{
    snprintf(process->path_kernel, sizeof(process->path_kernel), "%s/vmlinux", path_set_out);
    if (access(process->path_kernel, F_OK))
    {
        fprintf(stderr, "SET device: Cannot find \"vmlinux\"\n");
        return -1;
    }
    return 0;
}

SETProcess *SET_create_process(char *name, ProcessType type)
{
    SETProcess *process;
    char output_path[FULL_PATH_LEN];

    if (name == NULL)
    {
        fprintf(stderr, "SET device: Invalid name!\n");
        return NULL;
    }

    /* Create process */
    process = (SETProcess *) malloc(sizeof(SETProcess));
    if (process == NULL)
    {
        fprintf(stderr, "SET device: No memory space to allocate SETProcess!\n");
        return NULL;
    }
    memset(process, 0, sizeof(SETProcess));

    process->pid = -1;
    process->idx = -1;

    /* Create tracing count */
    process->tracing = (int *) malloc(sizeof(int));
    *process->tracing = 0;

    if (SET_set_process_type(process, type) != 0)
    {
		DBG("SET TYPE ERROR\n");
        SET_delete_process(process);
        return NULL;
    }

    if (SET_set_process_name(process, name) != 0)
    {
		DBG("SET NAME ERROR\n");
        SET_delete_process(process);
        return NULL;
    }

    /* Create output stream */
    if (process->type & SET_PROCESS_OUTPUT)
    {
        snprintf(output_path, sizeof(output_path), "%s/%s.set", path_set_out, process->name);
        process->output = SET_output_create(output_path);
        if (process->output == NULL)
        {
			DBG("OUTPUT ERROR\n");
            SET_delete_process(process);
            return NULL;
        }
    }

    return process;
}

SETProcess *SET_copy_process(SETProcess *process_src)
{
    SETProcess *process_dest;

    if (process_src == NULL)
        return NULL;

    /* Create process */
    process_dest = (SETProcess *) malloc(sizeof(SETProcess));
    if (process_dest == NULL)
    {
        fprintf(stderr, "SET device: No memory space to allocate SET Process!\n");
        return NULL;
    }

    memcpy(process_dest, process_src, sizeof(SETProcess));

    (*process_dest->tracing)++;

    /* Private data */
    if (process_src->kernel)
    {
        process_dest->kernel = (Stack *) calloc(1, sizeof(Stack));
        if (process_dest->kernel == NULL)
            goto fail;

		process_dest->kernel_table = hash_init(MAX_ENTRY_SIZE, MAX_SLOT_SIZE, MAX_INDEX_SIZE);
    }
    if (process_src->irq)
    {
        process_dest->irq = (Stack *) calloc(1, sizeof(Stack));
        if (process_dest->irq == NULL)
            goto fail;
    }
    if (process_src->ic)
    {
        process_dest->ic = (Stack *) calloc(1, sizeof(Stack));
        if (process_dest->ic == NULL)
            goto fail;
    }
    if (process_src->user)
    {
        process_dest->user = (Stack *) calloc(1, sizeof(Stack));
        if (process_dest->user == NULL)
            goto fail;
		
		process_dest->user_table = hash_init(MAX_ENTRY_SIZE, MAX_SLOT_SIZE, MAX_INDEX_SIZE);
    }

	if (process_src->js)
	{
        process_dest->js = (Stack *) calloc(1, sizeof(Stack));
        if (process_dest->js == NULL)
            goto fail;

		process_dest->JSTable = (JSFile *)malloc(sizeof(JSFile) * MAX_ENTRY_SIZE);
		if (process_dest->JSTable == NULL)
			goto fail;
	}


    return process_dest;

fail:
    fprintf(stderr, "SET device: No memory space to allocate Stack!\n");
    SET_delete_process(process_dest);
    return NULL;
}

void SET_delete_process(SETProcess *process)
{
    if (process == NULL)
        return;

    if (*process->tracing > 0){
        //fprintf(stderr, "SET device: End tracing process \"%s\", pid: %d\n", process->name, process->pid);
        fprintf(stderr, "SET device: End tracing pid: %d, parent: %s, name: %s\n", process->pid, process->name, pid_name_base[process->pid]);
	}

    (*process->tracing)--;

    /* Free private data */
    if (process->kernel)
        free(process->kernel);
    if (process->irq)
        free(process->irq);
    if (process->ic)
        free(process->ic);
    if (process->user)
        free(process->user);
	if (process->js)
		free(process->js);

	if (process->kernel_table)
		hash_free(process->kernel_table, MAX_ENTRY_SIZE, MAX_SLOT_SIZE, MAX_INDEX_SIZE);
	if (process->user_table)
		hash_free(process->user_table, MAX_ENTRY_SIZE, MAX_SLOT_SIZE, MAX_INDEX_SIZE);

    /* Check if other shared processes (threads) still running. If no, free "shared" resources */
    /* 0 means no other threads, -1 means deleting before start tracing */
    if (*process->tracing < 1)
    {
        /* Kernel object */
        if (process->objKernel)
        {
            process->objKernel->count--;
            SET_delete_obj(process->objKernel);
        }

        /* User space object */
        int i;
        for (i = 0; i < process->objTableSize; i++)
        {
            process->objTable[i]->count--;
            SET_delete_obj(process->objTable[i]);
        }

        /* Dex */
        for (i = 0; i < process->dexTableSize; i++)
        {
            process->dexTable[i]->count--;
//            SET_delete_dex(process->dexTable[i]);
        }

        /* Output */
//        if (process->output)
//            SET_output_close(process->output);

        /* Reference count */
        free(process->tracing);
    }
    free(process);
	process = NULL;
}

/* Add tracing process to the list */
void SET_attach_process(SETProcess *process)
{
    if (set.procTableSize == MAX_PROC_TABLE_SIZE)
        return;

    set.procTable[set.procTableSize] = process;
    process->idx = set.procTableSize;
    set.procTableSize++;
}

void SET_detach_process(SETProcess *process)
{
    if (set.procTableSize <= 0)
        return;

    int i = process->idx;
    if (i < 0)
        return;

    /* To remove an entry in 'i' place, shift entries on its right to the left by one */
    for (; i < set.procTableSize - 1; i++){
        set.procTable[i] = set.procTable[i+1];
		set.procTable[i]->idx = i;
	}

    process->idx = -1;
    set.procTableSize--;
}

int SET_set_process_type(SETProcess *process, ProcessType type)
{
    /* Impossible process type flags */

    ProcessType changedType = type & (type ^ process->type);
    /* Check C binary if process name has been set and type flag "C" will be changed and set */
    if ((changedType & SET_PROCESS_USER_C) && (process->name[0] != '\0'))
        if ((SET_set_C_binary(process) != 0))
            return -1;

    if (changedType & (SET_PROCESS_KERNEL | SET_PROCESS_IRQ))
        if ((SET_set_kernel_binary(process) != 0))
            return -1;

    process->type = type;

    return 0;
}

int SET_set_process_name(SETProcess *process, char *name)
{
    int i, ch;

    /* Copy the process name from after last slash */
    for (i = 0; (ch = *(name++)) != '\0';)
    {
        if (ch == '/')
            i = 0;  /* overwrite what we wrote */
        else if (i < (sizeof(process->name) - 1))
            process->name[i++] = ch;
    }
    process->name[i] = '\0';

    /* Avoid duplicated process names */
    for (i = 0; i < set.procTableSize; i++)
    {
        if (strcmp(process->name, set.procTable[i]->name) == 0)
        {
            fprintf(stderr, "SET device: Duplicated process name %s!\n", process->name);
            return -1;
        }
    }

    if (process->type & SET_PROCESS_USER_C)
        if (SET_set_C_binary(process) != 0)
            return -1;

    return 0;
}

void SET_attach_obj(SETProcess *process, ObjFile *obj)
{
    if (process->objTableSize == MAX_OBJ_TABLE_SIZE)
	{
		DBG("OBJ TABLE overflow\n");
        return;
	}

    if (process->output)
        SET_output_write_obj(process->output, obj->id, obj->path);

    obj->count++;

    /* Linear search */
    int i, j;
    for (i = 0; i < process->objTableSize; i++)
    {
        if (obj->text_start < process->objTable[i]->text_start)
        {
            /* To insert an entry in 'i' place, shift entries on its right to the right by one */
            for (j = process->objTableSize; j > i; j--)
                process->objTable[j] = process->objTable[j-1];

            break;
        }
    }
    process->objTable[i] = obj;
    process->objTableSize++;
}

/* FIXME munmap() */
void SET_detach_obj(ObjFile *obj)
{
    obj->count--;
}

void SET_attach_dex(SETProcess *process, DexFile *dex)
{
    if (process->dexTableSize == MAX_DEX_TABLE_SIZE)
        return;

    if (process->output)
        SET_output_write_dex(process->output, dex->id, dex->path);

    dex->count++;

    /* Linear search */
    int i, j;
    for (i = 0; i < process->dexTableSize; i++)
    {
        if (dex->start < process->dexTable[i]->start)
        {
            /* To insert an entry in 'i' place, shift entries on its right to the right by one */
            for (j = process->dexTableSize; j > i; j--)
                process->dexTable[j] = process->dexTable[j-1];

            break;
        }
    }
    process->dexTable[i] = dex;
    process->dexTableSize++;
}

/* FIXME munmap() */
void SET_detach_dex(DexFile *dex)
{
    dex->count--;
}

//evo0209
void SET_trace_kernel()
{
    trace_kernel = 1;
}
