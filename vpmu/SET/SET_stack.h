/*
 * SET_stack.h
 * Copyright (C) 2014 xvocker <xvocker@gmail.com>
 *
 * Distributed under terms of the LGPL license.
 */

#ifndef SET_STACK_H
#define SET_STACK_H

#include <stdio.h>
#include "SET_object.h"

#define MAX_INDEX_SIZE 32768
#define MAX_ENTRY_SIZE 61
#define MAX_SLOT_SIZE 61

typedef struct ReturnEntry
{
	uint32_t tag;
	uint32_t *indexs;
	uint32_t index_num;

}ReturnEntry;

typedef struct CallStackHashEntry
{
	ReturnEntry *ret_entrys;
	uint32_t size;

}CallStackHashEntry;

typedef CallStackHashEntry * CallStackHashTable;

int search(CallStackHashTable h, uint32_t key, uint32_t value);
int insert_value(CallStackHashTable h, Stack *s, uint32_t value);
int remove_value(CallStackHashTable h, Stack *s, uint32_t value);

CallStackHashTable hash_init(uint32_t entry_num, uint32_t slot_num, uint32_t index_num);

void hash_reset(CallStackHashTable h, uint32_t entry_num);
void hash_free(CallStackHashTable h, uint32_t entry_num, uint32_t set_num, uint32_t index_num);

#define SWAP(x,y) {void *t = x; x = y; y = t;}

#endif /* !SET_STACK_H */
