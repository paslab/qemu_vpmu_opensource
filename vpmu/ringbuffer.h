#ifndef _ringbuffer_h
#define _ringbuffer_h

#define ringBuffer_typedef(T, NAME) \
    typedef struct { \
        int size; \
        volatile int start; \
        volatile int end; \
        T* elems; \
    } NAME

#define bufferInit(BUF, S, T, B_T) \
    BUF = (B_T*)malloc(sizeof(B_T));\
    BUF->size = S+1; \
    BUF->start = 0; \
    BUF->end = 0; \
    BUF->elems = (T*)calloc(BUF->size, sizeof(T))

#define isBufferAllocated(BUF) (BUF->elems != NULL)
#define bufferDestroy(BUF) free((void *)BUF->elems)
#define nextStartIndex(BUF) ((BUF->start + 1) % BUF->size)
#define nextEndIndex(BUF) ((BUF->end + 1) % BUF->size)
#define isBufferEmpty(BUF) (BUF->end == BUF->start)
#define isBufferFull(BUF) (nextEndIndex(BUF) == BUF->start)
#define isBufferNotEmpty(BUF) (BUF->end != BUF->start)
#define remainedBufferSpace(BUF) ((BUF->end >= BUF->start) ? \
        (BUF->size - (BUF->end - BUF->start) - 1) \
        : ((BUF->start - BUF->end) - 1))

#define bufferWrite(BUF, ELEM) \
    BUF->elems[BUF->end] = ELEM; \
    VPMU_MEM_FENCE();\
    BUF->end = nextEndIndex(BUF);

#define bufferRead(BUF, ELEM) \
    ELEM = BUF->elems[BUF->start]; \
    VPMU_MEM_FENCE();\
    BUF->start = nextStartIndex(BUF);

//Bulk write uses memory copy instead of looping for better performance
#define bulkWrite(BUF, _ELEMS, _NUM_ELEMS, _SIZE_OF_ELEM) do {\
    if ((BUF->end + _NUM_ELEMS) < BUF->size) { \
        memcpy(BUF->elems + BUF->end, \
               _ELEMS, \
               _NUM_ELEMS * _SIZE_OF_ELEM); \
        VPMU_MEM_FENCE(); \
        BUF->end = (BUF->end + _NUM_ELEMS) % BUF->size; \
    } \
    else { \
        memcpy(BUF->elems + BUF->end, \
               _ELEMS, \
               (BUF->size - BUF->end) * _SIZE_OF_ELEM); \
        memcpy(BUF->elems, \
               _ELEMS + (BUF->size - BUF->end), \
               (_NUM_ELEMS - (BUF->size - BUF->end)) * _SIZE_OF_ELEM); \
        VPMU_MEM_FENCE(); \
        BUF->end = (BUF->end + _NUM_ELEMS) % BUF->size; \
    } \
} while(0)

//Bulk read uses memory copy when possible.
//If indexes are close or near the boundary, use normal copy for better performance.
//Also, it enables read to read out all the packets even it's less than buffer size.
#define bulkRead(BUF, _ELEMS, _NUM_ELEMS, _SIZE_OF_ELEM, _IDX_ELEM) do { \
    _IDX_ELEM = 0; \
    if (((int64_t)BUF->end - (int64_t)BUF->start) > _NUM_ELEMS) { \
        memcpy(_ELEMS, \
               BUF->elems + BUF->start, \
               _NUM_ELEMS * _SIZE_OF_ELEM); \
        VPMU_MEM_FENCE(); \
        BUF->start = (BUF->start + _NUM_ELEMS) % BUF->size; \
        _IDX_ELEM = _NUM_ELEMS; \
    } \
    else { \
        for (_IDX_ELEM = 0; \
             _IDX_ELEM < _NUM_ELEMS && likely(isBufferNotEmpty(BUF)); \
             _IDX_ELEM++) {\
            bufferRead(BUF, _ELEMS[_IDX_ELEM]); \
        } \
    } \
} while(0)

#define waitSpaceSize(BUF, _size) \
            while(remainedBufferSpace(BUF) <= _size) usleep(1);

#endif

