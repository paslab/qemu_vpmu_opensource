//helper function to calculate TLB misses
void HELPER(vpmu_tlb_access)(uint32_t addr)
{
    if (likely(VPMU_enabled)) {
    }
}

//helper function to caclulate cache reference
void HELPER(vpmu_memory_access)(CPUARMState *env, uint32_t addr, uint32_t rw, uint32_t size)
{
    if (likely(VPMU_enabled)) {
        GlobalVPMU.load_and_store_count += 1;
        if (vpmu_model_has(VPMU_DCACHE_SIM, GlobalVPMU)) {
            //PLD instructions http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ddi0434b/CHDEDHHD.html
            if (rw == ARM_PLD) {
                addr = env->regs[(addr>>16)&0xf] + (addr&0xfff);
                rw = CACHE_PACKET_READ;
            }
            // Periodically synchronize cache data back to VPMU for lazy cache simulator
            if (GlobalVPMU.load_and_store_count % 1024 == 0)
                VPMU_cache_sync_none_blocking();

#ifdef TARGET_ARM
                //uint32_t pa = cpu_get_phys_page_debug(env, addr);
                //uint32_t pt = pa & 0xFFFFE000; //cortex-a9 L1 cache has 256-entry, blocksize 32bytes, mask 13bits
                //addr = (addr & 0x00001FFF) | pt;
                //addr = cpu_get_phys_page_debug(env, addr);
#endif

            if (unlikely(GlobalVPMU.iomem_access_flag)) {
                //IO segment
                GlobalVPMU.iomem_access_flag = 0; //Clear flag
                GlobalVPMU.iomem_count++;
            }
            else {
                //Memory segment
                if (vpmu_model_has(VPMU_MODEL_SELECT, GlobalVPMU)) {
                    model_sel_ref(PROCESSOR_CPU, 0, addr, rw, size, NULL);
                }
                else {
                    cache_ref(PROCESSOR_CPU, 0, addr, rw, size);
                }
            }
        }
    }
}

// helper function for SET and other usage. Only "taken" branch will enter this helper.
void HELPER(vpmu_branch)(CPUARMState *env, uint32_t target_addr, uint32_t return_addr)
{
#ifdef CONFIG_VPMU_SET
    CPUState *cs = CPU(arm_env_get_cpu(env));
    GlobalVPMU.cs = (void *)cs;
    static uint32_t current_pid = 0;
    static char flag_tracing = 0;
    uint32_t *traced_pid = GlobalVPMU.traced_process_pid;

    //Linux Kernel: Fork a process
    if (flag_tracing && target_addr == GlobalVPMU.do_fork_addr) {
        //DBG("fork from %d\n", current_pid);
    }

    //Linux Kernel: wake up the newly forked process
    if (flag_tracing && target_addr == GlobalVPMU.wake_up_new_task_addr) {
        //This is kernel v3.6.11
        //uint32_t target_pid = READ_INT_FROM_GUEST(cs, env->regs[0], 204);
        //This is kernel v4.4.0
        uint32_t target_pid = READ_INT_FROM_GUEST(cs, env->regs[0], 512);
        if (current_pid != 0 && current_pid == traced_pid[0]) {
            traced_pid[GlobalVPMU.num_traced_threads] = target_pid;
            GlobalVPMU.num_traced_threads++;
        }
    }

    //Linux Kernel: New process creation
    if (!flag_tracing && target_addr == GlobalVPMU.execve_addr) {
        //This is kernel v3.6.11
        //char *filepath = (char *)READ_FROM_GUEST_KERNEL(cs, env->regs[0], 0);
        //This is kernel v4.4.0
        uintptr_t name_addr = (uintptr_t)READ_INT_FROM_GUEST(cs, env->regs[0], 0);
        char *filepath = (char *)READ_FROM_GUEST_KERNEL(cs, name_addr, 0);
        if (GlobalVPMU.traced_process_name[0] != '\0'
            && strstr(filepath, GlobalVPMU.traced_process_name) != NULL) {
            //DBG("target_addr == %x from %x\n", target_addr, return_addr);
            //DBG("file: %s (pid=%d)\n", filepath, current_pid);
            tic(&(GlobalVPMU.start_time));
            VPMU_reset();
            vpmu_model_setup(&GlobalVPMU, GlobalVPMU.traced_process_model);
            vpmu_simulator_status(&GlobalVPMU, 0);
            traced_pid[0] = current_pid;
            GlobalVPMU.num_traced_threads = 1;
            VPMU_enabled = 1;
            flag_tracing = 1;
        }
    }

    //Linux Kernel: Process End
    if (flag_tracing && target_addr == GlobalVPMU.exit_addr) {
        int flag_hit = 0;
        //Loop through pid list to find if hit
        for (int i = 0; traced_pid[i] != 0; i++) {
            if (current_pid == traced_pid[i]) {
                flag_hit = 1;
                break;
            }
        }
        if (flag_hit) GlobalVPMU.num_traced_threads--;
        if (flag_hit && GlobalVPMU.num_traced_threads == 0) { 
            VPMU_sync();
            toc(&(GlobalVPMU.start_time), &(GlobalVPMU.end_time));
            VPMU_dump_result();
            memset(traced_pid, 0, sizeof(GlobalVPMU.traced_process_pid));
            VPMU_enabled = 0;
            flag_tracing = 0;
        }
    }

    //Linux Kernel: Context switch
    if (target_addr == GlobalVPMU.switch_to_addr) {
#if TARGET_LONG_BITS == 32
        uint32_t task_ptr = READ_INT_FROM_GUEST(cs, env->regs[2], 12);
#else
#pragma message("VPMU SET: 64 bits Not supported!!")
#endif
        //current_pid = READ_INT_FROM_GUEST(cs, task_ptr, 204); //This is kernel v3.6.11
        current_pid = READ_INT_FROM_GUEST(cs, task_ptr, 512); //This is kernel v4.4.0
        //ERR_MSG("pid = %x %d\n", env->regs[2], current_pid);

        //Switching VPMU when current process is traced
        if (likely(flag_tracing)) {
            int flag_hit = 0;

            //Skip pid 0 (init). 0 might be the initialization value of VPMU's pid
            if (unlikely(current_pid == 0)) {VPMU_enabled = 0; return ;}
            //Loop through pid list to find if hit
            for (int i = 0; traced_pid[i] != 0; i++) {
                if (current_pid == traced_pid[i]) {
                    flag_hit = 1;
                    break;
                }
            }
            if (likely(flag_hit)) VPMU_enabled = 1;
            else VPMU_enabled = 0;
        }
    }
#endif

    if (likely(VPMU_enabled)) {
        //CONSOLE_LOG("pc: %x->%x\n", return_addr, target_addr);
    }
}

//TODO fix processor core ID!!!!!!!!!
uint32_t ISR_addr[NUM_FIND_SYMBOLS]={};
//helper function to accumulate counters
void HELPER(vpmu_accumulate_tb_info)(CPUARMState *env, void *opaque)
{
    // Thses are for branch
    static unsigned int last_tb_pc = 0;
    static unsigned int last_tb_has_branch = 0;

    ExtraTBInfo *extra_tb_info = (ExtraTBInfo *)opaque;
    uint8_t mode = env->uncached_cpsr & CPSR_M; //User(USR)/Supervisor(SVC)/Interrupt Request(IRQ)
    int i = 0;

//    static uint32_t return_addr = 0;
//    static uint32_t last_issue_time = 0;
//    char *state = &(GlobalVPMU.state);

    if (likely(env && VPMU_enabled)) {
#if 0
        /* TODO: this mechanism should be wrapped */
        /* asm_do_IRQ handles all the hardware interrupts, not only for timer interrupts
         * pac_timer_interrupt() is the timer handler which asm_do_IRQ will call
         * run_softirqd is softirq handler in older Linux version
         * Linux handle arm interrupt with run_timer_softirq()*/
        //static char timer_interrupt_exception = 0;
        char *timer_interrupt_exception = &(GlobalVPMU.timer_interrupt_exception);

        if (env->regs[15] == ISR_addr[TICK_SCHED_TIMER])
            //if (env->regs[15] == ISR_addr[RUN_TIMER_SOFTIRQ]
            //    || env->regs[15] == ISR_addr[GOLDFISH_TIMER_INTERRUPT])
        {
            if (*state == 0) {
                /* calibrate the timer interrupt */
                int64_t tmp;
                tmp = vpmu_estimated_execution_time() - last_issue_time;
                /* timer interrupt interval = 10 ms = 10000 us */
                if (tmp > TIMER_INTERRUPT_INTERVAL) {
                    last_issue_time = vpmu_estimated_execution_time();
                    *timer_interrupt_exception = 1;
                }
                /* The link register in asm_do_irq are being cleared
                 * so we cannot use env->regs[14] directly */
                return_addr = GlobalVPMU.timer_interrupt_return_pc;
                //printf("vpmu remembered return PC=0x%x\n",return_addr);
            }
            *state = 1;
        }

        /* In timer interrupt state */
        if (unlikely(*state == 1)) {
            /* check if timer interrupt is returned */
            if (unlikely((return_addr - 4) == env->regs[15])) {
                /* timer interrupt returned */
                *state = 0;
                *timer_interrupt_exception = 0;
            } else {
                /* still in the timer interrupt stage
                 * Prevent timer interrupt to be counted, must return
                 */
                if (*timer_interrupt_exception == 0)
                    return;
            }
        }
#endif
        if (vpmu_model_has(VPMU_ICACHE_SIM, GlobalVPMU)) {
            if (vpmu_model_has(VPMU_MODEL_SELECT, GlobalVPMU)) {
                //model_sel_ref(PROCESSOR_CPU, 0, 0, CACHE_PACKET_INSTRN, 0, extra_tb_info);
                GlobalVPMU.total_tb_visit_count++;
                int distance = GlobalVPMU.total_tb_visit_count - extra_tb_info->last_visit;
                if (0 < distance && distance < 100) {
                    GlobalVPMU.hot_icache_count += extra_tb_info->num_of_cacheblks;
                    extra_tb_info->last_visit++;

                    GlobalVPMU.hot_tb_visit_count++;
                } else {
                    extra_tb_info->last_visit = GlobalVPMU.total_tb_visit_count;
                    cache_ref(PROCESSOR_CPU, 0, extra_tb_info->start_addr,
                              CACHE_PACKET_INSTRN, extra_tb_info->size_insns);

                    GlobalVPMU.cold_tb_visit_count++;
                }
            } else {
                cache_ref(PROCESSOR_CPU, 0, extra_tb_info->start_addr,
                          CACHE_PACKET_INSTRN, extra_tb_info->size_insns);
            }
        }

        if (vpmu_model_has(VPMU_PIPELINE_SIM, GlobalVPMU)) {
            if (likely(GlobalVPMU.cpu_model.dual_issue)) {
#ifdef CONFIG_VPMU_VFP
                GlobalVPMU.VFP_BASE += extra_tb_info->vfp_base;
                for (i = 0; i < ARM_VFP_INSTRUCTION_TOTAL_COUNTS; i++) {
                    GlobalVPMU.VFP_count[i] += extra_tb_info->vfp_count[i];
                }
#endif
                //Automatically calculate the missing instructions.
                GlobalVPMU.ARM_count[ARM_INSTRUCTION_UNKNOWN] = extra_tb_info->arm_count[ARM_INSTRUCTION_UNKNOWN];
                //We need to know some arm instruction counts.
                for (i = 0; i < ARM_INSTRUCTION_TOTAL_COUNTS; i++) {
                    GlobalVPMU.ARM_count[i] += extra_tb_info->arm_count[i];
                }
                GlobalVPMU.ticks += extra_tb_info->ticks - extra_tb_info->dual_issue_reduce_ticks;
                GlobalVPMU.reduce_ticks += extra_tb_info->dual_issue_reduce_ticks;
            }
            else {
                GlobalVPMU.ticks += extra_tb_info->ticks;
            }
        }

        if (vpmu_model_has(VPMU_INSN_COUNT_SIM, GlobalVPMU)) {
            GlobalVPMU.branch_count += extra_tb_info->has_branch;
            if (mode == ARM_CPU_MODE_USR) {
                GlobalVPMU.USR_icount += extra_tb_info->insn_count;
                GlobalVPMU.USR_load_count += extra_tb_info->load_count;
                GlobalVPMU.USR_store_count += extra_tb_info->store_count;
                GlobalVPMU.USR_ldst_count += (extra_tb_info->load_count + extra_tb_info->store_count);
            } else if (mode == ARM_CPU_MODE_SVC) {
                if (GlobalVPMU.swi_fired) { //TODO This feature is still lack of setting this flag to true
                    GlobalVPMU.sys_call_icount += extra_tb_info->insn_count;
                    GlobalVPMU.sys_call_load_count += extra_tb_info->load_count;
                    GlobalVPMU.sys_call_store_count += extra_tb_info->store_count;
                    GlobalVPMU.sys_call_ldst_count += (extra_tb_info->load_count + extra_tb_info->store_count);
                } else {
                    GlobalVPMU.SVC_icount += extra_tb_info->insn_count;
                    GlobalVPMU.SVC_load_count += extra_tb_info->load_count;
                    GlobalVPMU.SVC_store_count += extra_tb_info->store_count;
                    GlobalVPMU.SVC_ldst_count += (extra_tb_info->load_count + extra_tb_info->store_count);
                }
            } else if (mode == ARM_CPU_MODE_IRQ) {
                GlobalVPMU.IRQ_icount += extra_tb_info->insn_count;
                GlobalVPMU.IRQ_load_count += extra_tb_info->load_count;
                GlobalVPMU.IRQ_store_count += extra_tb_info->store_count;
                GlobalVPMU.IRQ_ldst_count += (extra_tb_info->load_count + extra_tb_info->store_count);
            } else {
                GlobalVPMU.sys_rest_icount += extra_tb_info->insn_count;
                GlobalVPMU.sys_rest_load_count += extra_tb_info->load_count;
                GlobalVPMU.sys_rest_store_count += extra_tb_info->store_count;
                GlobalVPMU.sys_rest_ldst_count += (extra_tb_info->load_count + extra_tb_info->store_count);
            }
        }

        if (vpmu_model_has(VPMU_BRANCH_SIM, GlobalVPMU)) {
            // Add global counter value of branch count.
            if (last_tb_has_branch) {
                if (extra_tb_info->start_addr - last_tb_pc <= 4) {
                    branch_ref(0, last_tb_pc, 0, 0); // Not taken
                    //DBG("%x->%x not taken\n", last_tb_pc, extra_tb_info->icache_ref.address);
                }
                else {
                    branch_ref(0, last_tb_pc, 1, 0); // Taken
                    //DBG("%x->%x taken\n", last_tb_pc, extra_tb_info->icache_ref.address);
                }
            }
            last_tb_pc = extra_tb_info->start_addr + extra_tb_info->size_insns;
            last_tb_has_branch = extra_tb_info->has_branch;
        }
    }
}
