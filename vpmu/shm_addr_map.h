#ifndef _VPMU_SHM_ADDR_MAP_
#define _VPMU_SHM_ADDR_MAP_

/*
 *     addr     |    function
 *   0x000000        token
 *   0x000100
 *      |            reserved
 *   0x000FFF
 *   0x001000
 *      |            package data
 *   0x04FFFF
 *   0x050000
 *      |            ring buffer struct
 *   0x0500FF
 *   0x050100
 *      |            buffer data
 *   0xFFFFFF
*/

#define VPMU_SHM_ADDR_TOKEN      0x000000
#define VPMU_SHM_ADDR_PKG        0x001000
#define VPMU_SHM_ADDR_BUF        0x050000
#define VPMU_SHM_ADDR_BUF_DATA   0x050100
//0x4000000 = 64MB
#define VPMU_SHM_TOTAL_SIZE      0x4000000
#define VPMU_SHM_BUF_DATA_SIZE   \
        (VPMU_SHM_TOTAL_SIZE - VPMU_SHM_ADDR_BUF_DATA)

#endif
